<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class Customer extends Model implements AuditableContract
{
	use softDeletes;
	use Auditable;

    //protected $table = 'customers';
	protected $primaryKey = 'id';
	protected $dates = ['deleted_at'];
    protected $fillable = ['phone_number','name','country','carrier','company'];
	
    public function rcCompany()
    {
        return $this->belongsTo('App\Company','company');
    }
    public function rcCarrier()
    {
        return $this->hasMany('App\Carrier');
    }
    public function rcCountry()
    {
        return $this->hasMany('App\Country');
    }

}
