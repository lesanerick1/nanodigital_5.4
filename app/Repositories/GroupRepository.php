<?php

namespace App\Repositories;

use App\Group;

class GroupRepository
{

	protected $group;

	public function __construct(Group $group)
	{
	    $this->group = $group;
	}

	public function find($id)
	{
		return $this->group->find($id);
	}

	public function getAll()
	{
		return $this->group->all();
	}

	public function findBy($att, $column)
	{
		return $this->group->where($att, $column)->get();
	}

	public function create(array $attributes)
	{
	    return Group::create($attributes);
	}
	/*function getAll();
 
	function getById($id);
 
	function create(array $attributes);
 
	function update($id, array $attributes);
 
	function delete($id);*/
}

?>