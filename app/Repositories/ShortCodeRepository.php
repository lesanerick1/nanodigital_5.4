<?php

namespace App\Repositories;

use App\ShortCode;

class ShortCodeRepository
{

	protected $shortcode;

	public function __construct(ShortCode $shortcode)
	{
	    $this->shortcode = $shortcode;
	}

	public function find($id)
	{
		return $this->shortcode->find($id);
	}

	public function getAll()
	{
		return $this->shortcode->all();
	}

	public function findBy($att, $column)
	{
		return $this->shortcode->where($att, $column)->get();
	}

	public function create(array $attributes)
	{
	    return ShortCode::create($attributes);
	}
	/*function getAll();
 
	function getById($id);
 
	function create(array $attributes);
 
	function update($id, array $attributes);
 
	function delete($id);*/
}

?>