<?php

namespace App\Repositories;

use App\Files;

class FileRepository
{

	protected $file;

	public function __construct(Files $file)
	{
	    $this->file = $file;
	}

	public function find($id)
	{
		return $this->file->find($id);
	}

	public function getAll()
	{
		return $this->file->all();
	}

	public function findBy($att, $column)
	{
		return $this->file->where($att, $column)->get();
	}

	public function create(array $attributes)
	{
	    return $this->file->create($attributes);
	}

	public function delete($id){
		$picture = $this->file->find($id);
		return $picture->delete();


	}

	/*function getAll();
 
	function getById($id);
 
	function create(array $attributes);
 
	function update($id, array $attributes);
 
	function delete($id);*/
	
}

?>