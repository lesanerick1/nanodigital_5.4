<?php

namespace App\Repositories;

use App\Campaign;

class CampaignRepository
{

	protected $campaign;

	public function __construct(Campaign $campaign)
	{
	    $this->campaign = $campaign;
	}

	public function find($id)
	{
		return $this->campaign->find($id);
	}

	public function getAll()
	{
		return $this->campaign->all();
	}

	public function findBy($att, $column)
	{
		return $this->campaign->where($att, $column)->get();
	}

	/*function getAll();
 
	function getById($id);
 
	function create(array $attributes);
 
	function update($id, array $attributes);
 
	function delete($id);*/
}

?>