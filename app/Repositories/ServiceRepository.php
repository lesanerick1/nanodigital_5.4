<?php

namespace App\Repositories;

use App\Service;

class ServiceRepository
{

	protected $service;

	public function __construct(Service $service)
	{
	    $this->service = $service;
	}

	public function find($id)
	{
		return $this->service->find($id);
	}

	public function getAll()
	{
		return $this->service->all();
	}

	public function findBy($att, $column)
	{
		return $this->service->where($att, $column)->get();
	}

	/*function getAll();
 
	function getById($id);
 
	function create(array $attributes);
 
	function update($id, array $attributes);
 
	function delete($id);*/
}

?>