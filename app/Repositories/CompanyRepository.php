<?php

namespace App\Repositories;

use App\Company;

class CompanyRepository
{

	protected $company;

	public function __construct(Company $company)
	{
	    $this->company = $company;
	}

	public function find($id)
	{
		return $this->company->find($id);
	}

	public function getAll()
	{
		return $this->company->all();
	}

	public function findBy($att, $column)
	{
		return $this->company->where($att, $column)->get();
	}

	/*function getAll();
 
	function getById($id);
 
	function create(array $attributes);
 
	function update($id, array $attributes);
 
	function delete($id);*/
}

?>