<?php

namespace App\Repositories;

use App\Customer;

class CustomerRepository
{

	protected $customer;

	public function __construct(Customer $customer)
	{
	    $this->customer = $customer;
	}

	public function find($id)
	{
		return $this->customer->find($id);
	}

	public function getAll()
	{
		return $this->customer->all();
	}

	public function findBy($att, $column)
	{
		return $this->customer->where($att, $column)->get();
	}
	public function paginate($perPage = 10){
		return $this->customer->take($perPage);
	}
	public function getColumns(array $attributes) {
		return $this->customer->select($attributes);
	}

	/*function getAll();
 
	function getById($id);
 
	function create(array $attributes);
 
	function update($id, array $attributes);
 
	function delete($id);*/
}

?>