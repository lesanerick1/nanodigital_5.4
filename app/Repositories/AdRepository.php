<?php

namespace App\Repositories;

use App\Ad;

class AdRepository
{

	protected $ad;

	public function __construct(Ad $ad)
	{
	    $this->ad = $ad;
	}

	public function find($id)
	{
		return $this->ad->find($id);
	}

	public function getAll()
	{
		return $this->ad->all();
	}

	public function findBy($att, $column)
	{
		return $this->ad->where($att, $column)->get();
	}

	/*function getAll();
 
	function getById($id);
 
	function create(array $attributes);
 
	function update($id, array $attributes);
 
	function delete($id);*/
}

?>