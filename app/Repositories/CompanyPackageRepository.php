<?php

namespace App\Repositories;

use App\CompanyPackage;

class CompanyPackageRepository
{

	protected $company_package;

	public function __construct(CompanyPackage $company_package)
	{
	    $this->company_package = $company_package;
	}

	public function find($id)
	{
		return $this->company_package->find($id);
	}

	public function getAll()
	{
		return $this->company_package->all();
	}

	public function findBy($att, $column)
	{
		return $this->company_package->where($att, $column)->get();
	}

	public function create(array $attributes)
	{
	    return ShortCode::create($attributes);
	}
	/*function getAll();
 
	function getById($id);
 
	function create(array $attributes);
 
	function update($id, array $attributes);
 
	function delete($id);*/
}

?>