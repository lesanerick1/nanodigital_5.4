<?php

namespace App\Repositories;

use App\Role;

class RoleRepository
{

	protected $role;

	public function __construct(Role $role)
	{
	    $this->role = $role;
	}

	public function find($id)
	{
		return $this->role->find($id);
	}

	public function getAll()
	{
		return $this->role->all();
	}

	public function findBy($att, $column)
	{
		return $this->role->where($att, $column)->get();
	}

	/*function getAll();
 
	function getById($id);
 
	function create(array $attributes);
 
	function update($id, array $attributes);
 
	function delete($id);*/
}

?>