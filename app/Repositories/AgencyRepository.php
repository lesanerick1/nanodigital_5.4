<?php

namespace App\Repositories;

use App\Agency;

class AgencyRepository
{

	protected $agency;

	public function __construct(Agency $agency)
	{
	    $this->agency = $agency;
	}

	public function find($id)
	{
		return $this->agency->find($id);
	}

	public function getAll()
	{
		return $this->agency->all();
	}

	public function findBy($att, $column)
	{
		return $this->agency->where($att, $column)->get();
	}

	/*function getAll();
 
	function getById($id);
 
	function create(array $attributes);
 
	function update($id, array $attributes);
 
	function delete($id);*/
}

?>