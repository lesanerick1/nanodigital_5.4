<?php

namespace App\Repositories;

use App\User;

class UserRepository
{

	protected $user;

	public function __construct(User $user)
	{
	    $this->user = $user;
	}

	public function find($id)
	{
		return $this->user->find($id);
	}

	public function getAll()
	{
		return $this->user->all();
	}

	public function findBy($att, $column)
	{
		return $this->user->where($att, $column)->get();
	}

	/*function getAll();
 
	function getById($id);
 
	function create(array $attributes);
 
	function update($id, array $attributes);
 
	function delete($id);*/
}

?>