<?php

namespace App\Repositories;

use App\ShortCodeType;

class ShortCodeTypeRepository
{

	protected $shortcodetype;

	public function __construct(ShortCodeType $shortcodetype)
	{
	    $this->shortcodetype = $shortcodetype;
	}

	public function find($id)
	{
		return $this->shortcodetype->find($id);
	}

	public function getAll()
	{
		return $this->shortcodetype->all();
	}

	public function findBy($att, $column)
	{
		return $this->shortcodetype->where($att, $column)->get();
	}

	public function create(array $attributes)
	{
	    return Shortcodetype::create($attributes);
	}
	/*function getAll();
 
	function getById($id);
 
	function create(array $attributes);
 
	function update($id, array $attributes);
 
	function delete($id);*/
}

?>