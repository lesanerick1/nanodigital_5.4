<?php

namespace App\Repositories;

use App\Test;

class TestRepository
{

	protected $test;

	public function __construct(Test $test)
	{
	    $this->test = $test;
	}

	public function find($id)
	{
		return $this->test->find($id);
	}

	public function getAll()
	{
		return $this->test->all();
	}

	public function findBy($att, $column)
	{
		return $this->test->where($att, $column)->get();
	}

	public function save(Test $test) {
        return $this->test->create($test);
    }
	/*function getAll();
 
	function getById($id);
 
	function create(array $attributes);
 
	function update($id, array $attributes);
 
	function delete($id);*/
}

?>