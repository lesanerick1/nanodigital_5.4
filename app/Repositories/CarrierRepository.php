<?php

namespace App\Repositories;

use App\Carrier;

class CarrierRepository
{

	protected $carrier;

	public function __construct(Carrier $carrier)
	{
	    $this->carrier = $carrier;
	}

	public function find($id)
	{
		return $this->carrier->find($id);
	}

	public function getAll()
	{
		return $this->carrier->all();
	}

	public function findBy($att, $column)
	{
		return $this->carrier->where($att, $column)->get();
	}

	public function create(array $attributes)
	{
	    return Carrier::create($attributes);
	}
	/*function getAll();
 
	function getById($id);
 
	function create(array $attributes);
 
	function update($id, array $attributes);
 
	function delete($id);*/
}

?>