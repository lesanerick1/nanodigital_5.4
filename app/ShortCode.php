<?php

namespace App;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class ShortCode extends Model 
{
	//use Auditable;
	use softDeletes;
    protected $primaryKey = 'id';
	protected $dates = ['deleted_at'];
    protected $fillable = ['company','short_code','sender_id', 'type'];

	public function rsCompany()
    {
        return $this->belongsTo('App\Company', 'company');
    }

    

   

}


