<?php

namespace App;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class CompanyPackage extends Model implements AuditableContract
{
	use Auditable;
	use softDeletes;
    protected $primaryKey = 'id';
	protected $dates = ['deleted_at'];
    protected $fillable = ['company','number_of_messages','date_bought', 'validity', 'unit_price', 'amount_paid'];

	public function rsCompany()
    {
        return $this->belongsTo('App\Company', 'company');
    }

    

   

}


