<?php

namespace App;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Country extends Model implements AuditableContract
{
	use Auditable;
	use softDeletes;
    protected $primaryKey = 'id';
    protected $fillable = ['name','iso_code', 'calling_code'];


}