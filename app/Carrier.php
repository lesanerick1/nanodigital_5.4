<?php

namespace App;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Carrier extends Model implements AuditableContract
{
	use Auditable;
    protected $primaryKey = 'id';
    protected $fillable = ['name','slug', 'country'];


    public function campaign(){
        return $this->belongsToMany('App\Campaign', 'campaign_carriers', 'carrier', 'campaign');
    }

}