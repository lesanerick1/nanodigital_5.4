<?php

namespace App;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Database\Eloquent\Model;

class ShortCodeType extends Model implements AuditableContract
{
	use Auditable;

	protected $table = 'shortcode_types';
    protected $primaryKey = 'id';
    protected $fillable = ['name',];

	  

}


