<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class CampaignInfo extends Model implements AuditableContract
{
	use softDeletes;
	use Auditable;

	protected $table = 'campaign_info';
	protected $dates = ['deleted_at'];
	protected $primaryKey = "id";
    protected $fillable = ['campaign','broadcast_time','start_date','end_date','intervals'];

    public function rcCampaign()
	{
	    return $this->belongsTo('App\Campaign', 'campaign');
	}		

}
