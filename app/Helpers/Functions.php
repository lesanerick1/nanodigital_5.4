<?php

namespace App\Helpers;
use App\Test;
use App\Service;
use DB;
use DateTime;
use File;
use Sentinel;
use GuzzleHttp\Client;
use App\Repositories\ServiceRepository;
use App\Repositories\TestRepository;

class Functions {

	
	public function createSmil($image){
		$contents = substr($image, 14);
		$mms_image = "/var/www/admin/public".$image;
		$smil = substr($image, 0, -4);
		$smil1 = $smil.".smil";
		$smil_fil = str_replace('/', '%2F', $smil.".smil");
		$smil_file = public_path().''.$smil1;
		//echo $contents;
		$smil_contents = '<smil>
							<head>
								<layout>
									<root-layout width="960" height="960"
									background-color="white" />
									 <!--<region id="a" top="10"  />-->
									 <region id="b" />
								</layout>
							</head>
							<body>
								<par dur="5s">
									<!--<text src="unliminet.txt" region="a"
										begin="0s"
										dur="8s"/>-->
									<img src="'.$contents.'" region="b"
									 begin="0s"
									 end="8s"/>
								</par>
							</body>
						</smil>';
		//echo $smil_file;
		//echo $smil_fil;
		$bytes_written = File::put($smil_file, $smil_contents);
		if ($bytes_written === false)
		{
				die("Error writing to file");
		}
		return $smil_fil;
	}

	public function sendMessage($phone_no, $message, $subject, $media, $type)
    {
		$company = '1';
		$service = new Service();
		$service = new ServiceRepository($service);
			if($type == 1) {
				$company_settings = $service->getAll('company',$company);
				if (count($company_settings) == 0){
					$company_settings = $service->findBy('company',$company);
				}			
			}
			else {
				$company_settings = $service->findBy('company',$company); 
				if (count($company_settings) == 0){
					$company_settings = $service->findBy('company',$company);
				}
			}
			foreach ($company_settings as $company_setting) {
				$shortCode = $company_setting->shortCode;
				$spId = $company_setting->spId;
				$spPassword = $company_setting->spPassword;
				$serviceId = $company_setting->serviceId;
			} 			
			$timestamp = "20160908";
			$subReqId = mt_rand(1000000, mt_getrandmax());
			$correlator_no= mt_rand(1, mt_getrandmax());
			$password = md5($spId.$spPassword.$timestamp);
			$correlator="6266".$correlator_no;
			$delivery_endpoint = "http://cloud.nanodigital.co.ke:8008/report/dlr-sms.php";

			if(starts_with($phone_no, '07')){
				$mms_phone1= substr($phone_no, -9);
				$phone_no = "254".$mms_phone1;
			}
			else if(starts_with($phone_no, '+')){
				$phone_no = str_replace('+', '', $phone_no);
			}
       $pre = substr($phone_no, 0, 5);
        switch($pre) {
          case 25473:
			$carrier = 2;
			$this->sendAirtel($phone_no, $message, $subject, $media, $type, $correlator, $carrier, $delivery_endpoint, $company, $shortCode);
            break;
          case 25476:
			$carrier = 2;
            $this->sendAirtel($phone_no, $message, $subject, $media, $type, $correlator, $carrier, $delivery_endpoint, $company, $shortCode);
            break;
          case 25477:
			$carrier = 3;
            $this->sendAirtel($phone_no, $message, $subject, $media, $type, $correlator, $carrier, $delivery_endpoint, $company, $shortCode);
            break;
          case 25478:
			$carrier = 2;
            $this->sendAirtel($phone_no, $message, $subject, $media, $type, $correlator, $carrier, $delivery_endpoint, $company, $shortCode);
            break;
          default:
			$carrier = 1;
            $this->sendSafcom($phone_no, $message, $subject, $media, $type, $carrier, $spId, $password, $serviceId, $subReqId, $timestamp, $shortCode, $correlator, $delivery_endpoint, $company);
        }  
		unset($phone_no);
    }

    public function sendSafcom($phone_no, $message, $subject, $media, $type, $carrier, $spId, $password, $serviceId, $subReqId, $timestamp, $shortCode, $correlator, $delivery_endpoint, $company){

        if ($type == '1') {
        	
        	$client = new Client();
			$response = $client->get('http://cloud.nanodigital.co.ke:8008/sendSms/test_utils.php?phone_no='.$phone_no.'&caption='.$phone_no.'');

	    }

		else if ($type == '2') {
			$image = "/var/www/admin/public".$media;
			if (str_contains($media, ['.mp4', 'mpeg', '3gp'])){
	          $output = shell_exec('java sendVideo "'.$phone_no.'" "'.$subject.'" "'.$image.'" "'.$spId.'" "'.$password.'" "'.$serviceId.'" "'.$subReqId.'" "'.$timestamp.'" "'.$shortCode.'" "'.$correlator.'" "'.$delivery_endpoint.'" ');
	        }
	        else {
	          $output = shell_exec('java sendMms "'.$phone_no.'" "'.$subject.'" "'.$media.'" "'.$spId.'" "'.$password.'" "'.$serviceId.'" "'.$subReqId.'" "'.$timestamp.'" "'.$shortCode.'" "'.$correlator.'" "'.$delivery_endpoint.'" ');
	        }
		}
		else {

		}
        curl_close($ch);
        $this->recordTransaction($phone_no, $message, $subject, $media, $type, $carrier, $correlator, $shortCode);
    }

    public function sendAirtel($phone_no, $message, $subject, $media, $type, $correlator, $carrier, $delivery_endpoint, $company, $shortCode){
        if ($type == '1') {
        	$dlr= urlencode('http://cloud.nanodigital.co.ke:8008/report/dlr-sms.php?msgid='.$correlator.'&state=%d&msisdn=%p');

        	$client = new Client(['base_uri' => 'http://cloud.nanodigital.co.ke:13012/cgi-bin/']);

        	//$response = $client->get('sendsms?user=nanoSmsc&pass=csms0nan&text='.$message.'&to='.$phone_no.'&dlr-mask=31&dlr-url='.$dlr.'');
      
		}
		else if ($type == '2') {
			$encSubject= urlencode($subject);
			$smilFile = $this->createSmil($media);

			$client = new Client();
			$response = $client->get('http://104.131.29.74:10001/cgi-bin/sendmms?username=nano&password=@n@n0mms&dlr-url=http%3A%2F%2F104.131.29.74%2Freport%2Fdlr.php&rr-url=http%3A%2F%2F104.131.29.74%2Freport%2Frr.php&to='.$phone_no.'&subject='.$encSubject.'&allow-adaptations=1&content-adaptation=true&vasid=Nano&content-type=application%2Fsmil&content-url=http%3A%2F%2F104.131.29.74'.$smilFile);

		}
		else {

		}
        $this->recordTransaction($phone_no, $message, $subject, $media, $type, $carrier, $correlator, $shortCode);
    }

    public function recordTransaction($phone_no, $message, $subject, $media, $type, $carrier, $correlator, $shortCode) {
    	$test = new Test();
    	$recordTest = new TestRepository($test);
    	$test->content= $message;
    	$test->subject= $subject;
    	$test->type= $type;
    	$test->sender= Sentinel::getUser()->id;
    	$test->sender_short_code= $shortCode;
    	$test->receiver= $phone_no;
    	$test->broadcast_time= new DateTime();
    	$test->delivery_time= null;
    	$test->read_time= null;
    	$test->message_id= $correlator;
    	$test->carrier= $carrier;
    	$test->company= Sentinel::getUser()->id;
    	$test->delivery_status= "";

    	$record =$test->save();
    	
    }

}