<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
//use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Helpers\Functions;

class sendMessage implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $phone_no;
    protected $message;
    protected $subject;
    protected $media;
    protected $type;
    protected $spId;
    protected $password;
    protected $serviceId;
    protected $subReqId;
    protected $timestamp;
    protected $shortCode;
    protected $correlator;
    protected $delivery_endpoint;
    protected $company;

    public function __construct($phone_no, $message, $subject, $media, $type)
    {
        $this->phone_no = $phone_no;
        $this->message = $message;
        $this->subject = $subject;
        $this->media = $media;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $helper = new Functions();
        $task = $helper->sendMessage($this->phone_no, $this->message, $this->subject, $this->media, $this->type);

    }
}
