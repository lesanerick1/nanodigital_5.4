<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class Campaign extends Model implements AuditableContract
{
	use softDeletes;
	use Auditable;

	protected $primaryKey = 'id';
	protected $dates = ['deleted_at'];
    protected $fillable = ['type','company','name','content','subject'];
	
    public function rcCompany()
    {
        return $this->belongsTo('App\Company','company');
    }

    public function info(){
        return $this->hasOne('App\CampaignInfo', 'campaign');
    }

    public function type(){
        return $this->belongsTo('App\ProductType', 'type');
    }

    public function carrier(){
        return $this->belongsToMany('App\Carrier', 'campaign_carriers', 'campaign', 'carrier');
    }

}
