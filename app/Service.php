<?php

namespace App;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Service extends Model implements AuditableContract
{
	use Auditable;
	use softDeletes;
    protected $primaryKey = 'id';
	protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'sp_id', 'sp_password', 'service_id'];

	public function shortcode()
    {
        return $this->belongsTo('App\ShortCode', 'short_code');
    }

    

   

}


