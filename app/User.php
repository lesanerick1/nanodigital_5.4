<?php namespace App;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cache;
use Illuminate\Notifications\Notifiable;
use Sentinel;

class User extends EloquentUser  implements AuditableContract {
    use Auditable;
    use Notifiable;
    use SoftDeletes;

    protected $table = 'users';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
    protected $hidden = ['password', 'remember_token'];
    protected $fillable = [];

    public function rsCompany(){
        return $this->belongsTo('App\Company', 'company');
    }
    
    public function roles()
    {
        return $this->belongsToMany('App\Role', 'role_users', 'user_id', 'role_id');
    }

    public function scopeUserBrand($query, $id)
    {
        //return $query->where('brand_id', $id);
    }

    public static function isAdmin()
    {
        if (Sentinel::check()) {
            if (Sentinel::inRole('admin')) {
                return true;
            } 
        }
    }

    public function isOnline(){
        //return Cache::has('user-is-online-' . $this->id);
    }

    public function tickets(){
        //return $this->hasMany('App\Ticket', 'agent_id')->whereNull('completed_at');
        
    }
    public function agents(){
        //return $this->hasMany('App\Agent');
    }

    public function agentTickets($complete = false)
    {
        /*if ($complete) {
            return $this->hasMany('App\Ticket', 'agent_id')->whereNotNull('completed_at');
        } else {
            return $this->hasMany('App\Ticket', 'agent_id')->whereNull('completed_at');
        }*/
    }

    public function userTickets($complete = false)
    {
        /*if ($complete) {
            return $this->hasMany('App\Ticket', 'user_id')->whereNotNull('completed_at');
        } else {
            return $this->hasMany('App\Ticket', 'user_id')->whereNull('completed_at');
        }*/
    }

    function file()
    {
        //return $this-> hasMany('App\File');
    }

}

