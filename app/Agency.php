<?php

namespace App;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Agency extends Model implements AuditableContract
{
	use Auditable;
	use softDeletes;
    protected $primaryKey = 'id';
	protected $dates = ['deleted_at'];
    protected $fillable = ['name','slug'];

	public function rsCompany()
    {
        return $this->hasMany('App\Company', 'agency');
    }

	public function rcUser()
    {
        return $this->hasManyThrough('App\User','App\Company','agency', 'company', 'id');
    }

 	public function customers()
    {
        //return $this->hasMany('App\Customer','company');
    }

    public function campaign()
    {
        //return $this->hasManyThrough('App\Campaign','App\Company');
    }
	
     public function customer()
    {
        //return $this->hasManyThrough('App\Customer','App\Company');
    }

    public function scopeAgencyActive($query){

		//return $query->where('agency_status',1);
	}
	public function scopeAgencyPaused($query){
		//return $query->where('agency_status',0);
	}
	public function scopeAgencyDeleted($query){
		//return $query->where('agency_status',3);
	}
    

   

}


