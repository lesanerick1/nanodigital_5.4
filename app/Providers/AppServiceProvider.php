<?php

namespace App\Providers;

use App\Repositories\CompanyRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    protected $company;

    public function boot()
    {
        //
        //$company  = app(CompanyRepository::class);
        //$companies = $company->getAll();
        //$companies = $this->company->getAll(); 
        //view()->share('companies', $companies);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
