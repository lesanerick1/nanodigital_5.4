<?php

namespace App;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Test extends Model implements AuditableContract
{
	use Auditable;
	use softDeletes;
    protected $primaryKey = 'id';
    protected $fillable = ['content', 'subject', 'type', 'sender', 'sender_short_code', 'receiver', 'campaign', 'broadcast_time', 'delivery_time', 'read_time', 'message_id', 'carrier', 'company', 'delivery_status'];


}