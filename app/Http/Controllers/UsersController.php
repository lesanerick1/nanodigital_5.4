<?php namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Http\Request;
use Sentinel;
use URL;
use View;
use Session;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use App\Repositories\CompanyRepository;


class UsersController extends MainController
{
	public function __construct(RoleRepository $role, UserRepository $user, CompanyRepository $company)
    {
        $this->role = $role;
        $this->user = $user;
        $this->company = $company;
    }

	public function index()
    {
        $roles = $this->role->getAll();
        $users = $this->user->getAll();
        //$company = $this->company->find($id);
        return View('users.index', compact('roles', 'users'));
    }
    public function edit()
    {
    	if ($input['action'] == 'edit') {
		    
		} else if ($input['action'] == 'delete') {
		    
		} else if ($input['action'] == 'restore') {
		    
		}
        //$roles = $this->role->getAll();

        //return View('users.index', compact('roles'));
    }

    public function create()
    {
        // Get all the available groups
        $user = Sentinel::getUser();
        
        // Show the page
        return View('users/create');
    }
}