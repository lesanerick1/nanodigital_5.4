<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\CustomerTransformer;
use Dingo\Api\Routing\Helpers;
use App\Customer;
use App\Jobs\sendMessage;
use App\Http\Requests;
use Illuminate\Http\Request;

/**
 * @resource Bulk SMS
 *
 * These are the API calls to send Bulk SMS to a single number or several numbers.
 */
class BulkSmsController extends Controller
{
    use Helpers;
    /**
    * Send SMS
    * Sends an SMS message to a number provided.
    * Provide the phone_no (starting with 254) and the message 
    *
    * @param Request $request
    */
    public function sendSms(Request $request)
    {
         $phone_no=$request->phone_number;
         $message=$request->message;
         $subject = "";
         $media = "";
         $subject = "";
         $type = "1";
         //$job = (new sendMessage($phone_no, $message, $subject, $media, $type, $spId, $password, $serviceId, $subReqId, $timestamp, $shortCode, $correlator, $delivery_endpoint, $company))->delay(5);
         $job = (new sendMessage($phone_no, $message, $subject, $media, $type))->delay(5);
         dispatch($job);

         //return response()->json();
        //if ($job){
            //return $this->response->created();
        //}
        //return $this->response->errorBadRequest();
        //return $this->response->collection($customers, new CustomerTransformer);

    }
    /**
    * Send SMS to Customer Group
    * Sends an SMS message to a customer group defined in the system.
    * Provide the group name (e.g testing) and message 
    *
    * @param Request $request
    */
    public function sendToGroup($group, $message)
    {
        // $customers = Customer::leftJoin('customer_groups','customers.customer_group', '=', 'customer_groups.id')->where('slug', $group)->get();
        // foreach ($customers as $customer) {
        //     $job = (new sendSms($customer->phone_no,$message));
        //     dispatch($job);
        //     //return $this->response->collection($customer, new CustomerTransformer);
        // }
        // return $this->response->errorNotFound();
    }
    /**
    * Send SMS to Network
    * Sends an SMS message to a network carrier.
    * Provide the network name (e.g safaricom) and message 
    *
    * @param Request $request
    */
    public function sendToNetwork($carrier, $message)
    {
        // $customers = Customer::leftJoin('carriers','customers.carrier', '=', 'carriers.CarrierId')->where('slug', $carrier)->get();
        // foreach ($customers as $customer) {
        //     $job = (new sendSms($customer->phone_no,$message));
        //     dispatch($job);
        // }
        // return $this->response->errorNotFound();
    }
}