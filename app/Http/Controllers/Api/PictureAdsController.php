<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\CustomerTransformer;
use Dingo\Api\Routing\Helpers;
use App\Customer;
use App\Jobs\sendSms;
use App\Http\Requests;
use Illuminate\Http\Request;

/**
 * @resource Picture and Video Ads
 *
 * These are the API calls to send Picture and Video Ads to a single number or several numbers.
 */
class PictureAdsController extends Controller
{
    use Helpers;
    /**
    * Send Picture and Video Ads
    * Sends a Picture and Video Ad message to a number provided.
    * Provide the phone_no (starting with 254) and message 
    *
    * @param Request $request
    */
    public function sendAd(Request $request)
    {
        // $phone_no=$request->phone_no;
        // $message=$request->message;
        // $correlator_no= mt_rand(1, mt_getrandmax());
        // $correlator="6266".$correlator_no;
        // $job = (new sendSms($phone_no, $message, $correlator))->delay(5);
        // dispatch($job);

        // return response()->json(compact('phone_no'));
        //if ($job){
            //return $this->response->created();
        //}
        //return $this->response->errorBadRequest();
        //return $this->response->collection($customers, new CustomerTransformer);

    }
    /**
    * Send Picture and Video Ads to Customer Group
    * Sends a Picture and Video Ad message to a customer group defined in the system.
    * Provide the group name (e.g testing) and message 
    *
    * @param Request $request
    */
    public function sendToGroup($group, $message)
    {
        // $customers = Customer::leftJoin('customer_groups','customers.customer_group', '=', 'customer_groups.id')->where('slug', $group)->get();
        // foreach ($customers as $customer) {
        //     $job = (new sendSms($customer->phone_no,$message));
        //     dispatch($job);
        //     //return $this->response->collection($customer, new CustomerTransformer);
        // }
        // return $this->response->errorNotFound();
    }
    /**
    * Send Picture and Video Ads to Network
    * Sends a Picture and Video Ad message to a network carrier.
    * Provide the network name (e.g safaricom) and message 
    *
    * @param Request $request
    */
    public function sendToNetwork($carrier, $message)
    {
        // $customers = Customer::leftJoin('carriers','customers.carrier', '=', 'carriers.CarrierId')->where('slug', $carrier)->get();
        // foreach ($customers as $customer) {
        //     $job = (new sendSms($customer->phone_no,$message));
        //     dispatch($job);
        // }
        // return $this->response->errorNotFound();
    }
}