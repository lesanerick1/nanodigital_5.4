<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\CustomerTransformer;
use Dingo\Api\Routing\Helpers;
use App\Customer;
use App\Http\Requests;
use Illuminate\Http\Request;

/**
* @resource Customers
*
* These are the API calls to fetch customer details; one, by group, by network or all customers.
*/
class CustomerController extends Controller
{
    use Helpers;
    
    /**
    * Get All Customers
    * Retrieves all customers from the system.
    *  
    *
    * @param Request $request
    */
    public function getCustomers()
    {
        
        $customers = Customer::all();

        return $this->response->collection($customers, new CustomerTransformer);

    }

    /**
    * Get one Customer by Phone Number
    * Retrieves one customers from the system.
    * Provide the phone_no (format should start with 254) of the customer whose details you want to retrieve.
    *
    * @param Request $request
    */
    public function show(Request $request)
    {

        $customer = Customer::where('phone_no', $request->phone_number)->first();
        if ($customer) {
            return $this->response->collection($customer, new CustomerTransformer);
        }
        return $this->response->errorNotFound();
    }
    /**
    * Get Customers by Customer Group
    * Retrieves all customers within the customer group from the system.
    * Provide the group name (e.g testing).
    *
    * @param Request $request
    */
    public function customerByGroup($group)
    {
        $customer = Customer::leftJoin('customer_groups','customers.customer_group', '=', 'customer_groups.id')->where('slug', $group)->get();
        if ($customer) {
            return $this->response->collection($customer, new CustomerTransformer);
        }
        return $this->response->errorNotFound();
    }
    /**
    * Get Customers by Customer Network Carrier
    * Retrieves all customers within the network from the system.
    * Provide the network (e.g safaricom).
    *
    * @param Request $request
    */
    public function customerByNetwork($carrier)
    {
        $customer = Customer::leftJoin('carriers','customers.carrier', '=', 'carriers.CarrierId')->where('slug', $carrier)->get();
        if ($customer) {
            return $this->response->collection($customer, new CustomerTransformer);
        }
        return $this->response->errorNotFound();
    }
    /**
    * Get all subscribed Customers
    * Retrieves all customers who are subscribed to the service from the system.
    * 
    *
    * @param Request $request
    */
    public function customerSubscribed()
    {
        $customer = Customer::where('status', '1')->get();
        if ($customer) {
            return $this->response->collection($customer, new CustomerTransformer);
        }
        return $this->response->errorNotFound();
    }
    /**
    * Get all unsubscribed Customers
    * Retrieves all customers who are unsubscribed to the service from the system.
    * 
    *
    * @param Request $request
    */
    public function customerUnsubscribed()
    {
        $customer = Customer::where('status', '0')->get();
        if ($customer) {
            return $this->response->collection($customer, new CustomerTransformer);
        }
        return $this->response->errorNotFound();
    }
}