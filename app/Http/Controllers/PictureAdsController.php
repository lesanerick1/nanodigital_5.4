<?php namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Http\Request;
use Sentinel;
use URL;
use View;
use Session;
use Response;
use Input;
use File;
use Charts;
use Carbon\Carbon;
use App\Files;
use App\Campaign;
use App\Ad;
use Yajra\Datatables\Datatables;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\TestRepository;
use App\Repositories\CampaignRepository;
use App\Repositories\FileRepository;
use App\Repositories\AdRepository;
use App\Repositories\AgencyRepository;
use App\Repositories\GroupRepository;
use App\Repositories\CarrierRepository;
use Vinkla\Hashids\Facades\Hashids;


class PictureAdsController extends MainController
{
    public function __construct(TestRepository $test, UserRepository $user, CompanyRepository $company,CampaignRepository $campaign, FileRepository $filerepo, AdRepository $ad, AgencyRepository $agency, GroupRepository $group, CarrierRepository $carrier)
    {
        $this->test = $test;
        $this->user = $user;
        $this->company = $company;
        $this->campaign = $campaign;
        $this->filerepo = $filerepo;
        $this->ad = $ad;
        $this->agency = $agency;
        $this->group = $group;
        $this->carrier = $carrier;
    }

    public function index(){
        $tests = $this->test->findBy('type', '2');
        $campaigns = $this->campaign->findBy('type', '2');
        $agencies = $this->agency->findBy('name', 'Perry');
        $customer_groups = $this->group->getAll();
        $carriers = $this->carrier->getAll();
        // $company = Sentinel::getUser()->company;
        // $campaign_criteria = ['type' => '1', 'company' => $company];
        // $campaigns = Campaign::with('rcCompany','info','carrier')->select('campaigns.*')->where($campaign_criteria)->get();
        // dd($agencies);
        $uploaded_images = Files::all();
        $user = Sentinel::getUser();

        $campaign = [1,2,3];
        $start_date = "2017-01-14 08:19:04";
        $end_date = "2017-07-14 08:19:04";
        $campaigns_default = Ad::whereBetween('created_at', [$start_date, $end_date])->whereIn('campaign', [$campaign])->get();

        $chart = Charts::database($campaigns_default
              , 'pie', 'highcharts')
              ->elementLabel("Number of Messages")
              ->title('Message Deliveries')
              ->responsive(true)
              ->groupBy('delivery_status', null, [1 => 'Delivered', 2 => 'Not Delivered', 4 => 'Queued', ''=> 'Pending']);

        return View('picture-ads.index', compact('tests', 'campaigns', 'uploaded_images', 'user', 'chart', 'agencies', 'customer_groups', 'carriers'));
    }

    public function uploadFiles(Request $request){
        
        /*if($request->hasFile('file')) {
            $file = Input::file('file');
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = $timestamp. '-' .$file->getClientOriginalName();
            $name = str_replace(" ", "-", $name);
            if (str_contains($name, ["mp4", "mpeg", "3gp"])){
                //$store->file_type = 3;
            }
            else {
                //$store->file_type = 1;
            }
            //$store->file = $name;
            $file->move(public_path().'/uploads/file/', $name);
        }
        */
        // echo Input::file('file');

        if($request->hasFile('file')) {
            $file = request()->file('file');

            //$img = Image::make('foo.jpg')->resize(300, 200);

            //echo $file;
            $destinationPath = public_path().'/uploads';
            // If the uploads fail due to file system, you can try doing public_path().'/uploads' 
            // $extension = File::extension($file['name']);
            $extension= substr($file, 0, -4);
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $filename = $timestamp. '-' .$file->getClientOriginalName();
            $filename = str_replace(" ", "-", $filename);
            // $filename = str_random(12) .$extension;
            //$filename = $file->getClientOriginalName();
            //$extension =$file->getClientOriginalExtension(); 
            $upload_success = request()->file('file')->move($destinationPath, $filename);
            // $upload_success = Input::upload('file', $destinationPath, $filename);

            // if( $upload_success ) {
            //    return Response::json('success', 200);
            // } else {
            //    return Response::json('error', 400);
            // }
            $user = Sentinel::getUser();


            $created_file = Files::create([

                    'file_name' => $filename,
                    'original_name' => $file->getClientOriginalName(),
                    'company' => $user->company,
                    'posted_by' =>$user->id,
                ]);

        }

        else{
            echo "No file";
            exit;
        }
        //return View('user.index');
        
    }
    public function sendMms(){


        }

    public function deleteImage(Request $request){
        if(!is_null($request->id)){
            $this->filerepo->delete($request->id);
        }
    }

    public function campaignData(){

        $company = Sentinel::getUser()->company;
        $campaign_criteria = ['type' => '2', 'company' => $company];
        $campaigns = Campaign::with('rcCompany','info','carrier', 'type')->select('campaigns.*')->where($campaign_criteria)->get();
        $id=1;
        foreach($campaigns as $campaign){
            $campaign['index'] = $id;
            $id++;
        }
        // dd($campaigns);
        // $time = strtotime($campaigns[0]->info->broadcast_time);
        // dd(date('h:i:s A', strtotime($campaigns[0]->info->broadcast_time)));
        return Datatables::of($campaigns)
            // ->edit_column()
        // ->add_column('actions',function($campaigns) {
        //         $actions = "<a href=\" route('picture-ads/update', $campaigns->id)\" > <i class=\"livicon\" data-name=\"edit\" data-size=\"24\" data-loop=\"true\" data-c=\"#428BCA\" data-hc=\"#428BCA\" title=\"View\"></i></a>";
        //         return $actions;
        //     })
        // ->add_column('actions', '<a href=" {!! route(\'picture-ads.update\', $id) !!}"> <i class="livicon" data-name="edit" data-size="24" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="View"></i></a>')
        ->addColumn('actions', function ($campaigns) {
                return '<a href=' .route('picture-ads.update', $campaigns->id) .' class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
        ->editColumn('run', function($campaigns){
                return '<a href=""><i class="fa fa-envelope-o" style="color:#418BCA; font-size: 22px;"></i></a>';
        })
        ->editColumn('analysis', function($campaigns){
                return '<a href=' .route('picture-ads.delivery', Hashids::encode($campaigns->id))    .'><span class="fa fa-bar-chart-o" style="color:#418BCA; font-size: 22px;"></span></a>';
        })
        ->editColumn('status', function($campaigns){
                if ( $campaigns->status == '0')
                  return 'Inactive';
                else if ( $campaigns->status == '1')
                  return 'Active';
                else
                  return  " ";       
        })
        ->rawColumns(['actions', 'analysis', 'run', 'status'])
        ->make(true);
    }

    public function deliveryAnalysis(Request $request){
        if(isset($request->id)){
            $encodedId = $request->id;
            $decodedId = Hashids::decode($request->id);
            // dd($decodedId);
            $campaign = $this->campaign->find($decodedId);

            $chart = Charts::database(Ad::where('campaign', $decodedId)
              ->get(), 'pie', 'highcharts')
              ->elementLabel("Number of Messages")
              ->title('Message Deliveries')
              ->responsive(true)
              ->groupBy('delivery_status', null, [1 => 'Delivered', 2 => 'Not Delivered', 4 => 'Queued', ''=> 'Pending']);

            return View('picture-ads.delivery', compact('campaign', 'encodedId', 'chart'));
              // dd($chart->render());
              // echo "<pre>";
              // print_r($chart->render());
              // echo "</pre>";
              // exit;

        }
    }

    public function deliveryData(Request $request){
        

        $decodedId = Hashids::decode($request->id);
        $campaignStats = $this->ad->findBy('campaign', $decodedId);
        return Datatables::of($campaignStats)
        ->editColumn('delivery_status', function ($campaignStats){
            if ( $campaignStats->delivery_status == '1')
                  return 'Delivered';
                else if ( $campaignStats->delivery_status == '')
                  return 'Pending';
                else
                  return  "Sent";       
        })
        ->rawColumns(['delivery_status'])
        ->make(true);

    }

    public function campaignAnalysis(Request $request){
        // dd($request);
        //return $request->daterange;


        $tests = $this->test->findBy('type', '2');
        $campaigns = $this->campaign->findBy('type', '2');
        $uploaded_images = Files::all();
        $user = Sentinel::getUser();

        $start_date = substr($request->daterange,0, 10) ." 00:00:00";
        $end_date = substr($request->daterange,13, 10) ." 23:59:59";
        // $campaign = implode(",", $request->campaign);
        // $campaign = [1,2,3];
        // $start_date = "2017-01-14 08:19:04";
        // $end_date = "2017-07-14 08:19:04";
        $campaigns_filtered = Ad::whereBetween('created_at', [$start_date, $end_date])->get(['ads.id', 'ads.receiver', 'ads.subject', 'ads.content', 'ads.delivery_status']);


        $datatables =  Datatables::of($campaigns_filtered)
        ->editColumn('delivery_status', function ($campaignStats){
            if ( $campaignStats->delivery_status == '1')
                  return 'Delivered';
                else if ( $campaignStats->delivery_status == '')
                  return 'Pending';
                else
                  return  "Sent";       
        })
        ->rawColumns(['delivery_status'])
        ->make(true);
        

        $chart = Charts::database($campaigns
              , 'pie', 'highcharts')
              ->elementLabel("Number of Messages")
              ->title('Message Deliveries')
              ->responsive(true)
              ->groupBy('delivery_status', null, [1 => 'Delivered', 2 => 'Not Delivered', 4 => 'Queued', ''=> 'Pending']);
        // View::share('chart', $chart);
        // dd($chart);
        // return $chart;
              $chart_json = json_encode($chart);
        return ['datatables' => $datatables, 'chart' => $chart_json];
              //return View('picture-ads.index', compact('tests', 'campaigns', 'uploaded_images', 'user', 'chart'));

    }

    public function create(Request $request){
        // echo "Working";
        dd($request);

    }
}