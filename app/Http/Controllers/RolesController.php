<?php

namespace App\Http\Controllers;

use App\Repositories\RoleRepository;

class RolesController
{
	protected $role;

	public function __construct(RoleRepository $role)
	{
		$this->role = $role;
	}

	public function getAllRoles()
	{
		return $this->role->getAll();
	}

	public function show($slug)
	{
		return $this->role->findBy('slug', $slug);
	}


}
?>