<?php

namespace App\Http\Controllers;

use App\Repositories\TestRepository;

class TestsController
{
	protected $test;

	public function __construct(TestRepository $test)
	{
		$this->test = $test;
	}

	public function getAllTests()
	{
		return $this->test->getAll();
	}

	public function show($type)
	{
		return $this->test->findBy('type', $type);
	}

	 

}