<?php namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Http\Request;
use Sentinel;
use URL;
use View;
use Session;
use Yajra\Datatables\Datatables;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use App\Repositories\AgencyRepository;


class AgencyController extends MainController
{
	public function __construct(AgencyRepository $agency)
    {
    	$this->agency = $agency;
    }
    /**
     * Show a list of all the users.
     *
     * @return View
     */

     public function index()
    {
        //$agencies = $this->agency->getAll();
        return view('agency.index');

    }

    public function agencyData(){
        $agencies = $this->agency->getAll();
        // $agencies = $this->agency->find(1);
        // dd($agencies);
        $datatables = Datatables::of($agencies)
            // ->edit_column()
        // ->add_column('actions',function($agency) {
        //         $actions = '<a href='. route('agency/update', $agency->id) .'><i class="livicon" data-name="edit" data-size="24" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="View"></i>View</a>';
        //         return $actions;
        //     })
        ->add_column('actions',function($agency) {
                $actions = '<a href='. route('agency/update', $agency->id) .'><span class="fa fa-pencil-square-o" style="color:#418BCA; font-size: 22px;"></span></a> &nbsp; <a href="" data-target="#delete_confirm" data-toggle="modal"><span class="fa fa-trash" style="color:#d32f2f; font-size: 22px;"></span></a>';
                return $actions;
            })
        ->editColumn('status', function($agency){
                if ( $agency->status == '0')
                  return 'Inactive';
                else if ( $agency->status == '1')
                  return 'Active';
                else
                  return  " ";       
        })
        ->rawColumns(['actions', 'status'])
        ->make(true);
        return $datatables;
    }


}