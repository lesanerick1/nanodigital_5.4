<?php namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Http\Request;
use Sentinel;
use URL;
use View;
use Session;
use App\Jobs\sendMessage;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\TestRepository;
use App\Repositories\CampaignRepository;
use App\Repositories\GroupRepository;
use App\Repositories\CarrierRepository;


class SocialMediaController extends MainController
{

	public function __construct(TestRepository $test, UserRepository $user, CompanyRepository $company,CampaignRepository $campaign, GroupRepository $group, CarrierRepository $carrier)
    {
        $this->test = $test;
        $this->user = $user;
        $this->company = $company;
        $this->campaign = $campaign;
        $this->group = $group;
        $this->carrier = $carrier;
    }

    public function index()
    {
        $tests = $this->test->findBy('type', '1');
        $campaigns = $this->campaign->findBy('type', '1');
        $groups = $this->group->getAll();
        $carriers = $this->carrier->getAll();

        return View('social-media.index', compact('tests', 'campaigns', 'groups', 'carriers'));
    }
    public function sendSms(Request $request)
    {
      
     
    }

}