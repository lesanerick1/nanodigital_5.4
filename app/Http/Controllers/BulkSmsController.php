<?php namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Http\Request;
use Sentinel;
use URL;
use View;
use Session;
use Charts;
use App\Campaign;
use App\Ad;
use App\Jobs\sendMessage;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\TestRepository;
use App\Repositories\CampaignRepository;
use App\Repositories\GroupRepository;
use App\Repositories\CarrierRepository;
use App\Repositories\CompanyPackageRepository;
use Vinkla\Hashids\Facades\Hashids;
use Yajra\Datatables\Datatables;


class BulkSmsController extends MainController
{

	public function __construct(TestRepository $test, UserRepository $user, CompanyRepository $company,CampaignRepository $campaign, GroupRepository $group, CarrierRepository $carrier, CompanyPackageRepository $company_package)
    {
        $this->test = $test;
        $this->user = $user;
        $this->company = $company;
        $this->campaign = $campaign;
        $this->group = $group;
        $this->carrier = $carrier;
        $this->company_package = $company_package;
    }

    public function index()
    {
        $tests = $this->test->findBy('type', '1');
        $campaigns = $this->campaign->findBy('type', '1');
        $groups = $this->group->getAll();
        $carriers = $this->carrier->getAll();
        $company_packages = $this->company_package->getAll();
        $companies = $this->company->getAll();

        return View('bulk-sms.index', compact('tests', 'campaigns', 'groups', 'carriers', 'company_packages', 'companies'));
    }
    public function sendSms(Request $request)
    {
      
      $phone_nos = explode(",", $request->phone_number);
      $message = $request->message;
      $subject = "";
      $media = "";
      $type = "1";
      foreach($phone_nos as $phone_no) {
        $job = (new sendMessage($phone_no, $message, $subject, $media, $type))->delay(5);
        dispatch($job);
      }
      unset($phone_no);
    }
    public function createCampaign(Request $request)
    {
      /*$campaign = new Campaign();
      $campaign->name = $request->name;
      $campaign->type = 1;
      $campaign->company = $request->company;
      $campaign->content = $request->message;
      $campaign->subject = "";
      $campaign->status = 0;

      $store = $campaign->save();*/
      if(is_array($request->groups))   $groups = implode(',', $request->groups); 
      $groups=explode(',', $groups);      
      if(is_array($request->carriers))   $carriers = implode(',', $request->carriers);
      $carriers=explode(',', $carriers);

      $start_date = strtotime($request->start_custom_date);
      $end_date = ($request->end_date == 0) ? "2019-12-31" : $request->end_date;
      $store = Campaign::create([
                'name' => $request->name,
                'type' => '1',
                'company' => $request->company,
                'content' => $request->message,
                'subject' => "",
                'status' => "0"
            ])->info()->create([
                'broadcast_time' => $request->broadcast_time.":00",
                'start_date' => date('Y-m-d',$start_date),
                'end_date' => $end_date,
                'intervals' => $request->intervals,
                'customer_groups' => "",
                'carriers' => ""
            ]);

    }

    public function campaignData(){
      $company = Sentinel::getUser()->company;
        $campaign_criteria = ['type' => '1', 'company' => $company];
        $campaigns = Campaign::with('rcCompany','info','carrier', 'type')->select('campaigns.*')->where($campaign_criteria)->get();
        $id=1;
        foreach($campaigns as $campaign){
            $campaign['index'] = $id;
            $id++;
        }
        
        return Datatables::of($campaigns)
        ->addColumn('actions', function ($campaigns) {
                return '<a href=' .route('picture-ads.update', $campaigns->id) .' class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
        ->editColumn('run', function($campaigns){
                return '<a href=""><i class="fa fa-envelope-o" style="color:#418BCA; font-size: 22px;"></i></a>';
        })
        ->editColumn('analysis', function($campaigns){
                return '<a href=' .route('bulk-sms.delivery', Hashids::encode($campaigns->id))    .'><span class="fa fa-bar-chart-o" style="color:#418BCA; font-size: 22px;"></span></a>';
        })
        ->editColumn('status', function($campaigns){
                if ( $campaigns->status == '0')
                  return 'Inactive';
                else if ( $campaigns->status == '1')
                  return 'Active';
                else
                  return  " ";       
        })
        ->rawColumns(['actions', 'analysis', 'run', 'status'])
        ->make(true);
    }

    public function deliveryAnalysis(Request $request){
      if(isset($request->id)){
            $encodedId = $request->id;
            $decodedId = Hashids::decode($request->id);
            // dd($decodedId);
            $campaign = $this->campaign->find($decodedId);

            $chart = Charts::database(Ad::where('campaign', $decodedId)
              ->get(), 'pie', 'highcharts')
              ->elementLabel("Number of Messages")
              ->title('Message Deliveries')
              ->responsive(true)
              ->groupBy('delivery_status', null, [1 => 'Delivered', 2 => 'Not Delivered', 4 => 'Queued', ''=> 'Pending']);

            return View('picture-ads.delivery', compact('campaign', 'encodedId', 'chart'));
              // dd($chart->render());
              // echo "<pre>";
              // print_r($chart->render());
              // echo "</pre>";
              // exit;

        }
    }

    public function deliveryData(Request $request){
        

        $decodedId = Hashids::decode($request->id);
        $campaignStats = $this->ad->findBy('campaign', $decodedId);
        return Datatables::of($campaignStats)
        ->editColumn('delivery_status', function ($campaignStats){
            if ( $campaignStats->delivery_status == '1')
                  return 'Delivered';
                else if ( $campaignStats->delivery_status == '')
                  return 'Pending';
                else
                  return  "Sent";       
        })
        ->rawColumns(['delivery_status'])
        ->make(true);

    }

}