<?php namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Http\Request;
use Sentinel;
use URL;
use View;
use Session;
use Storage;
use File;
use Redirect;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use App\Repositories\CustomerRepository;


class CustomersController extends MainController
{
	public function __construct(CustomerRepository $customer)
    {
    	$this->customer = $customer;
    }
    /**
     * Show a list of all the users.
     *
     * @return View
     */

    public function index()
    {
        //$customers = $this->customer->getAll();
        return view('customers.index');
    }

    public function importCustomers(Request $request)
    {      
        $user = Sentinel::getUser();
        $file = $request->file('customer');
        $extension = $file->getClientOriginalExtension();
        Storage::disk('public')->put($file->getClientOriginalName(),  File::get($file));
        $data = $file->getClientOriginalName();
        $job = (new importCustomers($data,$user))->delay(60);
        dispatch($job);

        return Redirect::route('customers')->with('success','Your Subscriber List Successfully has been queued.');
     
    }

    // AJAX function to return all customer data to the Customers DataTable
    public function customerData(){
        // $customers = $this->customer->getAll();
        $customers = $this->customer->getColumns(['name', 'phone_number', 'id', 'country', 'carrier', 'company', 'status']);
        // $customers = $this->customer->paginate();
        // $customers = $this->customer->find(1);
        return Datatables::of($customers)
            // ->edit_column()
        // ->add_column('actions',function($agency) {
        //         $actions = '<a href='. route('agency/update', $agency->id) .'><i class="livicon" data-name="edit" data-size="24" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="View"></i></a>';
        //         return $actions;
        //     })
        ->make(true);
    }


}