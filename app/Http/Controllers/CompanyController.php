<?php namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Http\Request;
use Sentinel;
use URL;
use View;
use Session;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use App\Repositories\CompanyRepository;


class CompanyController extends MainController
{
	public function __construct(CompanyRepository $company)
    {
    	$this->company = $company;
    }
    /**
     * Show a list of all the users.
     *
     * @return View
     */

     public function index()
    {
        $companies = $this->company->getAll();
        return view('company.index', compact('companies'));
    }


}