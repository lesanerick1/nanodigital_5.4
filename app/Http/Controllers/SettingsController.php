<?php namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Http\Request;
use Sentinel;
use URL;
use View;
use Session;
use Redirect;
use App\ShortCode;
use App\Repositories\RoleRepository;
use App\Repositories\ServiceRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\ShortCodeRepository;
use App\Repositories\CampaignRepository;
use App\Repositories\ShortCodeTypeRepository;


class SettingsController extends MainController
{

	public function __construct(ShortCodeRepository $shortcode, ServiceRepository $service, CompanyRepository $company, ShortCodeTypeRepository $shortcodetype)
    {
        $this->shortcode = $shortcode;
        $this->service = $service;
        $this->company = $company;
        $this->shortcodetype = $shortcodetype;
    }
	public function index()
    {
    	$shortcodes = $this->shortcode->getAll();
    	$companies = $this->company->getAll();
        $shortcodetypes = $this->shortcodetype->getAll();

        return View('settings.index', compact('shortcodes', 'companies', 'shortcodetypes'));
    }
    public function createShortCode(Request $request){
    	$this->shortcode->create($request->all());
    	//$this->shortcode->create(Input::all());
    	//return View('index');
    }


}