<?php

namespace App;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Group extends Model implements AuditableContract
{
	use Auditable;
	use softDeletes;
    protected $primaryKey = 'id';
	protected $dates = ['deleted_at'];
    protected $fillable = ['name','slug','company'];

	public function rsCompany()
    {
        return $this->belongsTo('App\Company', 'company');
    }

    

   

}