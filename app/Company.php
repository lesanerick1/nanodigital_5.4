<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class Company extends Model implements AuditableContract
{
	use softDeletes;
	use Auditable;

	protected $primaryKey = 'id';
	protected $dates = ['deleted_at'];
    protected $fillable = ['name','slug','logo'];
	
    public function rcAgency()
    {
        return $this->belongsTo('App\Agency','agency');
    }

    public function rcUser(){
    	return $this->hasMany('App\User', 'company');
    }

    public function shortcode(){
        return $this->hasMany('App\ShortCode', 'company');
    }

    public function services()
    {
        return $this->hasManyThrough('App\Service','App\ShortCode', 'company', 'short_code', 'id');
    }

    public function campaign(){
    	//return $this->hasMany('App\Campaign');
    }

    public function customer(){
    	//return $this->hasMany('App\Customer');
    }

     public function scopeBrandActive($query){

		//return $query->where('status',1);
	}
	public function scopeBrandPaused($query){
		//return $query->where('status',0);
	}
	public function scopeBrandDeleted($query){
		//return $query->where('status',3);
	}


}
