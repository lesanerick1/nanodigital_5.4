<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Files extends Model{


    use softDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'files';
    protected $primaryKey = 'id';
     protected $guarded  = array('id');
     protected $fillable = array('file_name', 'original_name', 'posted_by', 'company');



    // function file()
    // {
    //     return $this-> belongsTo ('App\User');
    // }

    public function rsCompany(){
        return $this->belongsTo('App\Company', 'company');
    }
}