<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Role extends Model implements AuditableContract
{
    //
	use Auditable;

    protected $fillable = array('name', 'slug', 'permissions');

    public function users()
    {
        return $this->belongsToMany('App\User', 'role_users');
    }
}
