<?php

namespace App;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Database\Eloquent\Model;

class ProductType extends Model implements AuditableContract
{
	use Auditable;

	protected $table = 'product_types';
    protected $primaryKey = 'id';
    protected $fillable = ['name'];

    public function rcCampaign(){
    	return $this->hasMany('App\Campaign');
    }

	  

}


