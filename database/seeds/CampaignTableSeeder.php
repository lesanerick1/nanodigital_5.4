<?php

use App\Campaign;
use App\CampaignInfo;
use Illuminate\Database\Seeder;

class CampaignTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $response = Campaign::create([
                'name' => 'Test1',
                'type' => '1',
                'company' => '1',
                'content' => '',
                'subject' => '',
                'status' => '1'
            ]);

        $info = CampaignInfo::create([
                'campaign' => $response->id,
                'broadcast_time' => '08:00',
                'start_date' => '2029-12-31',
                'end_date' => '2029-12-31'
            ]);
        $campaign = new Campaign();
        $campaign_id = $campaign->find($response->id);
        $carrier = ['5', '6'];
        $campaign_id->carrier()->attach($carrier);
        //$customer_group = '5';
        //$campaign_id->customer_group()->attach($customer_group);
        $campaign_id->save();
    }
}
