<?php

use Illuminate\Database\Seeder;
use App\CarrierCode;

class CarrierCodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('carrier_codes')->delete();

        // add carrier codes
        CarrierCode::create([
            'carrier' => '1',
            'dialing_code' => '70'
            ]
        );
        CarrierCode::create([
            'carrier' => '1',
            'dialing_code' => '71'
            ]
        );
        CarrierCode::create([
            'carrier' => '1',
            'dialing_code' => '72'
            ]
        );
        CarrierCode::create([
            'carrier' => '2',
            'dialing_code' => '73'
            ]
        );
        CarrierCode::create([
            'carrier' => '1',
            'dialing_code' => '74'
            ]
        );
        CarrierCode::create([
            'carrier' => '2',
            'dialing_code' => '75'
            ]
        );
        CarrierCode::create([
            'carrier' => '4',
            'dialing_code' => '76'
            ]
        );
        CarrierCode::create([
            'carrier' => '3',
            'dialing_code' => '77'
            ]
        );
        CarrierCode::create([
            'carrier' => '2',
            'dialing_code' => '78'
            ]
        );
        CarrierCode::create([
            'carrier' => '1',
            'dialing_code' => '79'
            ]
        );
    }
}
