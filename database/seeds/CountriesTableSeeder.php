<?php

use Illuminate\Database\Seeder;
use App\Country;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    //DB::table('countries')->delete();
    public function run()
    {
        // add country
        Country::create([
            'name' => 'Kenya',
            'iso_code' => 'KE',
            'calling_code' => '254'
            ]
        );
        Country::create([
            'name' => 'Uganda',
            'iso_code' => 'UG',
            'calling_code' => '256'
            ]
        );
        Country::create([
            'name' => 'Tanzania',
            'iso_code' => 'TZ',
            'calling_code' => '255'
            ]
        );
        Country::create([
            'name' => 'Rwanda',
            'iso_code' => 'RW',
            'calling_code' => '250'
            ]
        );
    }
}
