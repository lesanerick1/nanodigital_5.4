<?php

use Illuminate\Database\Seeder;
use App\Carrier;

class CarriersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('carriers')->delete();

        // add carriers
        Carrier::create([
            'name' => 'Safaricom',
            'slug' => 'safaricom',
            'country' => '1'
            ]
        );
        Carrier::create([
            'name' => 'Airtel Kenya',
            'slug' => 'airtel-kenya',
            'country' => '1'
            ]
        );
        Carrier::create([
            'name' => 'Telkom Kenya',
            'slug' => 'telkom-kenya',
            'country' => '1'
            ]
        );
        Carrier::create([
            'name' => 'Equitel',
            'slug' => 'equitel',
            'country' => '1'
            ]
        );
    }
}
