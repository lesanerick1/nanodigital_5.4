<?php

use Illuminate\Database\Seeder;
use App\ShortCodeType;

class ShortCodeTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('short_code_types')->delete();

        // add shortcode type
        ShortCodeType::create([
            'name' => 'Dedicated'
            ]
        );
        ShortCodeType::create([
            'name' => 'Shared'
            ]
        );
    }
}
