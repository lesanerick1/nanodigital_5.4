<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('users')->delete();

        // add user roles
        Role::create([
            'name' => 'Agency Admin',
            'slug' => 'agency-admin',
            'permissions' => ''
            ]
        );
        Role::create([
            'name' => 'Company Admin',
            'slug' => 'company-admin',
            'permissions' => ''
            ]
        );
    }
}
