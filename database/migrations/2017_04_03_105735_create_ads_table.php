<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->string('content');
            $table->string('subject')->nullable();
            $table->integer('type')->unsigned();
            $table->foreign('type')->references('id')->on('service_types')->onUpdate('cascade')->onDelete('cascade');
            $table->string('sender');
            $table->string('sender_short_code');
            $table->string('receiver');
            $table->datetime('broadcast_time')->nullable();
            $table->datetime('delivery_time')->nullable();
            $table->datetime('read_time')->nullable();
            $table->string('message_id')->nullable();
            $table->integer('carrier')->unsigned();
            $table->foreign('carrier')->references('id')->on('carriers')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('campaign')->unsigned();
            $table->foreign('campaign')->references('id')->on('campaigns')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('company')->unsigned();
            $table->foreign('company')->references('id')->on('companies')->onUpdate('cascade')->onDelete('cascade');
            $table->string('delivery_status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('ads');
    }
}
