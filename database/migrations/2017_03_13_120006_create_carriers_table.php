<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('carriers', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->string('name');
            $table->string('slug')->nullable();
            $table->integer('country')->unsigned();
            $table->foreign('country')->references('id')->on('countries')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            
            $table->unique('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('carriers');
    }
}
