<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('short_code')->unsigned();
            $table->foreign('short_code')->references('id')->on('short_codes')->onUpdate('cascade')->onDelete('cascade');
            $table->string('name');
            $table->string('sp_id');
            $table->string('sp_password');
            $table->string('service_id');
            $table->integer('type')->unsigned();
            $table->foreign('type')->references('id')->on('service_types')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('services');
    }
}
