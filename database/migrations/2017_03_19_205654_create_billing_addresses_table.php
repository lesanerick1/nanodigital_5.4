<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('billing_addresses', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('company')->unsigned();
            $table->foreign('company')->references('id')->on('companies')->onUpdate('cascade')->onDelete('cascade');
            $table->string('box')->nullable();
            $table->string('telephone')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('billing_addresses');
    }
}
