<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('customer_info', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('customer')->unsigned();
            $table->foreign('customer')->references('id')->on('customers')->onUpdate('cascade')->onDelete('cascade');
            $table->string('id_number')->nullable();
            $table->integer('age')->nullable();
            $table->string('gender')->nullable();
            $table->date('birthday')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('opt_in_date')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unique('id_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('customer_info');
    }
}
