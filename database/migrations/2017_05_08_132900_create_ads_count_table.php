<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsCountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('ads_count', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->string('sent_count')->nullable();
            $table->string('delivery_count')->nullable();
            $table->string('rollback_count')->nullable();
            $table->integer('campaign')->unsigned();
            $table->foreign('campaign')->references('id')->on('campaigns')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('company')->unsigned();
            $table->foreign('company')->references('id')->on('companies')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('ads_count');
    }
}
