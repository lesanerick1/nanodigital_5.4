<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->string('name');
            $table->integer('type')->unsigned();
            $table->foreign('type')->references('id')->on('service_types')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('company')->unsigned();
            $table->foreign('company')->references('id')->on('companies')->onUpdate('cascade')->onDelete('cascade');
            $table->string('content');
            $table->string('subject')->nullable();
            $table->boolean('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('campaigns');
    }
}
