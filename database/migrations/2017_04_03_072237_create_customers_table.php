<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->string('phone_number');
            $table->string('name');
            $table->integer('country')->unsigned();
            $table->foreign('country')->references('id')->on('countries')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('carrier')->unsigned();
            $table->foreign('carrier')->references('id')->on('carriers')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('company')->unsigned();
            $table->foreign('company')->references('id')->on('companies')->onUpdate('cascade')->onDelete('cascade');
            $table->boolean('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('customers');
    }
}
