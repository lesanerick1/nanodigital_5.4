<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarrierCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('carrier_codes', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('carrier')->unsigned();
            $table->foreign('carrier')->references('id')->on('carriers')->onUpdate('cascade')->onDelete('cascade');
            $table->string('dialing_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('carrier_codes');
    }
}
