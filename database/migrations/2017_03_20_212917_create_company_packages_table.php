<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('company_packages', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('company')->unsigned();
            $table->foreign('company')->references('id')->on('companies')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('type')->unsigned();
            $table->foreign('type')->references('id')->on('service_types')->onUpdate('cascade')->onDelete('cascade');
            $table->string('number_of_messages');
            $table->date('date_bought');
            $table->string('validity');
            $table->string('unit_price');
            $table->string('amount_paid');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('company_packages');
    }
}
