<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('campaign_info', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('campaign')->unsigned();
            $table->foreign('campaign')->references('id')->on('campaigns')->onUpdate('cascade')->onDelete('cascade');
            $table->time('broadcast_time');
            $table->date('start_date');
            $table->date('end_date')->default('2029-12-31');
            $table->string('intervals')->nullable();
            $table->string('locations')->nullable();
            $table->string('subscriptions')->nullable();
            // $table->string('customer_groups')->nullable();
            // $table->string('carriers')->nullable();
            $table->string('age_from')->nullable();
            $table->string('age_to')->nullable();
            $table->string('gender')->nullable();
            $table->string('interests')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('campaign_info');
    }
}
