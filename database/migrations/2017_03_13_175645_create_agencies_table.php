<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('agencies', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->string('name');
            $table->string('slug');
            $table->string('logo')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unique('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('agencies');
    }
}
