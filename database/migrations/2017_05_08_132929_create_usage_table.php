<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('usage', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->string('no_of_customers')->nullable();
            $table->string('no_of_deliveries')->nullable();
            $table->string('no_of_rollbacks')->nullable();
            $table->integer('campaign')->unsigned();
            $table->foreign('campaign')->references('id')->on('campaigns')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('company')->unsigned();
            $table->foreign('company')->references('id')->on('companies')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('ads_count');
    }
}
