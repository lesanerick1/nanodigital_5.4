<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->string('name');
            $table->integer('type')->unsigned();
            $table->foreign('type')->references('id')->on('service_types')->onUpdate('cascade')->onDelete('cascade');
            $table->string('number_of_messages');
            $table->string('validity');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('packages');
    }
}
