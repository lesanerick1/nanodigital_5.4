<?php

namespace RachidLaasri\LaravelInstaller\Helpers;

use Exception;
use Illuminate\Http\Request;

class EnvironmentManager
{
    /**
     * @var string
     */
    private $envPath;

    /**
     * @var string
     */
    private $envExamplePath;

    /**
     * Set the .env and .env.example paths.
     */
    public function __construct()
    {
        $this->envPath = base_path('.env');
        $this->envExamplePath = base_path('.env.example');
    }

    /**
     * Get the content of the .env file.
     *
     * @return string
     */
    public function getEnvContent()
    {
        if (!file_exists($this->envPath)) {
            if (file_exists($this->envExamplePath)) {
                copy($this->envExamplePath, $this->envPath);
            } else {
                touch($this->envPath);
            }
        }

        //return file_get_contents($this->envPath);

        $data = file_get_contents($this->envPath);
        $convert = explode("\n", $data); //create array separate by new line

        for ($i=0;$i<count($convert);$i++) 
        {
            $convert[$i].', '; //write value by index
        }
        $host = substr($convert[8], 8 );
        $username = substr($convert[10], 12 );
        $password = substr($convert[11], 12 );
        $db_name = substr($convert[12], 12 );
        return array('host' => $host, 'username' => $username, 'password' => $password, 'db_name' => $db_name);
    }

    /**
     * Save the edited content to the file.
     *
     * @param Request $input
     * @return string
     */
    public function saveFile(Request $input)
    {
        $message = trans('messages.environment.success');

        try {
            file_put_contents($this->envPath, $input->get('envConfig'));
        }
        catch(Exception $e) {
            $message = trans('messages.environment.errors');
        }

        return $message;
    }
}