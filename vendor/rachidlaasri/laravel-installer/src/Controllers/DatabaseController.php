<?php

namespace RachidLaasri\LaravelInstaller\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Artisan;
use File;
use Sentinel;
use App\User;
use App\Agency;
use RachidLaasri\LaravelInstaller\Helpers\DatabaseManager;
use RachidLaasri\LaravelInstaller\Helpers\InstalledFileManager;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use App\Repositories\RoleRepository;

class DatabaseController extends Controller
{

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @param DatabaseManager $databaseManager
     */
    public function __construct(DatabaseManager $databaseManager, RoleRepository $role)
    {
        $this->databaseManager = $databaseManager;
        $this->envPath = base_path('.env');
        $this->envExamplePath = base_path('.env.example');
        $this->role = $role;
    }

    /**
     * Migrate and seed the database.
     *
     * @return \Illuminate\View\View
     */
    public function database()
    {
        $response = $this->databaseManager->migrateAndSeed();

        return redirect()->route('LaravelInstaller::final')
                         ->with(['message' => $response]);
    }

    public function configuration()
    {
        $this->envPath = base_path('.env');
        $this->envExamplePath = base_path('.env.example');

        if (!file_exists($this->envPath)) {
            if (file_exists($this->envExamplePath)) {
                copy($this->envExamplePath, $this->envPath);
            } else {
                touch($this->envPath);
            }
        }

        //return file_get_contents($this->envPath);

        $data = file_get_contents($this->envPath);
        $convert = explode("\n", $data); //create array separate by new line

        for ($i=0;$i<count($convert);$i++) 
        {
            $convert[$i].', '; //write value by index
        }
        $host = substr($convert[7], 8 );
        $username = substr($convert[10], 12 );
        $password = substr($convert[11], 12 );
        $db_name = substr($convert[9], 12 );
        
        return view('vendor.installer.db-configuration', compact('host', 'username', 'password', 'db_name') );
    }

    public function administrator()
    {
        $roles = $this->role->getAll();
        return view('vendor.installer.administrator', compact('roles'));
    }

    public function save(Request $request, Redirector $redirect)
    {   
        $host = $request->host;
        $username = $request->username;
        $password = $request->password;
        $db_name = $request->db_name;

        //$envPath = base_path(' - Copy.env');
        //$envOldPath = base_path('old.env');
        copy($this->envExamplePath, $this->envPath);
        File::delete($this->envPath);
        $data = file_get_contents($this->envExamplePath);
        $convert = explode("\n", $data); //create array separate by new line

        for ($i=0;$i<count($convert);$i++) 
        {
            if (starts_with($convert[$i], 'DB_HOST')){
                $convert[$i] = "DB_HOST=".$host;
            }
            else if (starts_with($convert[$i], 'DB_DATABASE')){
                $convert[$i] = "DB_DATABASE=".$db_name;
            }
            else if (starts_with($convert[$i], 'DB_USERNAME')){
                $convert[$i] = "DB_USERNAME=".$username;
            }
            else if (starts_with($convert[$i], 'DB_PASSWORD')){
                $convert[$i] = "DB_PASSWORD=".$password;
            }
           //echo $convert[$i].'<br>'; //write value by index
           $contents = $convert[$i]."\n";
           //echo $contents;
           //if (strcmp())
           File::append($this->envPath, $contents);
        }
        
        Artisan::call('key:generate');
        $response = $this->databaseManager->migrateAndSeed();
        
        //echo $response;
        return $redirect->route('LaravelInstaller::administrator')
                        ->with(['message' => $response]);
    }

    public function saveAdmin(Request $request, InstalledFileManager $fileManager)
    {
        $company = $request->company;
        $role = $request->role;
        $name = $request->name;
        $email = $request->email;
        $password = $request->password;

        $response = Agency::create([
                'name' => $company,
                'slug' => str_slug($company,'-')
            ])->rsCompany()->create([
                'name' => $company,
                'slug' => str_slug($company,'-')
            ])->rcUser()->create([
            'email' => $email,
            'password' => bcrypt($password),
            'first_name' => $name,
        ])->roles()->attach($role);

        $user = Sentinel::findById(1);

        $activation = Activation::create($user);

        $fileManager->update();
        return view('vendor.installer.completed');
    }
}
