<?php

Route::group(['prefix' => 'install', 'as' => 'LaravelInstaller::', 'namespace' => 'RachidLaasri\LaravelInstaller\Controllers', 'middleware' => 'web'], function()
{
    Route::group(['middleware' => 'canInstall'], function()
    {
        Route::get('/', [
            'as' => 'welcome',
            'uses' => 'WelcomeController@welcome'
        ]);

        Route::get('environment', [
            'as' => 'environment',
            'uses' => 'EnvironmentController@environment'
        ]);

        Route::post('environment/save', [
            'as' => 'environmentSave',
            'uses' => 'EnvironmentController@save'
        ]);

        Route::get('requirements', [
            'as' => 'requirements',
            'uses' => 'RequirementsController@requirements'
        ]);
        
        Route::get('permissions', [
            'as' => 'permissions',
            'uses' => 'PermissionsController@permissions'
        ]);

        Route::get('database', [
            'as' => 'database',
            'uses' => 'DatabaseController@database'
        ]);

        Route::get('db-configuration', [
            'as' => 'db-configuration',
            'uses' => 'DatabaseController@configuration'
        ]);

        Route::post('db-configuration/save', [
            'as' => 'db-configurationSave',
            'uses' => 'DatabaseController@save'
        ]);

        Route::get('administrator', [
            'as' => 'administrator',
            'uses' => 'DatabaseController@administrator'
        ]);
        Route::post('administrator/save', [
            'as' => 'saveAdmin',
            'uses' => 'DatabaseController@saveAdmin'
        ]);
        Route::get('final', [
            'as' => 'final',
            'uses' => 'FinalController@finish'
        ]);
    });
});
