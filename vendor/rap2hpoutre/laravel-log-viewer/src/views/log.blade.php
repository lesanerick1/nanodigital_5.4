@extends('layouts.master')
@section('title') 
Logs | Nano Digital 
@stop 
@section('header_styles')
<link rel="stylesheet" href="{{ asset('assets/css/lib/datatables-net/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/datatables-net.min.css') }}">
@stop
@section('content')
<div class="page-content">
  <div class="container-fluid">
      <header class="section-header">
        <div class="tbl">
          <div class="tbl-row">
            <div class="tbl-cell">
              <h2>Logs</h2>
              <div class="subtitle"></div>
              <div class="tools pull-right"></div>
                <div class="pull-right">
                   <a href="?dl={{ base64_encode($current_file) }}" class="btn btn-default"><span class="glyphicon glyphicon-download-alt"></span> Download file</a> 
                    <a id="delete-log" href="?del={{ base64_encode($current_file) }}" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete file</a>
                </div>
            </div>
            </div>
          </div>
        </div>
      </header>
      <section class="card">
        <div class="card-block">
          <div class="row">
            <div class=" col-md-2">
              <div class="list-group">
                @foreach($files as $file)
                <a href="?l={{ base64_encode($file) }}" class="list-group-item @if ($current_file == $file) llv-active @endif">{{$file}}</a>
                @endforeach
              </div>
            </div>
            <div class="col-md-10 table-container" style="float:right;padding-top:0 !important;">
              @if ($logs === null)
            <div>
            Log file >50M, please download it.
          </div>
          @else
          <table id="table-log" class="display table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Level</th>
                <th>Context</th>
                <th>Date</th>
                <th>Content</th>
              </tr>
            </thead>
            <tbody>

              @foreach($logs as $key => $log)
              <tr>
                <td class="text-{{{$log['level_class']}}}"><span class="glyphicon glyphicon-{{{$log['level_img']}}}-sign" aria-hidden="true"></span> &nbsp;{{$log['level']}}</td>
                <td class="text">{{$log['context']}}</td>
                <td class="date">{{{$log['date']}}}</td>
                <td class="text">
                  @if ($log['stack']) <a class="pull-right expand btn btn-default btn-xs" data-display="stack{{{$key}}}"><span class="glyphicon glyphicon-search"></span></a>@endif {{{$log['text']}}} @if (isset($log['in_file']))
                  <br />{{{$log['in_file']}}}@endif @if ($log['stack'])
                  <div class="stack" id="stack{{{$key}}}" style="display: none; white-space: pre-wrap;">{{{ trim($log['stack']) }}}</div>@endif
                </td>
              </tr>
              @endforeach

            </tbody>
          </table>
          @endif
        </div>
      </section>
    </div><!--.container-fluid-->
  </div><!--.page-content-->
@stop
@section('footer_scripts')
<script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script>
<script>
  $(document).ready(function() {
    $('#table-log').DataTable({
      "order": [1, 'desc'],
      "stateSave": true,
      "stateSaveCallback": function(settings, data) {
        window.localStorage.setItem("datatable", JSON.stringify(data));
      },
      "stateLoadCallback": function(settings) {
        var data = JSON.parse(window.localStorage.getItem("datatable"));
        if (data) data.start = 0;
        return data;
      }
    });
    $('.table-container').on('click', '.expand', function() {
      $('#' + $(this).data('display')).toggle();
    });
    $('#delete-log').click(function() {
      return confirm('Are you sure you want to delete the log file?');
    });
  });

</script>
@stop
