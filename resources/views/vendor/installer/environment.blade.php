@extends('vendor.installer.layouts.master')

@section('title', trans('installer_messages.environment.title'))
@section('container')
<section class="box-typical steps-icon-block">
    <div class="row">
    @include('vendor.installer.layouts.progress')
    <div class="col-sm-10 offset-sm-1">
        <header class="steps-numeric-title "><strong>{{ trans('installer_messages.pre-installation.message') }}</strong></header>
        <hr>
        @if($php_version >= $min_version)
            <p class="label label-success">{{ trans('installer_messages.pre-installation.check') }}</p>
        @else
            <p class="label label-danger">{{ trans('installer_messages.pre-installation.fail') }}</p>
        @endif
        <p>Minimum Requirements </p>
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Required</th>
                        <th>Current</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>PHP Version</td>
                        <td>{{ $min_version }}</td>                        
                        <td><span class="label label-{{ ($php_version > $min_version) ? 'success' : 'error' }}">{{ $php_version }}</span> </td>
                    </tr>
                    <tr>
                        <td>PHP PDO driver library</td>
                        <td>At least one</td>                        
                        <td><span class="label label-success">{{ $pdo_libs }}</span></td>
                    </tr>
                    <tr>
                        <td>PHP mbstring Library</td>
                        <td align="center">
                            <div class="checkbox-bird green">
                                <input type="checkbox" disabled id="check-bird-11" checked/>
                                <label for="check-bird-11"></label>
                            </div> 
                        </td>                        
                        <td align="center">
                            <div class="checkbox-bird green">
                                <input type="checkbox"  disabled id="check-bird-11" checked/>
                                <label for="check-bird-11"></label>
                            </div>  
                        </td>
                    </tr>
                    <!--<tr>
                        <td>Minimum memory available</td>
                        <td>512MB</td>                        
                        <td>
                            
                        </td>
                    </tr>-->
                    
                    <!--<tr>
                        <td>/bootstrap directory</td>
                        <td>755</td>                        
                        <td>
                            
                        </td>
                    </tr>-->
                    <tr>
                        <td>Session writable</td>
                        <td align="center">
                            <div class="checkbox-bird green">
                                <input type="checkbox" disabled id="check-bird-11" checked/>
                                <label for="check-bird-11"></label>
                            </div> 
                        </td>                        
                        <td align="center">
                            <div class="checkbox-bird green">
                                <input type="checkbox" disabled id="check-bird-11" checked/>
                                <label for="check-bird-11"></label>
                            </div> 
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
            <p>Optional Requirements </p>
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Recommended</th>
                        <th>Current</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>PHP zip library</td>
                        <td align="center">
                            <div class="checkbox-bird green">
                                <input type="checkbox" id="check-bird-11" checked/>
                                <label for="check-bird-11"></label>
                            </div> 
                        </td>                        
                        <td align="center">
                            <div class="checkbox-bird green">
                                <input type="checkbox" id="check-bird-11" checked/>
                                <label for="check-bird-11"></label>
                            </div>  
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <hr>
        <a href="{{ route('LaravelInstaller::welcome') }}" class="btn btn-grey float-left">← {{ trans('installer_messages.back') }}</a>
        @if( ! isset($environment['errors']))
            <a href="{{ route('LaravelInstaller::db-configuration') }}" class="btn float-right">{{ trans('installer_messages.next') }} →</a>
        @endif
    </div>
    </div>
</section><!--.steps-icon-block-->
@stop
