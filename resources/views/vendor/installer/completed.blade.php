@extends('vendor.installer.layouts.master')

@section('title', trans('installer_messages.completed.title'))
@section('container')
<section class="box-typical steps-icon-block">
    <div class="row">
    <div class="col-sm-10 offset-sm-1">
        <header class="steps-numeric-title "><strong>{{ trans('installer_messages.completed.message') }}</strong></header>
        <hr>
        <strong></strong>
        <p>&nbsp;</p>
        <p>{{ trans('installer_messages.completed.text') }}</p>
        <hr>
        <a href="/signin" class="btn float-right">{{ trans('installer_messages.completed.login') }} →</a>
    </div>
    </div>
</section><!--.steps-icon-block-->
@stop
