@extends('vendor.installer.layouts.master')

@section('title', trans('installer_messages.welcome.title'))
@section('container')
    <div class="col-sm-10 offset-sm-1">
        <header class="steps-numeric-title ">{{ trans('installer_messages.welcome.message') }}</header>
        <div class="form-group row">
            <label for="exampleSelect" class="col-sm-10 offset-sm-1 form-control-label">Please select your preferred Language:</label>
            <div class="col-sm-10">
                <select id="exampleSelect" class="form-control">
                    <option selected="selected">English</option>
                </select>
            </div>
        </div>
        <strong>Your preferred language will be used through out the installation process.</strong>
        <a href="{{ route('LaravelInstaller::environment') }}" class="btn float-right">{{ trans('installer_messages.next') }} →</a>
    </div>
@stop
