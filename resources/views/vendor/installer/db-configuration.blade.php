@extends('vendor.installer.layouts.master')

@section('title', trans('installer_messages.administrator.title'))
@section('container')
<section class="box-typical steps-icon-block">
    <div class="row">
    @include('vendor.installer.layouts.progress') 
    <div class="col-sm-10 offset-sm-1">
        <header class="steps-numeric-title "><strong>{{ trans('installer_messages.db-configuration.message') }}</strong></header>
        <hr>
        <p class="label label-success">{{ trans('installer_messages.db-configuration.dba') }}</p>
        <p class="paragraph">{{ session('message')['message'] }}</p>
        <p>Please enter the database settings you want to use for NanoDigital.<br />Please note that all fields marked with * are mandatory. </p>
        <form method="post" action="{{ route('LaravelInstaller::db-configurationSave') }}">
        {{ csrf_field() }}
        <div class="form-group row">
            <label class="col-sm-4 form-control-label">Database Type (*)</label>
            <div class="col-sm-8">
                <fieldset class="form-group">
                    <p class="form-control-static">
                        <select id="exampleSelect" required class="form-control">
                            <option selected="selected">MySQL</option>
                            <!--<option >Postgre SQL</option>
                            <option >ORACLE</option>-->
                        </select>
                        </p>
                    <small class="text-muted">Database Type. Default MySQL</small>
                </fieldset>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-4 form-control-label">Database Host (*)</label>
            <div class="col-sm-8">
                <p class="form-control-static"><input type="text" class="form-control" name="host" required value="{{ $host }}" placeholder=""></p>
                <small class="text-muted">Set this to the IP location of your database server or leave it as localhost if you are running on the server itself. If the database is using a custom port, attach it using a semicolon e.d db.host.com:1234</small>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-4 form-control-label">Database User (*)</label>
            <div class="col-sm-8">
                <p class="form-control-static"><input type="text" class="form-control" name="username" required value="{{ $username }}" placeholder=""></p>
                <small class="text-muted">Your database server user name.</small>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-4 form-control-label">Database Password (*)</label>
            <div class="col-sm-8">
                <p class="form-control-static"><input id="hide-show-password" type="password" name="password" required class="form-control" value="{{ $password }}" placeholder=""></p>
                <small class="text-muted">Yout database server password.</small>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-4 form-control-label">Database Name (*)</label>
            <div class="col-sm-8">
                <p class="form-control-static"><input type="text" class="form-control" name="db_name" required value="{{ $db_name }}" placeholder=""></p>
                <small class="text-muted">Your database name. Make sure the user specified above has the necessary database privileges.</small>
            </div>
        </div>
        <hr>
        <a href="{{ route('LaravelInstaller::environment') }}" class="btn btn-grey float-left">← {{ trans('installer_messages.back') }}</a>
        <button type="submit" class="btn float-right">{{ trans('installer_messages.next') }} →</button>
        </form>
    </div>
    </div>
</section><!--.steps-icon-block-->
@stop