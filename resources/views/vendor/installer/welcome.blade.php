@extends('vendor.installer.layouts.master')

@section('title', trans('installer_messages.welcome.title'))
@section('container')
<section class="box-typical steps-icon-block">
    <div class="row">
    @include('vendor.installer.layouts.progress')
    <div class="col-sm-10 offset-sm-1">
        <header class="steps-numeric-title "><strong>{{ trans('installer_messages.welcome.message') }}</strong></header>
        <hr>
        <div class="form-group row">
            <label for="exampleSelect" class="col-sm-6 offset-sm-1 form-control-label">Please select your preferred Language:</label>
            <div class="col-sm-5">
                <select id="exampleSelect" class="form-control">
                    <option selected="selected">English</option>
                </select>
            </div>
        </div>
        <strong>Your preferred language will be used through out the installation process.</strong>
        <p>&nbsp;</p>
        <p>{{ trans('installer_messages.welcome.start') }}</p>
        <hr>
        <a href="{{ route('LaravelInstaller::environment') }}" class="btn float-right">{{ trans('installer_messages.next') }} →</a>
    </div>
    </div>
</section><!--.steps-icon-block-->
@stop
