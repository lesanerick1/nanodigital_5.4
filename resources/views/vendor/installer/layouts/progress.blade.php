<div class="steps-icon-progress">
    <ul>
        <li class="{{ isActive('LaravelInstaller::welcome') }} {{ isActive('LaravelInstaller::environment') }} {{ isActive('LaravelInstaller::db-configuration') }} {{ isActive('LaravelInstaller::administrator') }}">
            <div class="icon">
                <i class="font-icon font-icon-cart-2"></i>
            </div>
            <div class="caption">Welcome</div>
        </li>
        <li class="{{ isActive('LaravelInstaller::environment') }} {{ isActive('LaravelInstaller::db-configuration') }} {{ isActive('LaravelInstaller::administrator') }}">
            <div class="icon">
                <i class="font-icon font-icon-pin-2"></i>
            </div>
            <div class="caption">Pre-Installation Check</div>
        </li>
        <li class="{{ isActive('LaravelInstaller::db-configuration') }} {{ isActive('LaravelInstaller::administrator') }}">
            <div class="icon">
                <i class="font-icon font-icon-card"></i>
            </div>
            <div class="caption">Database Configuration</div>
        </li>
        <li class="{{ isActive('LaravelInstaller::administrator') }}" >
            <div class="icon">
                <i class="font-icon font-icon-check-bird"></i>
            </div>
            <div class="caption">Administrator</div>
        </li>
    </ul>
</div>