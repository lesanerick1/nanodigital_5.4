@extends('vendor.installer.layouts.master')

@section('title', trans('installer_messages.administrator.title'))
@section('container')
<section class="box-typical steps-icon-block">
    <div class="row">
    @include('vendor.installer.layouts.progress') 
    <div class="col-sm-10 offset-sm-1">
        <header class="steps-numeric-title "><strong>{{ trans('installer_messages.administrator.message') }}</strong></header>
        <hr>
        <p class="paragraph">{{ session('message')['message'] }}</p>
        <p>Configure the system administrator. </p>
        <form method="post" action="{{ route('LaravelInstaller::saveAdmin') }}">
        {{ csrf_field() }}
        <div class="form-group row">
            <label class="col-sm-4 form-control-label">Company Name (*)</label>
            <div class="col-sm-8">
                <fieldset class="form-group">
                    <p class="form-control-static"><input type="text" name="company" class="form-control" id="" required placeholder=""></p>
                    <small class="text-muted"></small>
                </fieldset>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-4 form-control-label">User Role (*)</label>
            <div class="col-sm-8">
                <fieldset class="form-group">
                    <p class="form-control-static">
                        <select name="role" class="form-control" required >
                            <option value="" selected="selected" >Select Role</option>
                            @foreach($roles as $role)
                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </p>
                    <small class="text-muted"></small>
                </fieldset>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-4 form-control-label">Admin Name (*)</label>
            <div class="col-sm-8">
                <fieldset class="form-group">
                    <p class="form-control-static"><input type="text" name="name" class="form-control" id="" required placeholder=""></p>
                    <small class="text-muted"></small>
                </fieldset>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-4 form-control-label">Admin Email (*)</label>
            <div class="col-sm-8">
                <p class="form-control-static"><input type="email" name="email" class="form-control" id="" required value="" placeholder=""></p>
                <small class="text-muted"></small>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-4 form-control-label">Admin Password (*)</label>
            <div class="col-sm-8">
                <p class="form-control-static"><input id="hide-show-password" name="password" type="password" class="form-control" required  value="" placeholder=""></p>
                <small class="text-muted"></small>
            </div>
        </div>
        <!--<div class="form-group row">
            <label class="col-sm-4 form-control-label">Confirm Admin Password (*)</label>
            <div class="col-sm-8">
                <p class="form-control-static"><input id="hide-show-password2" type="password" class="form-control" required  value="" placeholder=""></p>
                <small class="text-muted"></small>
            </div>
        </div>-->
        <hr>
        <a href="{{ route('LaravelInstaller::db-configuration') }}" class="btn btn-grey float-left">← {{ trans('installer_messages.back') }}</a>
        <button type="submit" class="btn float-right">{{ trans('installer_messages.finish') }} →</button>
        </form>
    </div>
    </div>
</section><!--.steps-icon-block-->
@stop