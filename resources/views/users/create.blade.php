@extends('layouts.master')

@section('header_scripts')
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/jquery-steps.min.css') }}">
@stop

@section('content')

<div class="page-content">
	
    <div class="container-fluid">
		<section class="box-typical box-panel">
			<header class="box-typical-header">
				<div class="tbl-row">
					<div class="tbl-cell tbl-cell-title">
						<h3>Add New User</h3>
					</div>
				</div>
			</header>
			<div class="box-typical-body">
				<form id="example-form" action="#" class="form-wizard" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
					<div>
						<h3>Personal Details</h3>
						<section>
							<div class="form-group row ">
                                <label for="first_name" class="col-sm-2 offset-sm-1 control-label">Name *</label>
                                <div class="col-sm-9">
                                    <input id="first_name" name="first_name" type="text"
                                           placeholder="First Name" class="form-control "
                                           value=""/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-2 offset-sm-1 control-label">Email *</label>
                                <div class="col-sm-9">
                                    <input id="email" name="email" placeholder="E-Mail Address" type="text"
                                           class="form-control  email" value=""/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-sm-2 offset-sm-1 control-label">Password *</label>
                                <div class="col-sm-9">
                                    <input id="password" name="password" type="password" placeholder="Password"
                                           class="form-control " value=""/>
                                </div>
                            </div>
							<div class="form-group row">
								<label class="col-sm-3 offset-sm-1 form-control-label" for="exampleInputPassword1">Company</label>
								<div class="col-sm-8">
									<select class="select2-photo">
										<option data-photo="img/photo-64-1.jpg">Jenny Hess</option>
										<option data-photo="img/photo-64-2.jpg">Elliot Fu</option>
										<option data-photo="img/photo-64-3.jpg">Stevie Felicano</option>
										<option data-photo="img/photo-64-4.jpg">Christian</option>
									</select>
								</div>
								<br/>
								<br/>
							</div>
							<div class="form-group row">
								<fieldset >
							        <legend>Checkbox</legend>
							        <ul>
							            <li><input id="checkbox1" name="checkbox" type="checkbox" checked="checked"> <label for="checkbox1">Choice A</label></li>
							            <li><input id="checkbox2" name="checkbox" type="checkbox"> <label for="checkbox2">Choice B</label></li>
							            <li><input id="checkbox3" name="checkbox" type="checkbox"> <label for="checkbox3">Choice C</label></li>
							        </ul>
							    </fieldset>
  
							</div>
						</section>
						<h3>User Role</h3>
						<section>
							@include('layouts.permissions');
						</section>
					</div>
				</form>
			</div><!--.box-typical-body-->
		</section>

		<section class="card">
			
		</section>
	</div>
</div>
@stop

@section('footer_scripts')
<script src="{{ asset('assets/js/lib/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/jquery-steps/jquery.steps.min.js') }}"></script>
<script>
	$(function() {
		$("#example-basic ").steps({
			headerTag: "h3",
			bodyTag: "section",
			transitionEffect: "slideLeft",
			autoFocus: true
		});

		var form = $("#example-form");
		form.validate({
			rules: {
				agree: {
					required: true
				}
			},
			errorPlacement: function errorPlacement(error, element) { element.closest('.form-group').find('.form-control').after(error); },
			highlight: function(element) {
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			}
		});

		form.children("div").steps({
			headerTag: "h3",
			bodyTag: "section",
			transitionEffect: "slideLeft",
			onStepChanging: function (event, currentIndex, newIndex)
			{
				form.validate().settings.ignore = ":disabled,:hidden";
				return form.valid();
			},
			onFinishing: function (event, currentIndex)
			{
				form.validate().settings.ignore = ":disabled";
				return form.valid();
			},
			onFinished: function (event, currentIndex)
			{
				alert("Submitted!");
			}
		});

		$("#example-tabs").steps({
			headerTag: "h3",
			bodyTag: "section",
			transitionEffect: "slideLeft",
			enableFinishButton: false,
			enablePagination: false,
			enableAllSteps: true,
			titleTemplate: "#title#",
			cssClass: "tabcontrol"
		});

		$("#example-vertical").steps({
			headerTag: "h3",
			bodyTag: "section",
			transitionEffect: "slideLeft",
			stepsOrientation: "vertical"
		});
	});
</script>
@stop