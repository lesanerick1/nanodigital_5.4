<section class="box-typical box-typical-max-280 scrollable">
	<header class="box-typical-header">
		<div class="tbl-row">
			<div class="tbl-cell tbl-cell-title">
				<h3>User Groups</h3>
			</div>
			<div class="tbl-cell tbl-cell-actions">
				<a href="#" data-toggle="modal" data-target="#addGroup" class="btn btn-primary"><i class="font-icon-left font-icon-plus"></i>Create Group</a>
			</div>
		</div>

	</header>
	<div class="box-typical-body">
		<div class="table-responsive">
			<table id="groups-table" class="table table-bordered table-hover">
				<thead>
				<tr>
					<th width="1">#</th>
					<th>Group Name</th>
					<th>Number of Users</th>
				</tr>
				</thead>
				<tbody>
					@foreach($roles as $index => $role)
					<tr>
						<td>{{ $index+1 }}</td>
						<td>{{ $role->name }}</td>
						<td >{!! $role->users()->count() !!}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</section>

<div class="modal fade" id="addGroup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form class="form-horizontal" role="form" method="post" action="">
				<div class="modal-header">
					<button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
						<i class="font-icon-close-2"></i>
					</button>
					<h4 class="modal-title" id="myModalLabel">Create New User Group</h4>
				</div>
				<div class="modal-body">
                    <!-- CSRF Token -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-group row">
						<label class="col-sm-3 offset-sm-1 form-control-label">Group Name:</label>
						<div class="col-sm-7">
							<p class="form-control-static"> <input type="text" id="name" name="name" class="form-control" placeholder="Group Name" value="">
						</div>
					</div>
                    <div class="form-group row">
						@include('layouts.permissions');
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-success">Save changes</button>
				</div>
            </form>
		</div>
	</div>
</div><!--.modal-->
