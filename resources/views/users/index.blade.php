@extends('layouts.master')

@section('header_scripts')
<style>
#fieldset_user_global_rights fieldset {
    float: left !important;
}
fieldset fieldset {
    margin: .8em !important;
    background: #fff !important;
    border: 1px solid #aaa !important;
    background: #E8E8E8 !important;
}
fieldset, .preview_sql {
    margin-top: 1em !important;
    border-radius: 4px 4px 0 0 !important;
    -moz-border-radius: 4px 4px 0 0 !important;
    -webkit-border-radius: 4px 4px 0 0 !important;
    border: #aaa solid 1px !important;
    padding: 1.5em !important;
    background: #eee !important;
    text-shadow: 1px 1px 2px #fff inset !important;
    -moz-box-shadow: 1px 1px 2px #fff inset !important;
    -webkit-box-shadow: 1px 1px 2px #fff inset !important;
    box-shadow: 1px 1px 2px #fff inset !important;
}
fieldset legend {
    font-weight: bold !important;
    color: #444 !important;
    padding: 5px 10px !important;
    border-radius: 2px !important;
    -moz-border-radius: 2px !important;
    -webkit-border-radius: 2px !important;
    border: 1px solid #aaa !important;
    background-color: #fff !important;
    -moz-box-shadow: 3px 3px 15px #bbb !important;
    -webkit-box-shadow: 3px 3px 15px #bbb !important;
    box-shadow: 3px 3px 15px #bbb !important;
    max-width: 100% !important;
}
</style>
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/lib/bootstrap-table/bootstrap-table.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/lib/datatables-net/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/datatables-net.min.css') }}">
@stop

@section('content')

<div class="page-content">
	
    <div class="container-fluid">
    	<section class="tabs-section">
			<div class="tabs-section-nav tabs-section-nav-inline pull-right">
				<ul class="nav pull-right" role="tablist">
					<li class="nav-item">
						<a class="nav-link " href="#overview" role="tab" data-toggle="tab">
							Overview
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link active" href="#users" role="tab" data-toggle="tab">
							Users
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#groups" role="tab" data-toggle="tab">
							User Groups
						</a>
					</li>
				</ul>
			</div><!--.tabs-section-nav-->

			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade " id="overview">
					<p>&nbsp;</p>
					@include('users.overview')
				</div><!--.tab-pane-->
				<div role="tabpanel" class="tab-pane fade in active" id="users">
					<p>&nbsp;</p>
					@include('users.users')
				</div><!--.tab-pane-->
				<div role="tabpanel" class="tab-pane fade" id="groups">
					<p>&nbsp;</p>
					@include('users.groups')
				</div><!--.tab-pane-->
			</div><!--.tab-content-->
		</section><!--.tabs-section-->
	</div>
</div>
@stop

@section('footer_scripts')
<script src="{{ asset('assets/js/lib/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/bootstrap-table/bootstrap-table.js') }}"></script>
<script src="{{ asset('assets/js/lib/bootstrap-table/bootstrap-table-filter-control.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/bootstrap-table/bootstrap-table-filter-control-init.js') }}"></script>
<script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/table-edit/jquery.tabledit.min.js') }}"></script>
<script>
$(function () {
	$('#groups-table').Tabledit({
		url: "{!! route('groups.edit') !!}",
		
		columns: {
			identifier: [0, 'id'],
			editable: [[1, 'name']]
		},
		buttons: {
		    edit: {
		      class: 'btn btn-sm btn-primary',
		      html: '<span class="glyphicon glyphicon-pencil"></span>',
		      action: 'edit'
		    },
		    delete: {
		      class: 'btn btn-sm btn-danger',
		      html: '<span class="glyphicon glyphicon-trash"></span>',
		      action: 'delete'
		    },
		    save: {
		      class: 'btn btn-sm btn-success',
		      html: 'Save'
		    },
		    restore: {
		      class: 'btn btn-sm btn-warning',
		      html: 'Restore',
		      action: 'restore'
		    },
		    confirm: {
		      class: 'btn btn-sm btn-danger',
		      html: 'Delete'
		    }
		  },
		onDraw: function() {
	        console.log('onDraw()');
	    },
	    onSuccess: function(data, textStatus, jqXHR) {
	        console.log('onSuccess(data, textStatus, jqXHR)');
	        console.log(data);
	        console.log(textStatus);
	        console.log(jqXHR);
	    },
	    onFail: function(jqXHR, textStatus, errorThrown) {
	        console.log('onFail(jqXHR, textStatus, errorThrown)');
	        console.log(jqXHR);
	        console.log(textStatus);
	        console.log(errorThrown);
	    },
	    onAlways: function() {
	        console.log('onAlways()');
	    },
	    onAjax: function(action, serialize) {
	        console.log('onAjax(action, serialize)');
	        console.log(action);
	        console.log(serialize);
	    }
	});
});
</script>

<script>
$(document).ready(function() {
var $table = $('#users-table');

$table.bootstrapTable({
    iconsPrefix: 'font-icon',
    icons: {
        paginationSwitchDown:'font-icon-arrow-square-down',
        paginationSwitchUp: 'font-icon-arrow-square-down up',
        refresh: 'font-icon-refresh',
        toggle: 'font-icon-list-square',
        columns: 'font-icon-list-rotate',
        export: 'font-icon-download',
        detailOpen: 'font-icon-plus',
        detailClose: 'font-icon-minus-1'
    },
    paginationPreText: '<i class="font-icon font-icon-arrow-left"></i>',
    paginationNextText: '<i class="font-icon font-icon-arrow-right"></i>'
    //,
    //data: data
});

/*$('#users-table select').selectpicker({
    style: '',
    width: '100%',
    size: 8
});*/
});
</script>
@stop