<section class="box-typical">
	<header class="box-typical-header">
		<div class="tbl-row">
			<div class="tbl-cell tbl-cell-title">
				<h3>Users</h3>
			</div>
			<div class="tbl-cell tbl-cell-actions">
				<a href="users/create" class="btn btn-primary"><i class="font-icon-left font-icon-plus"></i>New User</a>
			</div>
		</div>
	</header>
	<div class="table-responsive">
		<table id="users-table"
				class="table table-striped"
			   data-search="true"
			   data-show-refresh="false"
			   data-show-toggle="false"
			   data-show-columns="false"
			   data-show-export="true"
			   data-detail-view="false"
			   data-minimum-count-columns="2"
			   data-show-pagination-switch="true"
			   data-pagination="true"
			   data-id-field="id"
			   data-page-list="[10, 25, 50, 100]"
			   data-show-footer="false"
			   data-filter-control="true"
			   data-response-handler="responseHandler"
			   data-toolbar="#toolbar">
			<thead>
			<tr>
				<th>#</th>
				<th data-sortable="true">Name</th>
				<th>Email</th>
				<th data-field="company" data-filter-control="select" data-sortable="true">Company</th>
				<th data-field="status" data-filter-control="select" data-sortable="true">Status</th>
				<th>Role</th>
				<th>Actions</th>
			</tr>
			</thead>
			<tbody>
			@foreach($users as $index => $user)
			<tr>
				<td>{{ $index+1 }}</td>
				<td>{{ $user->first_name }}</td>
				<td>{{ $user->email }}</td>
				<td>{{ $user->rsCompany->name }}</td>
				<td>{{ $user->status }}</td>
				<td></td>
			</tr>
			@endforeach
			</tbody>
			<!--<tfoot>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Email</th>
				<th>Company</th>
				<th>Status</th>
				<th>Role</th>
				<th>Actions</th>
			</tr>
			</tfoot>-->
		</table>
	</div>
</section><!--.box-typical-->


	
