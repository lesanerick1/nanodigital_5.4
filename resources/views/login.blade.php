<!DOCTYPE html>
<html>
<head>
<title>Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">
<script type=text/javascript>
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });
 </script>
<!-- global level css -->
<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" />
<!-- end of global level css -->
<!-- page level css -->
<link rel="prefetch" href="{{ asset('assets/img/login-background.png') }}">
<link type="text/css" href="{{ asset('assets/css/login.css') }}" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="row">
            <!-- Notifications -->
            <div style="margin-top: 50px!important"></div>
            <div class="col-md-4 col-xs-12 col-md-offset-2">
              <div class="iphone img_responsive">
                <div class="screen picHolder">                
                 <img class="content _content " src="{{ asset('images/login-page/login2.png') }}" />
                 <img class="content _content" src="{{ asset('images/login-page/login3.png') }}" />
                 <img class="content _content" src="{{ asset('images/login-page/login4.png') }}" />
                  <img class="content _content" src="{{ asset('images/login-page/login1.png') }}" />
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-md-6 col-sm-6">
                <div  class="well  login-box ">
                    <form action="" autocomplete="on" method="post" role="form">
                        <img src="{{ asset('images/login-page/admin-area.jpg') }}" alt="nano logo"><br><br>
                        <!-- CSRF Token -->
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                        <div class="form-group {{ $errors->first('email', 'has-error') }}">
                            <input id="email" class="form-control"  name="email" required type="email" placeholder="E-mail" value="{!! old('email') !!}"/>
                            <div class="col-sm-12">
                                {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->first('password', 'has-error') }}">
                            <input id="password" class="form-control"  name="password" required type="password" placeholder="Password"/>
                            <div class="col-sm-12">
                                {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <!--<div class="form-group">
                            <label>
                                <input type="checkbox" name="remember-me" id="remember-me" value="remember-me"
                                       class="square-blue"/>
                                Keep me logged in
                            </label>
                        </div>-->
                        <p class="login button">
                            <button class="btn btn-success btn-block">
                              <b>Log in</b>
                          </button>
                        </p>
                        <!--<p>
                            By Loging in, you agree to our Terms & Privacy Policy.
                        </p>-->
                    </form>
                </div>
               <div  class="well well-sm pass-box " >
                 <a href="" data-toggle="modal"  style="color:blue;" data-target="#forget">Forgot password?</a>
                </div>
            </div>
        </div>
    </div>
    <!--<footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-9">
                    <ul class="nav">
                        <li><a href="/"><font color="#003569">ABOUT US</font></a></li>
                        <li><a href="#"><font color="#003569">BLOG</font></a></li>
                        <li><a href="#"><font color="#003569">PRESS</font></a></li>
                        <li><a href="#"><font color="#003569">TERMS & CONDITIONS</font></a></li>
                        <li class="pull-right"><a href="#"><font color="#999">© 2016 NANO DIGITAL</font></a></a></li>
                    </ul>
                </div>
                <div class="col-sm-1"></div>
            </div>
        </div>
    </footer>-->

<div class="modal fade" id="forget" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog " role="document">
    <div class="modal-content">
     <form action="" autocomplete="on" method="post" role="form">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Reset Password</h4>
      </div>
      <div class="modal-body">
      <div class="row">
        <div class="col-sm-12">
            <p>
                Enter your email address below and we'll send a special reset password link to your inbox.
            </p>
            <br>
            <!-- CSRF Token -->
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <div class="form-group {{ $errors->first('email', 'has-error') }}">
                <input id="email" name="email" required type="email" class="form-control" placeholder="your@mail.com"
                       value="{!! old('email') !!}"/>
                <div class="col-sm-12">
                    {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
        </div>
        </div>           
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" value="Submit" class="btn btn-success">Reset</button>
      </div>
       </form>
    </div>
  </div>
</div>


<script src="{{ asset('assets/js/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="{{ asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!--livicons-->
<script type="text/javascript">
$(function() {
var picsLength = $(".picHolder").children('img');
if (picsLength.length > 1) {
    setInterval( function() {
        var active = $(".picHolder img.active");
        var next = active.next().length ? active.next() : $(".picHolder img:first");
        active.addClass('last-active');
        next.css( { opacity: 0.0} ).addClass('active').animate( { opacity : 1.0}, 1000, function() {
        active.removeClass('active last-active');
    } );
}, 3000);
}
});

</script>

</body>
</html>