	<nav class="side-menu side-menu-compact">
	    <ul class="side-menu-list">
	        <li class="brown {{ isActive('dashboard') }}">
	            <a href="/">
	                <i class="font-icon font-icon-home"></i>
	                <span class="lbl">Dashboard</span>
	            </a>
	        </li>
	        <li class="magenta {{ isActive('picture-ads') }}">
	            <a href="/picture-ads">
	                <i class="font-icon font-icon-picture"></i>
	                <span class="lbl">Picture & Videos</span>
	            </a>
	        </li>
	        <!--<li class="gold">
	            <a href="composer">
	                <i class="font-icon font-icon-picture-2"></i>
	                <span class="lbl">Composer</span>
	            </a>
	        </li>-->
	        <li class="blue-dirty {{ isActive('bulk-sms') }}">
	            <a href="/bulk-sms">
	                <i class="font-icon font-icon-tablet"></i>
	                <span class="lbl">Bulk SMS</span>
	            </a>
	        </li>
	        <li class="purple {{ isActive('messages') }}">
	            <a href="/messages">
	                <i class="font-icon font-icon-comments"></i>
	                <span class="lbl">Instant Messaging</span>
	                
	            </a>
	        </li>
	        <li class="blue {{ isActive('social-media') }}">
	            <a href="/social-media">
	                <i class="font-icon font-icon-twitter"></i>
	                <span class="lbl">Social Media</span>
	                
	            </a>
	        </li>
	        <li class="magenta {{ isActive('website-apps') }}">
	            <a href="">
	                <i class="font-icon font-icon-internet"></i>
	                <span class="lbl">Website Ads</span>
	            </a>
	        </li>
	        <li class="magenta {{ isActive('mobile-apps') }}">
	            <a href="">
	                <i class="font-icon font-icon-tablet"></i>
	                <span class="lbl">Mobile App Ads</span>
	            </a>
	        </li>
	        <li class="magenta {{ isActive('mobile-apps') }}">
	            <a href="">
	                <i class="font-icon font-icon-tablet"></i>
	                <span class="lbl">Market Review</span>
	            </a>
	        </li>
	        <li class="pink {{ isActive('customers') }}">
	            <a href="/customers">
	                <i class="font-icon font-icon-users-group"></i>
	                <span class="lbl">Customers</span>
	            </a>
	        </li>
	        <li class="gold {{ isActive('companies') }}">
	            <a href="/companies">
	                <i class="font-icon font-icon-speed"></i>
	                <span class="lbl">Companies</span>
	            </a>
	        </li>
	        <li class="green {{ isActive('agencies') }}">
	            <a href="/agencies">
	                <i class="font-icon font-icon-cart"></i>
	                <span class="lbl">Agencies</span>
	            </a>
	        </li>
	        <!--<li class="aquamarine">
	            <a href="/invoice">
	                <i class="font-icon font-icon money"></i>
	                <span class="lbl">Billing</span>
	            </a>
	        </li>
	        <li class="coral">
	            <a href="#">
	                <i class="font-icon font-icon-chart"></i>
	                <span class="lbl">Reports</span>
	            </a>
	        </li>
	        <li class="blue">
	            <a href="users">
	                <i class="font-icon font-icon-users"></i>
	                <span class="lbl">Users</span>
	            </a>
	        </li>
	        <li class="coral">
	            <a href="#">
	                <i class="font-icon font-icon-cogwheel"></i>
	                <span class="lbl">Settings</span>
	            </a>
	        </li>
	        <li class="orange-red">
	            <a href="support">
	                <i class="font-icon font-icon-help"></i>
	                <span class="lbl">Support</span>
	            </a>
	        </li>
	        
	        <li class="grey">
	            <a href="#">
	                <i class="font-icon font-icon-dots"></i>
	                <span class="lbl">More</span>
	            </a>
	        </li>
	        <li class="blue-dirty">
	            <a href="#">
	                <i class="font-icon font-icon-edit"></i>
	                <span class="lbl">Forms</span>
	            </a>
	        </li>-->
	        <!--<li class="pink-red">
	            <a href="#">
	                <i class="font-icon font-icon-zigzag"></i>
	                <span class="lbl">Activity</span>
	            </a>
	        </li>-->
	        <!--<li class="magenta">
	            <a href="#">
	                <i class="font-icon font-icon-widget"></i>
	                <span class="lbl">Widges</span>
	            </a>
	        </li>
	        <li class="pink">
	            <a href="#">
	                <i class="font-icon font-icon-map"></i>
	                <span class="lbl">Maps</span>
	            </a>
	        </li>
	        <li class="blue-darker">
	            <a href="#">
	                <i class="font-icon font-icon-chart-2"></i>
	                <span class="lbl">Charts</span>
	            </a>
	        </li>-->
	        <!--<li class="magenta">
	            <a href="#">
	                <i class="font-icon font-icon-user"></i>
	                <span class="lbl">Users</span>
	            </a>
	        </li>
	        <li class="blue-dirty">
	            <a href="#">
	                <i class="font-icon font-icon-notebook"></i>
	                <span class="lbl">Tasks</span>
	            </a>
	        </li>-->
	        
	        <!--<li class="brown">
	            <a href="#">
	                <i class="font-icon font-icon-event"></i>
	                <span class="lbl">Event</span>
	            </a>
	        </li>
	        <li class="red">
	            <a href="#">
	                <i class="font-icon font-icon-case-2"></i>
	                <span class="lbl">Project</span>
	            </a>
	        </li>-->
	    </ul>
	</nav><!--.side-menu-->

	<!--<nav class="side-menu">
	    <ul class="side-menu-list">
	        <li class="grey">
	            <span>
	                <i class="font-icon font-icon-dashboard"></i>
	                <span class="lbl">Dashboard</span>
	            </span>
	        </li>
	        <li class="gold with-sub">
	            <span>
	                <i class="font-icon font-icon-edit"></i>
	                <span class="lbl">Audience Manager</span>
	            </span>
	            <ul>
	                <li><a href="ui-form.html"><span class="lbl">Subscription</span></a></li>
	                <li><a href="ui-buttons.html"><span class="lbl">Customer Groups</span></a></li>
	                <li><a href="ui-select.html"><span class="lbl">Carriers</span></a></li>
	                <li><a href="ui-checkboxes.html"><span class="lbl">Clean Database</span></a></li>
	                <li><a href="ui-checkboxes.html"><span class="lbl">Phone Type</span></a></li>
	            </ul>
	        </li>
	        <li class="gold with-sub">
	            <span>
	                <i class="font-icon font-icon-edit"></i>
	                <span class="lbl">Picture & Video Ads</span>
	            </span>
	            <ul>
	                <li><a href="ui-form.html"><span class="lbl">Ad Composer</span></a></li>
	                <li><a href="ui-buttons.html"><span class="lbl">Campaigns</span></a></li>
	                <li><a href="ui-select.html"><span class="lbl">Campaign Analysis</span></a></li>
	                <li><a href="ui-checkboxes.html"><span class="lbl">Inventory</span></a></li>
	            </ul>
	        </li>
	        <li class="gold with-sub">
	            <span>
	                <i class="font-icon font-icon-edit"></i>
	                <span class="lbl">Bulk SMS</span>
	            </span>
	            <ul>
	                <li><a href="ui-form.html"><span class="lbl">Send SMS</span></a></li>
	                <li><a href="ui-buttons.html"><span class="lbl">Campaigns</span></a></li>
	                <li><a href="ui-select.html"><span class="lbl">Campaign Analysis</span></a></li>
	                <li><a href="ui-checkboxes.html"><span class="lbl">Inventory</span></a></li>
	            </ul>
	        </li>
	        <li class="gold with-sub">
	            <span>
	                <i class="font-icon font-icon-edit"></i>
	                <span class="lbl">Agency Manager</span>
	            </span>
	            <ul>
	                <li><a href="ui-form.html"><span class="lbl">View Agencies</span></a></li>
	                <li><a href="ui-buttons.html"><span class="lbl">Agency Short Codes</span></a></li>
	            </ul>
	        </li>
	        <li class="gold with-sub">
	            <span>
	                <i class="font-icon font-icon-edit"></i>
	                <span class="lbl">Company Manager</span>
	            </span>
	            <ul>
	                <li><a href="ui-form.html"><span class="lbl">View Companies</span></a></li>
	                <li><a href="ui-buttons.html"><span class="lbl">Company Short Codes</span></a></li>
	                <li><a href="ui-select.html"><span class="lbl">Bulk SMS Packages</span></a></li>
	                <li><a href="ui-checkboxes.html"><span class="lbl">Picture Ad Packages</span></a></li>
	            </ul>
	        </li>
	        <li class="purple with-sub">
	            <span>
	                <i class="font-icon font-icon-comments active"></i>
	                <span class="lbl">Messages</span>
	            </span>
	            <ul>
	                <li><a href="messenger.html"><span class="lbl">Messenger</span></a></li>
	                <li><a href="chat.html"><span class="lbl">Messages</span><span class="label label-custom label-pill label-danger">8</span></a></li>
	                <li><a href="chat-write.html"><span class="lbl">Write Message</span></a></li>
	                <li><a href="chat-index.html"><span class="lbl">Select User</span></a></li>
	            </ul>
	        </li>
	        <li class="gold with-sub">
	            <span>
	                <i class="font-icon font-icon-edit"></i>
	                <span class="lbl">Users</span>
	            </span>
	            <ul>
	                <li><a href="ui-form.html"><span class="lbl">View Users</span></a></li>
	                <li><a href="ui-buttons.html"><span class="lbl">User Groups</span></a></li>
	            </ul>
	        </li>
	        <li class="red">
	            <a href="mail.html">
	                <i class="font-icon glyphicon glyphicon-send"></i>
	                <span class="lbl">Billing</span>
	            </a>
	        </li>
	        <li class="blue-dirty">
	            <a href="tables.html">
	                <span class="glyphicon glyphicon-th"></span>
	                <span class="lbl">Customer Support</span>
	            </a>
	        </li>
	        
	    </ul>
	
	    <section>
	        <header class="side-menu-title">Tags</header>
	        <ul class="side-menu-list">
	            <li>
	                <a href="#">
	                    <i class="tag-color green"></i>
	                    <span class="lbl">Website</span>
	                </a>
	            </li>
	            <li>
	                <a href="#">
	                    <i class="tag-color grey-blue"></i>
	                    <span class="lbl">Bugs/Errors</span>
	                </a>
	            </li>
	            <li>
	                <a href="#">
	                    <i class="tag-color red"></i>
	                    <span class="lbl">General Problem</span>
	                </a>
	            </li>
	            <li>
	                <a href="#">
	                    <i class="tag-color pink"></i>
	                    <span class="lbl">Questions</span>
	                </a>
	            </li>
	            <li>
	                <a href="#">
	                    <i class="tag-color orange"></i>
	                    <span class="lbl">Ideas</span>
	                </a>
	            </li>
	        </ul>
	    </section>
	</nav>--><!--.side-menu-->