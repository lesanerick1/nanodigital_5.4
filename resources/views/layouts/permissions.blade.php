<form class="submenu-item" name="usersForm" id="addUsersForm" action="" method="post">
	<fieldset id="fieldset_user_global_rights">
		<legend data-submenu-label="Global">Global privileges<input type="checkbox" id="addUsersForm_checkall" class="checkall_box" title="Check all" /> <label for="addUsersForm_checkall">Check all</label></legend>
		<fieldset>
			<legend>
				<input type="checkbox" class="sub_checkall_box" id="checkall_menu_priv" title="Check all"/><label for="checkall_Data_priv">Side Menu</label>
			</legend>
			<div class="item">
				<input type="checkbox" class="checkall" name="Select_priv" id="checkbox_Select_priv" value="Y" title="Allows reading data." checked="checked"/>
				<label for="checkbox_Select_priv"><code><dfn title="Allows reading data.">Picture & Video</dfn></code></label>
			</div>
			<div class="item">
				<input type="checkbox" class="checkall" name="Insert_priv" id="checkbox_Insert_priv" value="Y" title="Allows inserting and replacing data." checked="checked"/>
				<label for="checkbox_Insert_priv"><code><dfn title="Allows inserting and replacing data.">Bulk SMS</dfn></code></label>
			</div>
			<div class="item">
				<input type="checkbox" class="checkall" name="Update_priv" id="checkbox_Update_priv" value="Y" title="Allows changing data." checked="checked"/>
				<label for="checkbox_Update_priv"><code><dfn title="Allows changing data.">Messages</dfn></code></label>
			</div>
			<div class="item">
				<input type="checkbox" class="checkall" name="Delete_priv" id="checkbox_Delete_priv" value="Y" title="Allows deleting data." checked="checked"/>
				<label for="checkbox_Delete_priv"><code><dfn title="Allows deleting data.">Customers</dfn></code></label>
			</div>
			<div class="item">
				<input type="checkbox" class="checkall" name="File_priv" id="checkbox_File_priv" value="Y" title="Allows importing data from and exporting data into files." checked="checked"/>
				<label for="checkbox_File_priv"><code><dfn title="Allows importing data from and exporting data into files.">Companies</dfn></code></label>
			</div>
			<div class="item">
				<input type="checkbox" class="checkall" name="File_priv" id="checkbox_File_priv" value="Y" title="Allows importing data from and exporting data into files." checked="checked"/>
				<label for="checkbox_File_priv"><code><dfn title="Allows importing data from and exporting data into files.">Agencies</dfn></code></label>
			</div>
		</fieldset>
		<fieldset>
			<legend>
				<input type="checkbox" class="sub_checkall_box" id="checkall_Data_priv" title="Check all"/><label for="checkall_Data_priv">Top Menu</label>
			</legend>
			<div class="item">
				<input type="checkbox" class="checkall" name="Select_priv" id="checkbox_Select_priv" value="Y" title="Allows reading data." checked="checked"/>
				<label for="checkbox_Select_priv"><code><dfn title="Allows reading data.">Settings</dfn></code></label>
			</div>
			<div class="item">
				<input type="checkbox" class="checkall" name="Insert_priv" id="checkbox_Insert_priv" value="Y" title="Allows inserting and replacing data." checked="checked"/>
				<label for="checkbox_Insert_priv"><code><dfn title="Allows inserting and replacing data.">Users</dfn></code></label>
			</div>
			<div class="item">
				<input type="checkbox" class="checkall" name="Update_priv" id="checkbox_Update_priv" value="Y" title="Allows changing data." checked="checked"/>
				<label for="checkbox_Update_priv"><code><dfn title="Allows changing data.">Logs</dfn></code></label>
			</div>
			<div class="item">
				<input type="checkbox" class="checkall" name="Delete_priv" id="checkbox_Delete_priv" value="Y" title="Allows deleting data." checked="checked"/>
				<label for="checkbox_Delete_priv"><code><dfn title="Allows deleting data.">Billing</dfn></code></label>
			</div>
			<div class="item">
				<input type="checkbox" class="checkall" name="File_priv" id="checkbox_File_priv" value="Y" title="Allows importing data from and exporting data into files." checked="checked"/>
				<label for="checkbox_File_priv"><code><dfn title="Allows importing data from and exporting data into files.">Reports</dfn></code></label>
			</div>
		</fieldset>
		<fieldset>
			<legend>
				<input type="checkbox" class="sub_checkall_box" id="checkall_Structure_priv" title="Check all"/><label for="checkall_Structure_priv">Structure</label>
			</legend>
			<div class="item">
				<input type="checkbox" class="checkall" name="Create_priv" id="checkbox_Create_priv" value="Y" title="Allows creating new databases and tables." checked="checked"/>
				<label for="checkbox_Create_priv"><code><dfn title="Allows creating new databases and tables.">CREATE</dfn></code></label>
			</div>
			<div class="item">
				<input type="checkbox" class="checkall" name="Alter_priv" id="checkbox_Alter_priv" value="Y" title="Allows altering the structure of existing tables." checked="checked"/>
				<label for="checkbox_Alter_priv"><code><dfn title="Allows altering the structure of existing tables.">ALTER</dfn></code></label>
			</div>
			<div class="item">
				<input type="checkbox" class="checkall" name="Index_priv" id="checkbox_Index_priv" value="Y" title="Allows creating and dropping indexes." checked="checked"/>
				<label for="checkbox_Index_priv"><code><dfn title="Allows creating and dropping indexes.">INDEX</dfn></code></label>
			</div>
			<div class="item">
				<input type="checkbox" class="checkall" name="Drop_priv" id="checkbox_Drop_priv" value="Y" title="Allows dropping databases and tables." checked="checked"/>
				<label for="checkbox_Drop_priv"><code><dfn title="Allows dropping databases and tables.">DROP</dfn></code></label>
			</div>
			<div class="item">
				<input type="checkbox" class="checkall" name="Create_tmp_table_priv" id="checkbox_Create_tmp_table_priv" value="Y" title="Allows creating temporary tables." checked="checked"/>
				<label for="checkbox_Create_tmp_table_priv"><code><dfn title="Allows creating temporary tables.">CREATE TEMPORARY TABLES</dfn></code></label>
			</div>
			<div class="item">
				<input type="checkbox" class="checkall" name="Show_view_priv" id="checkbox_Show_view_priv" value="Y" title="Allows performing SHOW CREATE VIEW queries." checked="checked"/>
				<label for="checkbox_Show_view_priv"><code><dfn title="Allows performing SHOW CREATE VIEW queries.">SHOW VIEW</dfn></code></label>
			</div>
			<div class="item">
				<input type="checkbox" class="checkall" name="Create_routine_priv" id="checkbox_Create_routine_priv" value="Y" title="Allows creating stored routines." checked="checked"/>
				<label for="checkbox_Create_routine_priv"><code><dfn title="Allows creating stored routines.">CREATE ROUTINE</dfn></code></label>
			</div>
			<div class="item">
				<input type="checkbox" class="checkall" name="Alter_routine_priv" id="checkbox_Alter_routine_priv" value="Y" title="Allows altering and dropping stored routines." checked="checked"/>
				<label for="checkbox_Alter_routine_priv"><code><dfn title="Allows altering and dropping stored routines.">ALTER ROUTINE</dfn></code></label>
			</div>
			<div class="item">
<input type="checkbox" class="checkall" name="Execute_priv" id="checkbox_Execute_priv" value="Y" title="Allows executing stored routines." checked="checked"/>
<label for="checkbox_Execute_priv"><code><dfn title="Allows executing stored routines.">EXECUTE</dfn></code></label>
</div>
<div class="item">
<input type="checkbox" class="checkall" name="Create_view_priv" id="checkbox_Create_view_priv" value="Y" title="Allows creating new views." checked="checked"/>
<label for="checkbox_Create_view_priv"><code><dfn title="Allows creating new views.">CREATE VIEW</dfn></code></label>
</div>
<div class="item">
<input type="checkbox" class="checkall" name="Event_priv" id="checkbox_Event_priv" value="Y" title="Allows to set up events for the event scheduler." checked="checked"/>
<label for="checkbox_Event_priv"><code><dfn title="Allows to set up events for the event scheduler.">EVENT</dfn></code></label>
</div>
<div class="item">
<input type="checkbox" class="checkall" name="Trigger_priv" id="checkbox_Trigger_priv" value="Y" title="Allows creating and dropping triggers." checked="checked"/>
<label for="checkbox_Trigger_priv"><code><dfn title="Allows creating and dropping triggers.">TRIGGER</dfn></code></label>
</div>
</fieldset>
<fieldset>
<legend>
<input type="checkbox" class="sub_checkall_box" id="checkall_Administration_priv" title="Check all"/><label for="checkall_Administration_priv">Administration</label>
</legend>
<div class="item">
<input type="checkbox" class="checkall" name="Grant_priv" id="checkbox_Grant_priv" value="Y" title="Allows adding users and privileges without reloading the privilege tables." checked="checked"/>
<label for="checkbox_Grant_priv"><code><dfn title="Allows adding users and privileges without reloading the privilege tables.">GRANT</dfn></code></label>
</div>
<div class="item">
<input type="checkbox" class="checkall" name="Super_priv" id="checkbox_Super_priv" value="Y" title="Allows connecting, even if maximum number of connections is reached; required for most administrative operations like setting global variables or killing threads of other users." checked="checked"/>
<label for="checkbox_Super_priv"><code><dfn title="Allows connecting, even if maximum number of connections is reached; required for most administrative operations like setting global variables or killing threads of other users.">SUPER</dfn></code></label>
</div>
<div class="item">
<input type="checkbox" class="checkall" name="Process_priv" id="checkbox_Process_priv" value="Y" title="Allows viewing processes of all users." checked="checked"/>
<label for="checkbox_Process_priv"><code><dfn title="Allows viewing processes of all users.">PROCESS</dfn></code></label>
</div>
<div class="item">
<input type="checkbox" class="checkall" name="Reload_priv" id="checkbox_Reload_priv" value="Y" title="Allows reloading server settings and flushing the server's caches." checked="checked"/>
<label for="checkbox_Reload_priv"><code><dfn title="Allows reloading server settings and flushing the server's caches.">RELOAD</dfn></code></label>
</div>
<div class="item">
<input type="checkbox" class="checkall" name="Shutdown_priv" id="checkbox_Shutdown_priv" value="Y" title="Allows shutting down the server." checked="checked"/>
<label for="checkbox_Shutdown_priv"><code><dfn title="Allows shutting down the server.">SHUTDOWN</dfn></code></label>
</div>
<div class="item">
<input type="checkbox" class="checkall" name="Show_db_priv" id="checkbox_Show_db_priv" value="Y" title="Gives access to the complete list of databases." checked="checked"/>
<label for="checkbox_Show_db_priv"><code><dfn title="Gives access to the complete list of databases.">SHOW DATABASES</dfn></code></label>
</div>
<div class="item">
<input type="checkbox" class="checkall" name="Lock_tables_priv" id="checkbox_Lock_tables_priv" value="Y" title="Allows locking tables for the current thread." checked="checked"/>
<label for="checkbox_Lock_tables_priv"><code><dfn title="Allows locking tables for the current thread.">LOCK TABLES</dfn></code></label>
</div>
<div class="item">
<input type="checkbox" class="checkall" name="References_priv" id="checkbox_References_priv" value="Y" title="Has no effect in this MySQL version." checked="checked"/>
<label for="checkbox_References_priv"><code><dfn title="Has no effect in this MySQL version.">REFERENCES</dfn></code></label>
</div>
<div class="item">
<input type="checkbox" class="checkall" name="Repl_client_priv" id="checkbox_Repl_client_priv" value="Y" title="Allows the user to ask where the slaves / masters are." checked="checked"/>
<label for="checkbox_Repl_client_priv"><code><dfn title="Allows the user to ask where the slaves / masters are.">REPLICATION CLIENT</dfn></code></label>
</div>
<div class="item">
<input type="checkbox" class="checkall" name="Repl_slave_priv" id="checkbox_Repl_slave_priv" value="Y" title="Needed for the replication slaves." checked="checked"/>
<label for="checkbox_Repl_slave_priv"><code><dfn title="Needed for the replication slaves.">REPLICATION SLAVE</dfn></code></label>
</div>
<div class="item">
<input type="checkbox" class="checkall" name="Create_user_priv" id="checkbox_Create_user_priv" value="Y" title="Allows creating, dropping and renaming user accounts." checked="checked"/>
<label for="checkbox_Create_user_priv"><code><dfn title="Allows creating, dropping and renaming user accounts.">CREATE USER</dfn></code></label>
</div>
</fieldset>

</form>