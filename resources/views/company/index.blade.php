@extends('layouts.master')

@section('header_scripts')
<link rel="stylesheet" href="{{ asset('assets/css/lib/bootstrap-table/bootstrap-table.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/lib/datatables-net/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/datatables-net.min.css') }}">
@stop

@section('content')
<div class="page-content">
	<div class="container-fluid">
    <div class="row">
    	<div class="col-md-12">
        <section class="card">
          <header class="card-header">
            Companies
            <button type="button" class="modal-close">
              <a href="users/create" class="btn btn-primary"><i class="font-icon-left font-icon-plus"></i>Add Company</a>
            </button>
          </header>
          <div class="card-block">
            <table id="agencies" class="display table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
          <tr>
            <th>#</th>
            <th>Logo</th>
            <th>Name</th>
            <th>Location</th>
            <th>Description</th>
            <th>Opt In Date</th>
            <th>Short Codes</th>
            <th>Services</th>
            <th>Status</th>
            <th></th>
          </tr>
          </thead>
          <tbody>
          @foreach($companies as $index => $company)
          <tr>
            <td>{{ $index+1 }}</td>
            <td>{{ $company->logo }}</td>
            <td>{{ $company->name }}</td>
            <td></td>
            <td></td>
            <td></td>
            <td>{!! ($company->shortcode->count() > 0 ) ? $company->shortcode->count() : 'None <a href="#addShortCode" class="btn btn-inline btn-primary btn-sm ladda-button" data-style="expand-right" data-size="s"><span class="ladda-label">Add</span></a>' !!}</td>
            <td>{!! ($company->services->count() > 0 ) ? $company->services->count() : 'None <a href="#addService" class="btn btn-inline btn-primary btn-sm ladda-button" data-style="expand-right" data-size="s"><span class="ladda-label">Add</span></a>' !!}</td>
            <td></td>
            <td></td>
          </tr>
          @endforeach
          </tbody>
        </table>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>

@stop

@section('footer_scripts')
<script src="{{ asset('assets/js/lib/bootstrap-table/bootstrap-table.js') }}"></script>
<script src="{{ asset('assets/js/lib/bootstrap-table/bootstrap-table-filter-control.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/bootstrap-table/bootstrap-table-filter-control-init.js') }}"></script>
<script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script>
<script>
	$(function() {
		$('#agencies').DataTable();
	});
</script>
@stop