<div class="row">
	<div class="col-xl-4">
		<section class="widget widget-simple-sm">
			<div class="widget-simple-sm-statistic">
				<div class="number">{{ $company_packages->sum('number_of_messages') }}</div>
				<div class="caption color-blue">Bundle Purchased</div>
			</div>
			<div class="widget-simple-sm-bottom statistic"> Purchased on <strong></strong></div>
		</section><!--.widget-simple-sm-->
	</div>
	<div class="col-xl-4">
		<section class="widget widget-simple-sm">
			<div class="widget-simple-sm-statistic">
				<div class="number">63 541</div>
				<div class="caption color-red">Messages Used</div>
			</div>
			<div class="widget-simple-sm-bottom statistic">{{ $campaigns->count() }} Campaigns <strong></strong></div>
		</section><!--.widget-simple-sm-->
	</div>
	<div class="col-xl-4">
		<section class="widget widget-simple-sm">
			<div class="widget-simple-sm-statistic">
				<div class="number">{{ $company_packages->sum('number_of_messages') - $campaigns->count()  }}</div>
				<div class="caption color-green">Messages Remaining</div>
			</div>
			<div class="widget-simple-sm-bottom statistic"> Expires on <strong></strong></div>
		</section><!--.widget-simple-sm-->
	</div>
</div><!--.row-->
<section class="box-typical box-typical-max-280 scrollable">
	<header class="box-typical-header">
		<div class="tbl-row">
			<div class="tbl-cell tbl-cell-title">
				<h3>SMS Inventory</h3>
			</div>
		</div>
	</header>
	<div class="box-typical-body">
		<div class="table-responsive">
			<table class="table table-hover">
				<tbody>
					<tr>
						<td class="table-check">
							<div class="checkbox checkbox-only">
								<input type="checkbox" id="tbl-check-1"/>
								<label for="tbl-check-1"></label>
							</div>
						</td>
						<td><a href="#">Description</a></td>
						<td>Text</td>
						<td width="150">
							<div class="progress-with-amount">
								<progress class="progress progress-success progress-no-margin" value="25" max="100">25%</progress>
								<div class="progress-with-amount-number">25%</div>
							</div>
						</td>
						<td nowrap="nowrap">12.423<span class="caret color-red"></span></td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">NULL</div>
							0%
						</td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">Unique values</div>
							72
						</td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">Most commons</div>
							Florida (13%)
						</td>
					</tr>
					<tr>
						<td class="table-check">
							<div class="checkbox checkbox-only">
								<input type="checkbox" id="tbl-check-2"/>
								<label for="tbl-check-2"></label>
							</div>
						</td>
						<td><a href="#">Value</a></td>
						<td>Revene for last quarter in state America.</td>
						<td width="150">
							<div class="progress-with-amount">
								<progress class="progress progress-no-margin" value="50" max="100">50%</progress>
								<div class="progress-with-amount-number">50%</div>
							</div>
						</td>
						<td nowrap="nowrap">27.051<span class="caret caret-up color-green"></span></td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">NULL</div>
							0%
						</td>
						<td colspan="2" width="40%">
							<div class="bar-chart-wrapper">
								<span class="bar-chart">2,5,3,6,2,1,4,6,2,1,2,5,3,6,2,1,4,6,2,1,2,5,3,6,2,1,4,6,2,1,2,5,3,6,2,1,4,6,2,1,2,5,3,6,2,1,4,6,2,1,2,5,3,6,2,1,4,6,2,1,2,5,3,6,2,1,4,6,2,1,2,1</span>
								<div class="val left">0</div>
								<div class="val right">1,57 м</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="table-check">
							<div class="checkbox checkbox-only">
								<input type="checkbox" id="tbl-check-3"/>
								<label for="tbl-check-3"></label>
							</div>
						</td>
						<td><a href="#">Assignee</a></td>
						<td>Text</td>
						<td width="150">
							<div class="progress-with-amount">
								<progress class="progress progress-danger progress-no-margin" value="75" max="100">75%</progress>
								<div class="progress-with-amount-number">75%</div>
							</div>
						</td>
						<td nowrap="nowrap">12.423<span class="caret color-red"></span></td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">NULL</div>
							0%
						</td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">Unique values</div>
							72
						</td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">Most commons</div>
							Florida (13%)
						</td>
					</tr>
					<tr>
						<td class="table-check">
							<div class="checkbox checkbox-only">
								<input type="checkbox" id="tbl-check-4"/>
								<label for="tbl-check-4"></label>
							</div>
						</td>
						<td><a href="#">Data Create</a></td>
						<td>Revene for last quarter in state America.</td>
						<td width="150">
							<div class="progress-with-amount">
								<progress class="progress progress-info progress-no-margin" value="15" max="100">15%</progress>
								<div class="progress-with-amount-number">15%</div>
							</div>
						</td>
						<td nowrap="nowrap">12.423<span class="caret color-red"></span></td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">NULL</div>
							0%
						</td>
						<td colspan="2">
							<div class="bar-chart-wrapper">
								<span class="bar-chart">2,5,3,6,2,1,2,1,2,1,2,5,3,1,2,1,4,1,2,1,2,5,3,6,2,1,1,6,2,1,2,5,3,6,2,1,4,6,2,1,2,5,1,1,2,1,4,1,2,1,2,1,1,1,2,1,4,6,2,1,2,5,3,6,2,1,4,6,2,1,2,1</span>
								<div class="val left">0</div>
								<div class="val right">1,57 м</div>
							</div>
						</td>
					</tr>
					<tr>
						<td class="table-check">
							<div class="checkbox checkbox-only">
								<input type="checkbox" id="tbl-check-5"/>
								<label for="tbl-check-5"></label>
							</div>
						</td>
						<td><a href="#">In Progress</a></td>
						<td>Text</td>
						<td width="150">
							<div class="progress-with-amount">
								<progress class="progress progress-success progress-no-margin" value="25" max="100">25%</progress>
								<div class="progress-with-amount-number">25%</div>
							</div>
						</td>
						<td nowrap="nowrap">12.423<span class="caret color-red"></span></td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">NULL</div>
							0%
						</td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">Unique values</div>
							72
						</td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">Most commons</div>
							Florida (13%)
						</td>
					</tr>
					<tr class="table-active">
						<td class="table-check">
							<div class="checkbox checkbox-only">
								<input type="checkbox" id="tbl-check-6"/>
								<label for="tbl-check-6"></label>
							</div>
						</td>
						<td><a href="#">State</a></td>
						<td>Text</td>
						<td width="150">
							<div class="progress-with-amount">
								<progress class="progress progress-success progress-no-margin" value="25" max="100">25%</progress>
								<div class="progress-with-amount-number">25%</div>
							</div>
						</td>
						<td nowrap="nowrap">12.423<span class="caret color-red"></span></td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">NULL</div>
							0%
						</td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">Unique values</div>
							72
						</td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">Most commons</div>
							Florida (13%)
						</td>
					</tr>
					<tr class="table-success">
						<td class="table-check">
							<div class="checkbox checkbox-only">
								<input type="checkbox" id="tbl-check-7"/>
								<label for="tbl-check-7"></label>
							</div>
						</td>
						<td><a href="#">Value</a></td>
						<td>Revene for last quarter in state America.</td>
						<td width="150">
							<div class="progress-with-amount">
								<progress class="progress progress-success progress-no-margin" value="25" max="100">25%</progress>
								<div class="progress-with-amount-number">25%</div>
							</div>
						</td>
						<td nowrap="nowrap">12.423<span class="caret color-red"></span></td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">NULL</div>
							0%
						</td>
						<td colspan="2">
							<div class="bar-chart-wrapper">
								<span class="bar-chart">2,5,3,6,2,1,4,6,2,1,2,5,3,6,2,1,4,6,2,1,2,5,3,6,2,1,4,6,2,1,2,5,3,6,2,1,4,6,2,1,2,5,3,6,2,1,4,6,2,1,2,5,3,6,2,1,4,6,2,1,2,5,3,6,2,1,4,6,2,1,2,1</span>
								<div class="val left">0</div>
								<div class="val right">1,57 м</div>
							</div>
						</td>
					</tr>
					<tr class="table-warning">
						<td class="table-check">
							<div class="checkbox checkbox-only">
								<input type="checkbox" id="tbl-check-8"/>
								<label for="tbl-check-8"></label>
							</div>
						</td>
						<td><a href="#">Assignee</a></td>
						<td>Text</td>
						<td width="150">
							<div class="progress-with-amount">
								<progress class="progress progress-success progress-no-margin" value="25" max="100">25%</progress>
								<div class="progress-with-amount-number">25%</div>
							</div>
						</td>
						<td nowrap="nowrap">12.423<span class="caret color-red"></span></td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">NULL</div>
							0%
						</td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">Unique values</div>
							72
						</td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">Most commons</div>
							Florida (13%)
						</td>
					</tr>
					<tr class="table-danger">
						<td class="table-check">
							<div class="checkbox checkbox-only">
								<input type="checkbox" id="tbl-check-9"/>
								<label for="tbl-check-9"></label>
							</div>
						</td>
						<td><a href="#">Data Create</a></td>
						<td>Revene for last quarter in state America.</td>
						<td width="150">
							<div class="progress-with-amount">
								<progress class="progress progress-success progress-no-margin" value="25" max="100">25%</progress>
								<div class="progress-with-amount-number">25%</div>
							</div>
						</td>
						<td nowrap="nowrap">12.423<span class="caret color-red"></span></td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">NULL</div>
							0%
						</td>
						<td colspan="2">
							<div class="bar-chart-wrapper">
								<span class="bar-chart">2,5,3,6,2,1,2,1,2,1,2,5,3,1,2,1,4,1,2,1,2,5,3,6,2,1,1,6,2,1,2,5,3,6,2,1,4,6,2,1,2,5,1,1,2,1,4,1,2,1,2,1,1,1,2,1,4,6,2,1,2,5,3,6,2,1,4,6,2,1,2,1</span>
								<div class="val left">0</div>
								<div class="val right">1,57 м</div>
							</div>
						</td>
					</tr>
					<tr class="table-info">
						<td class="table-check">
							<div class="checkbox checkbox-only">
								<input type="checkbox" id="tbl-check-10"/>
								<label for="tbl-check-10"></label>
							</div>
						</td>
						<td><a href="#">In Progress</a></td>
						<td>Text</td>
						<td width="150">
							<div class="progress-with-amount">
								<progress class="progress progress-success progress-no-margin" value="25" max="100">25%</progress>
								<div class="progress-with-amount-number">25%</div>
							</div>
						</td>
						<td nowrap="nowrap">12.423<span class="caret color-red"></span></td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">NULL</div>
							0%
						</td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">Unique values</div>
							72
						</td>
						<td>
							<div class="font-11 color-blue-grey-lighter uppercase">Most commons</div>
							Florida (13%)
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div><!--.box-typical-body-->
</section><!--.box-typical-->
