<section class="content">    
    <div class="row">
      <div class="col-lg-12" >
    <!-- Trans label pie charts strats here -->
  <div class="panel panel-primary" style="overflow:auto;">
    <div class="panel-heading">
      <h3 class="panel-title">
          <i class="livicon" data-name="sms" data-size="16" data-loop="true" data-c="#fff" data-hc="#fff"></i>
        SMS Campaign Analysis
      </h3>
    </div>
    <div class="panel-body">
    <div class="row">
    <div class="col-lg-4">
      <div class="panel panel-primary">
        <div class="panel-body">
            <div class="form-group">
              <select onchange="filter();" class="select form-control" name="carrier">
                <option value="" selected="selected">Select Campaign</option>
				@foreach($campaigns as $index => $campaign)
                	<option value="{{ $campaign->id }}">{{ $campaign->name }}</option>
                @endforeach
              </select>
            </div>
        </div>
      </div>
    </div>
     <div class="col-lg-4">
      <div class="panel panel-primary">
        <div class="panel-body">
            <div class="form-group">
				<div class='input-group date'>
					<input id="daterange" type="text" value="01/01/2015 1:30 PM - 01/01/2015 2:00 PM" class="form-control">
					<span class="input-group-addon">
						<i class="font-icon font-icon-calend"></i>
					</span>
				</div>
			</div>
        </div>
      </div>
    </div>
   </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <div id="container" style="min-width: 310px; max-width: 600px; height: 400px; margin: 0 auto"></div>
      </div>
      <div class="col-lg-6">
        <div id="container1" style="min-width: 310px; max-width: 600px; height: 400px; margin: 0 auto"></div>
      </div>
    </div>
      </div>
  </div>
  </div>
</section>
