<div class="row" id="sms-content"> 
 <div class="col-md-4">
    <section class="card">
      <header class="card-header">
        SMS Composer
        
      </header>
      <div class="card-block">
        <form class="form-horizontal" name="send-sms" id="send-sms">
          <input type="hidden" name="_token" value="{{ csrf_token() }}" />
          <div class="form-group row">
            <label class="col-sm-3  form-control-label">Send To:</label>
            <div class="col-sm-9">
              <p class="form-control-static"><input type="text" name="phone_number" required class="form-control" id="phone_number" placeholder="Enter Phone Number(s)"></p>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3  form-control-label">Message:</label>
            <div class="col-sm-9">
              <p class="form-control-static"><textarea class="form-control" id="text" name="message" maxlength="280" required placeholder="Type in your message" rows="3"></textarea></p>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-3 offset-sm-8">
              <button type="button" id="sendSms" class="btn btn-success">
                Send
              </button>
            </div>
          </div>
        </form>
      </div>
    </section>
  </div> 
  <div class="col-md-8">
    <section class="card">
      <header class="card-header">
        Test Results
        
      </header>
      <div class="card-block">
        <table id="test-results" class="display table table-striped table-bordered" cellspacing="0" width="100%">
      <thead>
      <tr>
        <th>#</th>
        <th>Message</th>
        <th>Receiver</th>
        <th>Sender</th>
        <th>Delivery Status</th>
        <th></th>
      </tr>
      </thead>
      <tbody>
      @foreach($tests as $index =>$test)
      <tr>
        <td>{{ $index+1 }}</td>
        <td>{{ $test->content }}</td>
        <td>{{ $test->receiver }}</td>
        <td>{{ $test->sender }}</td>
        <td>{{ $test->delivery_status }}</td>
        <td></td>
      </tr>
      @endforeach
      </tbody>
    </table>
      </div>
    </section>
  </div>
</div>

