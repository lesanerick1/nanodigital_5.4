@extends('layouts.master')

@section('header_scripts')
<link rel="stylesheet" href="{{ asset('assets/css/lib/bootstrap-table/bootstrap-table.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/lib/datatables-net/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/datatables-net.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/pages/widgets.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/bootstrap-datetimepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/bootstrap-daterangepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/lib/fullcalendar/fullcalendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/pages/calendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/lib/clockpicker/bootstrap-clockpicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/select2.min.css') }}">
<style>
.m-progress-bar {
    min-height: 1em;
    background: #c12d2d;
    width: 5%;
}
</style>
@stop

@section('content')

<div class="page-content">
	
    <div class="container-fluid">
    	<section class="tabs-section" id="tabss">
			<div class="tabs-section-nav tabs-section-nav-inline pull-right" id="myTab">
				<ul class="nav pull-right" role="tablist">
					<li class="nav-item">
						<a class="nav-link " href="#overview" role="tab" data-toggle="tab">
							Overview
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link " href="#send" role="tab" data-toggle="tab">
							Send SMS
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link active" href="#campaigns" role="tab" data-toggle="tab">
							Campaigns
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#scheduler" role="tab" data-toggle="tab">
							Scheduler
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#analysis" role="tab" data-toggle="tab">
							Analysis
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#inventory" role="tab" data-toggle="tab">
							Inventory
						</a>
					</li>
				</ul>
			</div><!--.tabs-section-nav-->

			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade" id="overview">
					<p>&nbsp;</p>
					@include('bulk-sms.overview')
				</div><!--.tab-pane-->
				<div role="tabpanel" class="tab-pane fade " id="send">
					<p>&nbsp;</p>
					@include('bulk-sms.test')
				</div><!--.tab-pane-->
				<div role="tabpanel" class="tab-pane fade in active" id="campaigns">
					<p>&nbsp;</p>
					@include('bulk-sms.campaigns')
				</div><!--.tab-pane-->
				<div role="tabpanel" class="tab-pane fade" id="scheduler">
					<p>&nbsp;</p>
					@include('bulk-sms.scheduler')
				</div><!--.tab-pane-->
				<div role="tabpanel" class="tab-pane fade" id="analysis">
					<p>&nbsp;</p>
					@include('bulk-sms.analysis')
				</div><!--.tab-pane-->
				<div role="tabpanel" class="tab-pane fade" id="inventory">
					<p>&nbsp;</p>
					@include('bulk-sms.inventory')
				</div><!--.tab-pane-->
			</div><!--.tab-content-->
		</section><!--.tabs-section-->
	</div>
</div>
@stop

@section('footer_scripts')
<script>
$(document).ready(function(){
	$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
		localStorage.setItem('activeTab', $(e.target).attr('href'));
	});
	var activeTab = localStorage.getItem('activeTab');
	if(activeTab){
		$('#myTab a[href="' + activeTab + '"]').tab('show');
	}
});
</script>
<script type="text/javascript" src="{{ asset('assets/js/lib/moment/moment-with-locales.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lib/eonasdan-bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/fullcalendar/fullcalendar.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/fullcalendar/fullcalendar-init.js') }}"></script>	
<script type="text/javascript" src="{{ asset('assets/js/lib/match-height/jquery.matchHeight.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/bootstrap-table/bootstrap-table.js') }}"></script>
<script src="{{ asset('assets/js/lib/bootstrap-table/bootstrap-table-filter-control.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/bootstrap-table/bootstrap-table-filter-control-init.js') }}"></script>
<script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/clockpicker/bootstrap-clockpicker.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/clockpicker/bootstrap-clockpicker-init.js') }}"></script>
<script src="{{ asset('assets/js/lib/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/js/multi-step-modal.js') }}"></script>
<script>
sendEvent = function(sel, step) {
    $(sel).trigger('next.m.' + step);
}
</script>
<script>

    // Function for converting the Broadcast time to 12 hr clock system
    function tConv24(time24) {
      var ts = time24;
      var H = +ts.substr(0, 2);
      var h = (H % 12) || 12;
      h = (h < 10)?("0"+h):h;  // leading 0 at the left for 1 digit hours
      var ampm = H < 12 ? " AM" : " PM";
      ts = h + ts.substr(2, 3) + ampm;
      return ts;
    };

    $(function() {
         $('#table-campaigns').DataTable({
          processing: true,
          serverSide: true,
          ajax: '{{ route('bulk-sms.campaigns')}}',
          columns: [

              { data: 'index' },
              { data: 'name', name: 'campaign.name' },
              { data: 'rc_company.name', name:'company.name' },
              { data: 'subject', name: 'campaign.subject' },
               { data: 'info.broadcast_time',
                    render: function(data, type, row){
                        return data;
                       // fdata = tConv24(data);
                       // return fdata;
                    }
                 }, 
              { data: 'info.intervals', name: 'campaignInfo.interval' }, 
              
              
              { 
              data: 'status',
              render: function ( data, type, row ) {
                if ( data == 'Inactive')
                  data = '<span class="label label-warning">Inactive</span>';
                else if ( data == 'Active')
                  data = '<span class="label label-success">Active</span>';
                else
                  data = " ";
                return data;
                }
              },
              // {data: 'status'},
              { data: 'run' },
              { data: 'analysis' },

              { data: 'actions', name: 'actions', orderable: true, searchable: true }
          ],

          initComplete: function () {

            this.api().columns([2,3,4]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }

        });
        // table.on( 'draw', function () {
        //         $('.livicon').each(function(){
        //             $(this).updateLivicon();
        //         });
        //     } );


        });


</script>
<script>
$(function () {
    // Create the chart
    Highcharts.chart('container1', {
        chart: {
            type: 'pie'
        },
        colors: ['#008000', '#FFA500', '#FF0000'],
        title: {
            text: 'Message Deliveries [ALL CAMPAIGNS]'
        },
        subtitle: {
            text: 'Click the slices to view deliveries per network.'
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y:.1f}%'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },
        series: [{
            name: 'DELIVERY STATUS',
            colorByPoint: true,
            data: [{
                name: 'DELIVERED',
                y: 56.33,
                drilldown: 'delivered'
            }, {
                name: 'PENDING',
                y: 24.03,
                drilldown: 'pending'
            }, {
                name: 'NON DELIVERED',
                y: 10.38,
                drilldown: 'rejected'
            }]
        }],
        drilldown: {
            series: [{
                name: 'Delivered',
                colors: ['#008000', '#FF0000', '#FFA500','#000000'],
                id: 'delivered',
                data: [
                    [
                        'SAFARICOM',
                        24.13
                    ],
                    [
                        'AIRTEL',
                        17.2
                    ],
                    [
                        'TELKOM KENYA',
                        8.11
                    ],
                    [
                        'OTHERS',
                        5.33
                    ]
                ]
            }, {
                name: 'Pending',
                id: 'pending',
                data: [
                    [
                        'SAFARICOM',
                        5
                    ],
                    [
                        'AIRTEL',
                        4.32
                    ],
                    [
                        'TELKOM KENYA',
                        3.68
                    ],
                    [
                        'OTHERS',
                        2.96
                    ]
                ]
            }, {
                name: 'Rejected',
                id: 'rejected',
                data: [
                    [
                        'SAFARICOM',
                        2.76
                    ],
                    [
                        'AIRTEL',
                        2.32
                    ],
                    [
                        'TELKOM KENYA',
                        2.31
                    ],
                    [
                        'OTHERS',
                        1.27
                    ]
                ]
            }]
        }
    });
});

$(function () {
    // Create the chart
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        colors: ['#008000', '#FFA500', '#FF0000'],
        title: {
            text: 'Message Deliveries [ALL CAMPAIGNS]'
        },
        subtitle: {
            text: 'Click the columns to view deliveries per network.'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Number of SMS'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },

        series: [{
            name: 'DELIVERY STATUS',
            colorByPoint: true,
            data: [{
                name: 'DELIVERED',
                y: 56.33,
                drilldown: 'delivered'
            }, {
                name: 'PENDING',
                y: 24.03,
                drilldown: 'pending'
            }, {
                name: 'NON DELIVERED',
                y: 10.38,
                drilldown: 'rejected'
            }]
        }],
        drilldown: {
            series: [{
                name: 'Delivered',
                colors: ['#008000', '#FF0000', '#FFA500','#000000'],
                id: 'delivered',
                data: [
                    [
                        'SAFARICOM',
                        24.13
                    ],
                    [
                        'AIRTEL',
                        17.2
                    ],
                    [
                        'TELKOM KENYA',
                        8.11
                    ],
                    [
                        'OTHERS',
                        5.33
                    ]
                ]
            }, {
                name: 'Pending',
                id: 'pending',
                data: [
                    [
                        'SAFARICOM',
                        5
                    ],
                    [
                        'AIRTEL',
                        4.32
                    ],
                    [
                        'TELKOM KENYA',
                        3.68
                    ],
                    [
                        'OTHERS',
                        2.96
                    ]
                ]
            }, {
                name: 'Rejected',
                id: 'rejected',
                data: [
                    [
                        'SAFARICOM',
                        2.76
                    ],
                    [
                        'AIRTEL',
                        2.32
                    ],
                    [
                        'TELKOM KENYA',
                        2.31
                    ],
                    [
                        'OTHERS',
                        1.27
                    ]
                ]
            }]
        }
    });
});
</script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script>
$(function() {
	function cb(start, end) {
				$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			}
			cb(moment().subtract(29, 'days'), moment());

	$('#daterange').daterangepicker({
		"timePicker": true,
		ranges: {
			'Today': [moment(), moment()],
			'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			'This Month': [moment().startOf('month'), moment().endOf('month')],
			'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		},
		"linkedCalendars": false,
		"autoUpdateInput": false,
		"alwaysShowCalendars": true,
		"showWeekNumbers": true,
		"showDropdowns": true,
		"showISOWeekNumbers": true
	});

	$('#daterange3').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true
	});

	$('#daterange4').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true
	});

	$('#daterange').on('show.daterangepicker', function(ev, picker) {
		/*$('.daterangepicker select').selectpicker({
			size: 10
		});*/
	});
});
</script>
<script src="{{ asset('assets/js/lib/jquery-validation/jquery.validate.min.js') }}"></script>
	
<script>
	$(function() {
		$('#test-results').DataTable();
	});
</script>
<script>
	$(document).ready(function() {
    var $table = $('#table-campaigns');

    $table.bootstrapTable({
        iconsPrefix: 'font-icon',
        icons: {
            paginationSwitchDown:'font-icon-arrow-square-down',
            paginationSwitchUp: 'font-icon-arrow-square-down up',
            refresh: 'font-icon-refresh',
            toggle: 'font-icon-list-square',
            columns: 'font-icon-list-rotate',
            export: 'font-icon-download',
            detailOpen: 'font-icon-plus',
            detailClose: 'font-icon-minus-1'
        },
        paginationPreText: '<i class="font-icon font-icon-arrow-left"></i>',
        paginationNextText: '<i class="font-icon font-icon-arrow-right"></i>'
        //,
        //data: data
    });

    /*$('#table-campaigns select').selectpicker({
        style: '',
        width: '100%',
        size: 8
    });*/
});
</script>
<script>
$(document).ready(function(){ 	
	$('#tabs').tabs();
	$("#sendSms").live(function(){
		$("#sendSms").innerHTML = "Sending...";
		//$("#tabs").tab();
		$.ajax({
			type: "POST",
			url: "{!! route('sendSms') !!}",
			data: $('#send-sms').serialize(),
			success: function(message){ 
				//$("#addShortCode").modal('hide');
				$( "#tabss" ).load(window.location.href + " #tabss>*","" );
				$.notify({
		            icon: 'font-icon font-icon-check-circle',
		            message: 'SMS sent successfully.'
		        },{
		            type: 'success'
		        });
			},
			error: function(){
				//$("#addShortCode").modal('hide');
				$( "#sms-content" ).load(window.location.href + " #sms-content>*","" );
				$.notify({
		            icon: 'font-icon font-icon-warning',
		            message: 'Something went wrong. Try again.'
		        },{
		            type: 'danger'
		        });
			}
		});

		var tabId = $("#myTab").tabs("option", "active");
		$("#myTab").tabs("option", "active", tabId);
		$("#myTab").tabs("load", tabId);
	});
	
});

$(document).ready(function(){ 	
	$("#createCampaign").click(function(){
		$("#createCampaign").innerHTML = "Creating...";
		$.ajax({
			type: "POST",
			url: "{!! route('createCampaign') !!}",
			data: $('#create-campaign').serialize(),
			success: function(message){ 
				$("#create-campaign").modal('hide');
				
				$( "#create-campaign" ).load(window.location.href + " #create-campaign>*","" );
				$.notify({
		            icon: 'font-icon font-icon-check-circle',
		            message: 'New SMS campaign created successfully.'
		        },{
		            type: 'success'
		        });
			},
			error: function(){
				$("#create-campaign").modal('hide');
				$( "#create-campaign" ).load(window.location.href + " #create-campaign>*","" );
				$.notify({
		            icon: 'font-icon font-icon-warning',
		            message: 'Something went wrong. Try again.'
		        },{
		            type: 'danger'
		        });
			}
		});
	});
	
});
$(document).ready(function () {

    $('#hidden').hide();

    $('#hidden1').hide();

    $('#hidden2').hide();

    $('#hidden3').hide();

    $(".radio").change(function () { //use change event

        if (this.value == "datepicker") { //check value if it is datepicker

            $('#hidden').stop(true,true).show(200); //than show

        } else {

            $('#hidden').stop(true,true).hide(200); //else hide

        }

    });

    $(".radio1").change(function () { //use change event

        if (this.value == "datepicker") { //check value if it is datepicker

            $('#hidden1').stop(true,true).show(200); //than show

        } else {

            $('#hidden1').stop(true,true).hide(200); //else hide

        }

    });

    $(".radio2").change(function () { //use change event

        if (this.value == "specific") { //check value if it is datepicker

            $('#hidden2').stop(true,true).show(200); //than show

        } else {

            $('#hidden2').stop(true,true).hide(200); //else hide

        }

    });

    $(".radio3").change(function () { //use change event

        if (this.value == "specific") { //check value if it is datepicker

            $('#hidden3').stop(true,true).show(200); //than show

        } else {

            $('#hidden3').stop(true,true).hide(200); //else hide

        }

    });

});
</script>
@stop