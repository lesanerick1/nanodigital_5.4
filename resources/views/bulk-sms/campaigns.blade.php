<section class="box-typical">
	<header class="box-typical-header">
		<div class="tbl-row">
			<div class="tbl-cell tbl-cell-title">
				<h3>Campaigns</h3>
			</div>
			<div class="tbl-cell tbl-cell-actions">
				<a href="#" data-toggle="modal" data-target="#create-campaign" class="btn btn-primary"><i class="font-icon-left font-icon-plus"></i>New Campaign</a>
			</div>
		</div>
	</header>
	<div class="table-responsive">
		<table id="table-campaigns"
			   data-filter-control="true"
			   data-toolbar="#toolbar"
			   class="table table-bordered">
			<thead>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Company</th>
				<th>Advert</th>
				<th>Broadcast Time</th>
				<th>Interval</th>
				<th>Status</th>
				<th>Run</th>
				<th>Analysis</th>
				<th>Actions</th>
			</tr>
			</thead>
			<tfoot>
			<tr>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
			</tr>
			</tfoot>
		</table>
	</div>
</section><!--.box-typical-->

<form class="modal multi-step" name="create-campaign" id="create-campaign">
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
	            <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
					<i class="font-icon-close-2"></i>
				</button>
				<h4 class="modal-title" id="myModalLabel">Add New SMS Campaign</h4>
                <!--<h4 class="modal-title step-1" data-step="1">Step 1</h4>
                <h4 class="modal-title step-2" data-step="2">Step 2</h4>
                <h4 class="modal-title step-3" data-step="3">Final Step</h4>-->
                <p></p>
                <div class="m-progress">
                    <div class="m-progress-bar-wrapper">
                        <div class="m-progress-bar">
                        </div>
                    </div>
                    <div class="m-progress-stats">
                        <span class="m-progress-current">
                        </span>
                        /
                        <span class="m-progress-total">
                        </span>
                    </div>
                    <div class="m-progress-complete">
                        Completed
                    </div>
                </div>
            </div>
            <div class="modal-body step-1" data-step="1">
                <div class="form-group row">
							<label class="col-sm-3  form-control-label" for="company">Company:</label>
				            <div class="col-sm-9">
				              <p class="form-control-static">
				              	<select id="company" name="company" required class="form-control">
									<option value="" selected>Select Company</option>
									@foreach($companies as $company)
									<option value="{{ $company->id }}">{{ $company->name }}</option>
									@endforeach
								</select>
				              </p>
				            </div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3  form-control-label" for="company">Campaign Name:</label>
				            <div class="col-sm-9">
				              <p class="form-control-static"><input type="text" class="form-control" name="name" placeholder="Enter campaign name" required></p>
				            </div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3  form-control-label" for="company">Message:</label>
				            <div class="col-sm-9">
				              <p class="form-control-static">
				              <textarea class="form-control" name="message" placeholder="Type in your message" cols="10" rows="2" required></textarea>
				              </p>
				            </div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3  form-control-label" for="company">Send To:</label>
				            <div class="col-sm-9">
				                <p class="form-control-static input-group clockpicker" data-autoclose="true">
					                <select class="select2" name="groups[]" multiple="multiple" placeholder="Select customer groups">
					                	<option value="1000000" selected >All Customer Groups</option>
										@foreach($groups as $group)
										<option value="{{ $company->id }}">{{ $group->name }}</option>
										@endforeach
									</select>
								</p>
				            </div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3  form-control-label" for="company">Network Carriers:</label>
				            <div class="col-sm-9">
				                <p class="form-control-static input-group clockpicker" data-autoclose="true">
					                <select class="select2" name="carriers[]" multiple="multiple" placeholder="Select customer groups">
					                	<option value="1000000" selected >All Network Carriers</option>
										@foreach($carriers as $carrier)
										<option value="{{ $company->id }}">{{ $carrier->name }}</option>
										@endforeach
									</select>
								</p>
				            </div>
						</div>
            </div>
            <div class="modal-body step-2" data-step="2">
                <div class="form-group row">
							<label class="col-sm-3  form-control-label" for="company">Start Date:</label>
				            <div class="col-sm-9">
				                <p class="form-control-static">
					                <style>.none { display:none; } </style>
                                    <label class="radio-inline"><input class="radio" name="start_date" type="radio" value="1" checked>Immediately</label>
                                    <label class="radio-inline"><input class="radio" type="radio" name="start_date" value="datepicker">Custom Date</label>
                                    <div id="hidden" ><br/><input type="text" class="form-control" name="start_custom_date" id="daterange3"/></div>
								</p>
				            </div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3  form-control-label" for="company">End Date:</label>
				            <div class="col-sm-9">
				                <p class="form-control-static">
					                <label class="radio-inline"><input class="radio1" id="" type="radio" name="end_date" value="0" checked>No End Dates</label>
                                    <label class="radio-inline"><input class="radio1" type="radio" name="end_date" value="datepicker" >Custom Date</label>
                                    <div id="hidden1" ><br/><input type="text" name="end_custom_date" class="form-control" id="daterange4"/></div>
								</p>
				            </div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3  form-control-label" for="company">Broadcast Time:</label>
				            <div class="col-sm-9">
				                <p class="form-control-static input-group clockpicker" data-autoclose="true">
					                <input type="text" class="form-control" name="broadcast_time" value="13:00">
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-time"></span>
									</span>
								</p>
				            </div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3  form-control-label" for="company">Broadcast Interval:</label>
				            <div class="col-sm-9">
				                <p class="form-control-static">
					                <select id="intervals" name="intervals" required class="form-control">
									<option value="" selected>Select Broadcast Interval</option>
									<option value="Once">One Time Broadcast</option>
									<option value="Daily">Daily</option>
									<option value="Weekly">Weekly</option>
									<option value="Monthly">Monthly</option>
								</select>
								</p>
				            </div>
						</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success step step-1" data-step="1" onclick="sendEvent('#create-campaign', 2)">Continue</button>
                <button type="button" class="btn btn-primary step step-2" data-step="2" onclick="sendEvent('#create-campaign', 1)">Back</button>
                <button type="button" id="createCampaign" class="btn btn-success step step-2" data-step="2" onclick="sendEvent('#create-campaign', 3)">Continue</button>
            </div>
        </div>
    </div>
</form>
	


					