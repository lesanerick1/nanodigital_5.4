<div class="row">
	<div class="col-xl-3">
		<section class="widget widget-simple-sm">
			<div class="widget-simple-sm-statistic">
				<div class="number">1 426</div>
				<div class="caption color-blue">Active Campaigns</div>
			</div>
			<div class="widget-simple-sm-bottom statistic"><span class="arrow color-green">↑</span> View Campaigns<strong></strong></div>
		</section><!--.widget-simple-sm-->
	</div>
	<div class="col-xl-3">
		<section class="widget widget-simple-sm">
			<div class="widget-simple-sm-statistic">
				<div class="number">63 541</div>
				<div class="caption color-purple">Total Campaigns</div>
			</div>
			<div class="widget-simple-sm-bottom statistic"><span class="arrow color-green">↑</span> View Campaigns <strong></strong></div>
		</section><!--.widget-simple-sm-->
	</div>
	<div class="col-xl-3">
		<section class="widget widget-simple-sm">
			<div class="widget-simple-sm-statistic">
				<div class="number">852 / 100,000</div>
				<div class="caption color-red">Message Usage</div>
			</div>
			<div class="widget-simple-sm-bottom statistic"><span class="arrow color-green">↑</span> View Inventory <strong></strong></div>
		</section><!--.widget-simple-sm-->
	</div>
	<div class="col-xl-3">
		<section class="widget widget-simple-sm">
			<div class="widget-simple-sm-statistic">
				<div class="number">31/12/2017</div>
				<div class="caption color-green">Bundle Expiry</div>
			</div>
			<div class="widget-simple-sm-bottom statistic"><span class="arrow color-red">↓</span> RENEW <strong></strong></div>
		</section><!--.widget-simple-sm-->
	</div>
</div><!--.row-->

<div class="row">
	<div class="col-md-6">
		<div class="box-typical">
			<div class="calendar">
				<div class="calendar">
					<div class="calendar-page-title">Scheduler</div>
					<div class="calendar">
						<div id='calendar'></div>
					</div><!--.calendar-page-content-in-->
				</div><!--.calendar-page-content-->				
			</div><!--.calendar-page-->
		</div><!--.box-typical-->
	</div>
	<div class="col-md-6">
		<div class="box-typical">
			
		</div><!--.box-typical-->
	</div>
</div>

