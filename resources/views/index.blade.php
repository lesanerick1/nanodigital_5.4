@extends('layouts.master')

@section('header_scripts')
<link rel="stylesheet" href="{{ asset('assets/css/lib/lobipanel/lobipanel.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/lobipanel.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/lib/jqueryui/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/pages/widgets.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/bootstrap-datetimepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/lib/fullcalendar/fullcalendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/pages/calendar.min.css') }}">
@stop

@section('content')

	<div class="page-content">
	    <div class="container-fluid">
	        <div class="row">
	            <div class="col-xl-6">
	                <div class="chart-statistic-box">
	                    <div class="chart-txt">
	                        <div class="chart-txt-top">
	                            <p><span class="unit">$</span><span class="number">1540</span></p>
	                            <p class="caption">Week income</p>
	                        </div>
	                        <table class="tbl-data">
	                            <tr>
	                                <td class="price color-purple">120$</td>
	                                <td>Orders</td>
	                            </tr>
	                            <tr>
	                                <td class="price color-yellow">15$</td>
	                                <td>Investments</td>
	                            </tr>
	                            <tr>
	                                <td class="price color-lime">55$</td>
	                                <td>Others</td>
	                            </tr>
	                        </table>
	                    </div>
	                    <div class="chart-container">
	                        <div class="chart-container-in">
	                            <div id="chart_div"></div>
	                            <header class="chart-container-title">Income</header>
	                            <div class="chart-container-x">
	                                <div class="item"></div>
	                                <div class="item">tue</div>
	                                <div class="item">wed</div>
	                                <div class="item">thu</div>
	                                <div class="item">fri</div>
	                                <div class="item">sat</div>
	                                <div class="item">sun</div>
	                                <div class="item">mon</div>
	                                <div class="item"></div>
	                            </div>
	                            <div class="chart-container-y">
	                                <div class="item">300</div>
	                                <div class="item"></div>
	                                <div class="item">250</div>
	                                <div class="item"></div>
	                                <div class="item">200</div>
	                                <div class="item"></div>
	                                <div class="item">150</div>
	                                <div class="item"></div>
	                                <div class="item">100</div>
	                                <div class="item"></div>
	                                <div class="item">50</div>
	                                <div class="item"></div>
	                            </div>
	                        </div>
	                    </div>
	                </div><!--.chart-statistic-box-->
	            </div><!--.col-->
	            <div class="col-xl-6">
	                <div class="row">
	                	<div class="col-sm-6">
							<section class="widget widget-simple-sm">
								<div class="widget-simple-sm-statistic">
									<div class="number">1 426</div>
									<div class="caption color-blue">New orders</div>
								</div>
								<div class="widget-simple-sm-bottom statistic"><span class="arrow color-green">↑</span> 3% increase <strong>1w ago</strong></div>
							</section>
						</div>
						<div class="col-sm-6">
							<section class="widget widget-simple-sm">
								<div class="widget-simple-sm-statistic">
									<div class="number">63 541</div>
									<div class="caption color-purple">Total sales gross</div>
								</div>
								<div class="widget-simple-sm-bottom statistic"><span class="arrow color-green">↑</span> 3% increase <strong>1w ago</strong></div>
							</section>
						</div>
						<div class="col-sm-6">
							<section class="widget widget-simple-sm">
								<div class="widget-simple-sm-statistic">
									<div class="number">852</div>
									<div class="caption color-red">New orders</div>
								</div>
								<div class="widget-simple-sm-bottom statistic"><span class="arrow color-green">↑</span> 3% increase <strong>1w ago</strong></div>
							</section>
						</div>
						<div class="col-sm-6">
							<section class="widget widget-simple-sm">
								<div class="widget-simple-sm-statistic">
									<div class="number">87</div>
									<div class="caption color-green">Total sales gross</div>
								</div>
								<div class="widget-simple-sm-bottom statistic"><span class="arrow color-red">↓</span> 6% decrease <strong>1w ago</strong></div>
							</section>
						</div>	                	
	                    <!--<div class="col-sm-6">
	                        <article class="statistic-box red">
	                            <div>
	                                <div class="number">26</div>
	                                <div class="caption"><div>Open tickets</div></div>
	                                <div class="percent">
	                                    <div class="arrow up"></div>
	                                    <p>15%</p>
	                                </div>
	                            </div>
	                        </article>
	                    </div>
	                    <div class="col-sm-6">
	                        <article class="statistic-box purple">
	                            <div>
	                                <div class="number">12</div>
	                                <div class="caption"><div>Closes tickets</div></div>
	                                <div class="percent">
	                                    <div class="arrow down"></div>
	                                    <p>11%</p>
	                                </div>
	                            </div>
	                        </article>
	                    </div>
	                    <div class="col-sm-6">
	                        <article class="statistic-box yellow">
	                            <div>
	                                <div class="number">104</div>
	                                <div class="caption"><div>New clients</div></div>
	                                <div class="percent">
	                                    <div class="arrow down"></div>
	                                    <p>5%</p>
	                                </div>
	                            </div>
	                        </article>
	                    </div>
	                    <div class="col-sm-6">
	                        <article class="statistic-box green">
	                            <div>
	                                <div class="number">29</div>
	                                <div class="caption"><div>Here is an example of a long name</div></div>
	                                <div class="percent">
	                                    <div class="arrow up"></div>
	                                    <p>84%</p>
	                                </div>
	                            </div>
	                        </article>
	                    </div>-->
	                </div><!--.row-->
	            </div><!--.col-->
	        </div><!--.row-->
			<div class="row">
				
			</div><!--.row-->
	        <div class="row">
	            <div class="col-xl-6 dahsboard-column">
	                <section class="box-typical box-typical-dashboard panel panel-default scrollable">
						<div class="box-typical">
							<div class="calendar">
								<div class="calendar">
									<div class="calendar-page-title">Scheduler</div>
									<div class="calendar">
										<div id='calendar-overview'></div>
									</div><!--.calendar-page-content-in-->
								</div><!--.calendar-page-content-->				
							</div><!--.calendar-page-->
						</div><!--.box-typical-->
	                </section><!--.box-typical-dashboard-->   
	            </div><!--.col-->
	            <div class="col-xl-6 dahsboard-column">
	                <section class="box-typical box-typical-dashboard panel panel-default scrollable">
	                    <header class="box-typical-header panel-heading">
	                        <h3 class="panel-title">Recent messages</h3>
	                    </header>
	                    <div class="box-typical-body panel-body">
	                        <table class="tbl-typical">
	                            <tr>
	                                <th><div>Status</div></th>
	                                <th><div>Subject</div></th>
	                                <th align="center"><div>Department</div></th>
	                                <th align="center"><div>Date</div></th>
	                            </tr>
	                            <tr>
	                                <td>
	                                    <span class="label label-success">Open</span>
	                                </td>
	                                <td>Website down for one week</td>
	                                <td align="center">Support</td>
	                                <td nowrap align="center"><span class="semibold">Today</span> 8:30</td>
	                            </tr>
	                            <tr>
	                                <td>
	                                    <span class="label label-success">Open</span>
	                                </td>
	                                <td>Restoring default settings</td>
	                                <td align="center">Support</td>
	                                <td nowrap align="center"><span class="semibold">Today</span> 16:30</td>
	                            </tr>
	                            <tr>
	                                <td>
	                                    <span class="label label-warning">Progress</span>
	                                </td>
	                                <td>Loosing control on server</td>
	                                <td align="center">Support</td>
	                                <td nowrap align="center"><span class="semibold">Yesterday</span></td>
	                            </tr>
	                            <tr>
	                                <td>
	                                    <span class="label label-danger">Closed</span>
	                                </td>
	                                <td>Authorizations keys</td>
	                                <td align="center">Support</td>
	                                <td nowrap align="center">23th May</td>
	                            </tr>
	                        </table>
	                    </div><!--.box-typical-body-->
	                </section><!--.box-typical-dashboard-->
	                <section class="box-typical box-typical-dashboard panel panel-default scrollable">
	                    
	                </section>
	            </div><!--.col-->
	        </div>
	    </div><!--.container-fluid-->
	</div><!--.page-content-->

@stop

@section('footer_scripts')

<script type="text/javascript" src="{{ asset('assets/js/lib/jqueryui/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lib/lobipanel/lobipanel.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lib/match-height/jquery.matchHeight.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lib/moment/moment-with-locales.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lib/eonasdan-bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/fullcalendar/fullcalendar.min.js') }}"></script>
<script>
	$(document).ready(function(){

/* ==========================================================================
    Fullcalendar
    ========================================================================== */

    $('#calendar-overview').fullCalendar({
        header: {
            left: '',
            center: 'prev, title, next',
            right: 'today agendaDay,agendaTwoDay,agendaWeek,month'
        },
        buttonIcons: {
            prev: 'font-icon font-icon-arrow-left',
            next: 'font-icon font-icon-arrow-right',
            prevYear: 'font-icon font-icon-arrow-left',
            nextYear: 'font-icon font-icon-arrow-right'
        },
        defaultDate: '2016-01-12',
        editable: true,
        selectable: true,
        eventLimit: true, // allow "more" link when too many events
        events: [
            {
                title: 'All Day Event',
                start: '2016-01-01'
            },
            {
                title: 'Long Event',
                start: '2016-01-07',
                end: '2016-01-10',
                className: 'event-green'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: '2016-01-09T16:00:00',
                className: 'event-red'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: '2016-01-16T16:00:00',
                className: 'event-orange'
            },
            {
                title: 'Conference',
                start: '2016-01-11',
                end: '2016-01-13',
                className: 'event-coral'
            },
            {
                title: 'Meeting',
                start: '2016-01-12T10:30:00',
                end: '2016-01-12T12:30:00',
                className: 'event-green'
            },
            {
                title: 'Lunch',
                start: '2016-01-12T12:00:00'
            },
            {
                title: 'Meeting',
                start: '2016-01-12T14:30:00',
                className: 'event-red'
            },
            {
                title: 'Happy Hour',
                start: '2016-01-12T17:30:00'
            },
            {
                title: 'Dinner',
                start: '2016-01-12T20:00:00',
                className: 'event-orange'
            },
            {
                title: 'Birthday Party',
                start: '2016-01-13T07:00:00'
            },
            {
                title: 'Click for Google',
                url: 'http://google.com/',
                start: '2016-01-28',
                className: 'event-coral'
            }
        ],
        viewRender: function(view, element) {
            // При переключении вида инициализируем нестандартный скролл
            if (!("ontouchstart" in document.documentElement)) {
                $('.fc-scroller').jScrollPane({
                    autoReinitialise: true,
                    autoReinitialiseDelay: 100
                });
            }

            $('.fc-popover.click').remove();
        },
        eventClick: function(calEvent, jsEvent, view) {

            var eventEl = $(this);

            // Add and remove event border class
            if (!$(this).hasClass('event-clicked')) {
                $('.fc-event').removeClass('event-clicked');

                $(this).addClass('event-clicked');
            }

            // Add popover
            $('body').append(
                '<div class="fc-popover click">' +
                    '<div class="fc-header">' +
                        moment(calEvent.start).format('dddd • D') +
                        '<button type="button" class="cl"><i class="font-icon-close-2"></i></button>' +
                    '</div>' +

                    '<div class="fc-body main-screen">' +
                        '<p>' +
                            moment(calEvent.start).format('dddd, D YYYY, hh:mma') +
                        '</p>' +
                        '<p class="color-blue-grey">Name Surname Patient<br/>Surgey ACL left knee</p>' +
                        '<ul class="actions">' +
                            '<li><a href="#">More details</a></li>' +
                            '<li><a href="#" class="fc-event-action-edit">Edit event</a></li>' +
                            '<li><a href="#" class="fc-event-action-remove">Remove</a></li>' +
                        '</ul>' +
                    '</div>' +

                    '<div class="fc-body remove-confirm">' +
                        '<p>Are you sure to remove event?</p>' +
                        '<div class="text-center">' +
                            '<button type="button" class="btn btn-rounded btn-sm">Yes</button>' +
                            '<button type="button" class="btn btn-rounded btn-sm btn-default remove-popover">No</button>' +
                        '</div>' +
                    '</div>' +

                    '<div class="fc-body edit-event">' +
                        '<p>Edit event</p>' +
                        '<div class="form-group">' +
                            '<div class="input-group date datetimepicker">' +
                                '<input type="text" class="form-control" />' +
                                '<span class="input-group-addon"><i class="font-icon font-icon-calend"></i></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<div class="input-group date datetimepicker-2">' +
                                '<input type="text" class="form-control" />' +
                                '<span class="input-group-addon"><i class="font-icon font-icon-clock"></i></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<textarea class="form-control" rows="2">Name Surname Patient Surgey ACL left knee</textarea>' +
                        '</div>' +
                        '<div class="text-center">' +
                            '<button type="button" class="btn btn-rounded btn-sm">Save</button>' +
                            '<button type="button" class="btn btn-rounded btn-sm btn-default remove-popover">Cancel</button>' +
                        '</div>' +
                    '</div>' +
                '</div>'
            );

            // Datepicker init
            $('.fc-popover.click .datetimepicker').datetimepicker({
                widgetPositioning: {
                    horizontal: 'right'
                }
            });

            $('.fc-popover.click .datetimepicker-2').datetimepicker({
                widgetPositioning: {
                    horizontal: 'right'
                },
                format: 'LT',
                debug: true
            });


            // Position popover
            function posPopover(){
                $('.fc-popover.click').css({
                    left: eventEl.offset().left + eventEl.outerWidth()/2,
                    top: eventEl.offset().top + eventEl.outerHeight()
                });
            }

            posPopover();

            $('.fc-scroller, .calendar-page-content, body').scroll(function(){
                posPopover();
            });

            $(window).resize(function(){
               posPopover();
            });


            // Remove old popover
            if ($('.fc-popover.click').length > 1) {
                for (var i = 0; i < ($('.fc-popover.click').length - 1); i++) {
                    $('.fc-popover.click').eq(i).remove();
                }
            }

            // Close buttons
            $('.fc-popover.click .cl, .fc-popover.click .remove-popover').click(function(){
                $('.fc-popover.click').remove();
                $('.fc-event').removeClass('event-clicked');
            });

            // Actions link
            $('.fc-event-action-edit').click(function(e){
                e.preventDefault();

                $('.fc-popover.click .main-screen').hide();
                $('.fc-popover.click .edit-event').show();
            });

            $('.fc-event-action-remove').click(function(e){
                e.preventDefault();

                $('.fc-popover.click .main-screen').hide();
                $('.fc-popover.click .remove-confirm').show();
            });
        }
    });

	
/* ==========================================================================
    Side datepicker
    ========================================================================== */

    $('#side-datetimepicker').datetimepicker({
        inline: true,
        format: 'DD/MM/YYYY'
    });

/* ========================================================================== */

});


/* ==========================================================================
    Calendar page grid
    ========================================================================== */

(function($, viewport){
    $(document).ready(function() {

        if(viewport.is('>=lg')) {
            $('.calendar-page-content, .calendar-page-side').matchHeight();
        }

        // Execute code each time window size changes
        $(window).resize(
            viewport.changed(function() {
                if(viewport.is('<lg')) {
                    $('.calendar-page-content, .calendar-page-side').matchHeight({ remove: true });
                }
            })
        );
    });
})(jQuery, ResponsiveBootstrapToolkit);
</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>
	$(document).ready(function() {
		$('.panel').lobiPanel({
			sortable: true
		});
		$('.panel').on('dragged.lobiPanel', function(ev, lobiPanel){
			$('.dahsboard-column').matchHeight();
		});

		google.charts.load('current', {'packages':['corechart']});
		google.charts.setOnLoadCallback(drawChart);
		function drawChart() {
			var dataTable = new google.visualization.DataTable();
			dataTable.addColumn('string', 'Day');
			dataTable.addColumn('number', 'Values');
			// A column for custom tooltip content
			dataTable.addColumn({type: 'string', role: 'tooltip', 'p': {'html': true}});
			dataTable.addRows([
				['MON',  130, ' '],
				['TUE',  130, '130'],
				['WED',  180, '180'],
				['THU',  175, '175'],
				['FRI',  200, '200'],
				['SAT',  170, '170'],
				['SUN',  250, '250'],
				['MON',  220, '220'],
				['TUE',  220, ' ']
			]);

			var options = {
				height: 314,
				legend: 'none',
				areaOpacity: 0.18,
				axisTitlesPosition: 'out',
				hAxis: {
					title: '',
					textStyle: {
						color: '#fff',
						fontName: 'Proxima Nova',
						fontSize: 11,
						bold: true,
						italic: false
					},
					textPosition: 'out'
				},
				vAxis: {
					minValue: 0,
					textPosition: 'out',
					textStyle: {
						color: '#fff',
						fontName: 'Proxima Nova',
						fontSize: 11,
						bold: true,
						italic: false
					},
					baselineColor: '#16b4fc',
					ticks: [0,25,50,75,100,125,150,175,200,225,250,275,300,325,350],
					gridlines: {
						color: '#1ba0fc',
						count: 15
					},
				},
				lineWidth: 2,
				colors: ['#fff'],
				curveType: 'function',
				pointSize: 5,
				pointShapeType: 'circle',
				pointFillColor: '#f00',
				backgroundColor: {
					fill: '#008ffb',
					strokeWidth: 0,
				},
				chartArea:{
					left:0,
					top:0,
					width:'100%',
					height:'100%'
				},
				fontSize: 11,
				fontName: 'Proxima Nova',
				tooltip: {
					trigger: 'selection',
					isHtml: true
				}
			};

			var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
			chart.draw(dataTable, options);
		}
		$(window).resize(function(){
			drawChart();
			setTimeout(function(){
			}, 1000);
		});
	});
</script>

@stop