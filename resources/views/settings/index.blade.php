@extends('layouts.master')

@section('header_scripts')
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/lib/bootstrap-table/bootstrap-table.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/lib/datatables-net/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/datatables-net.min.css') }}">
@stop

@section('content')

<div class="page-content">
	
    <div class="container-fluid">
    	<section class="tabs-section">
			<div class="tabs-section-nav tabs-section-nav-inline pull-right">
				<ul class="nav pull-right" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" href="#short-codes" role="tab" data-toggle="tab">
							Short Codes
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link " href="#services" role="tab" data-toggle="tab">
							Services
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#payment-methods" role="tab" data-toggle="tab">
							Payment Methods
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#notifications" role="tab" data-toggle="tab">
							Notifications
						</a>
					</li>
					<!--<li class="nav-item">
						<a class="nav-link" href="#tabs-4-tab-4" role="tab" data-toggle="tab">
							Phone Types
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#phone-types" role="tab" data-toggle="tab">
							Phone Types
						</a>
					</li>-->
				</ul>
			</div><!--.tabs-section-nav-->

			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade  in active" id="short-codes">
					<p>&nbsp;</p>
					@include('settings.short-codes')
				</div><!--.tab-pane-->
				<div role="tabpanel" class="tab-pane fade" id="services">
					<p>&nbsp;</p>
					@include('settings.services')
				</div><!--.tab-pane-->
				<div role="tabpanel" class="tab-pane fade" id="payment-methods">
					<p>&nbsp;</p>
					@include('settings.payment')
				</div><!--.tab-pane-->
				<div role="tabpanel" class="tab-pane fade" id="notifications">
					<p>&nbsp;</p>
					@include('settings.notifications')
				</div><!--.tab-pane-->
			</div><!--.tab-content-->
		</section><!--.tabs-section-->
	</div>
</div>
@stop

@section('footer_scripts')
<script src="{{ asset('assets/js/lib/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/bootstrap-table/bootstrap-table.js') }}"></script>
<script src="{{ asset('assets/js/lib/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/table-edit/jquery.tabledit.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
<script>
$(function () {
	$('#shortcodes-table').DataTable();
	$('#shortcodes-table').Tabledit({
		url: "{!! route('groups.edit') !!}",
		
		columns: {
			identifier: [0, 'id'],
			editable: [[1, 'short_code'], [2, 'sender_id']]
		},
		buttons: {
		    edit: {
		      class: 'btn btn-sm btn-primary',
		      html: '<span class="glyphicon glyphicon-pencil"></span>',
		      action: 'edit'
		    },
		    delete: {
		      class: 'btn btn-sm btn-danger',
		      html: '<span class="glyphicon glyphicon-trash"></span>',
		      action: 'delete'
		    },
		    save: {
		      class: 'btn btn-sm btn-success',
		      html: 'Save'
		    },
		    restore: {
		      class: 'btn btn-sm btn-warning',
		      html: 'Restore',
		      action: 'restore'
		    },
		    confirm: {
		      class: 'btn btn-sm btn-danger',
		      html: 'Delete'
		    }
		  },
		onDraw: function() {
	        console.log('onDraw()');
	    },
	    onSuccess: function(data, textStatus, jqXHR) {
	        console.log('onSuccess(data, textStatus, jqXHR)');
	        console.log(data);
	        console.log(textStatus);
	        console.log(jqXHR);
	    },
	    onFail: function(jqXHR, textStatus, errorThrown) {
	        console.log('onFail(jqXHR, textStatus, errorThrown)');
	        console.log(jqXHR);
	        console.log(textStatus);
	        console.log(errorThrown);
	    },
	    onAlways: function() {
	        console.log('onAlways()');
	    },
	    onAjax: function(action, serialize) {
	        console.log('onAjax(action, serialize)');
	        console.log(action);
	        console.log(serialize);
	    }
	});
});
</script>
<script>
$(document).ready(function() {
    var $table = $('#table-campaigns');

    $table.bootstrapTable({
        iconsPrefix: 'font-icon',
        icons: {
            paginationSwitchDown:'font-icon-arrow-square-down',
            paginationSwitchUp: 'font-icon-arrow-square-down up',
            refresh: 'font-icon-refresh',
            toggle: 'font-icon-list-square',
            columns: 'font-icon-list-rotate',
            export: 'font-icon-download',
            detailOpen: 'font-icon-plus',
            detailClose: 'font-icon-minus-1'
        },
        paginationPreText: '<i class="font-icon font-icon-arrow-left"></i>',
        paginationNextText: '<i class="font-icon font-icon-arrow-right"></i>'
        //,
        //data: data
    });

    /*$('#table-campaigns select').selectpicker({
        style: '',
        width: '100%',
        size: 8
    });*/
});
</script>
<script>
$(document).ready(function(){ 	
	$("#createSC").click(function(){
		$.ajax({
			type: "POST",
			url: "{!! route('createShortCode') !!}",
			data: $('#create-shortcode').serialize(),
			success: function(message){ 
				$("#addShortCode").modal('hide');
				//$( "#content" ).load(window.location.href + " #content" );
				$.notify({
		            icon: 'font-icon font-icon-check-circle',
		            message: 'Short code added successfully.'
		        },{
		            type: 'success'
		        });
			},
			error: function(){
				$("#addShortCode").modal('hide');
				$.notify({
		            icon: 'font-icon font-icon-warning',
		            message: 'Something went wrong. Try again.'
		        },{
		            type: 'danger'
		        });
			}
		});
	});
	
});
/*$(document).ready(function(){
$("#createSC").click(function(){
$.ajax({
type: "POST",
url: "{{ route('createShortCode') }}",
data: $('#create-shortcode').serialize(),
success: function(message){
},
error: function(){
alert("Error");
}
});
});
});*/
</script>
@stop