<section class="box-typical box-typical">
	<header class="box-typical-header">
		<div class="tbl-row">
			<div class="tbl-cell tbl-cell-title">
				<h3>Short Codes</h3>
			</div>
			<div class="tbl-cell tbl-cell-actions">
				<a href="#" data-toggle="modal" data-target="#addShortCode" class="btn btn-primary"><i class="font-icon-left font-icon-plus"></i>Add Short Code</a>
			</div>
		</div>

	</header>
	<div class="box-typical-body" id="content">
		<div class="table-responsive">
			<table id="shortcodes-table" class="table table-bordered table-hover">
				<thead>
				<tr>
					<th width="1">#</th>
					<th>Short Code</th>
					<th>Sender ID</th>
					<th>Company</th>
				</tr>
				</thead>
				<tbody>
					@foreach($shortcodes as $index => $shortcode)
					<tr>
						<td>{{ $index+1 }}</td>
						<td>{{ $shortcode->short_code }}</td>
						<td>{{ $shortcode->sender_id }}</td>
						<td >{!! App\Company::find($shortcode->company)->name !!}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</section>

<div class="modal fade" id="addShortCode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form name="create-shortcode" id="create-shortcode">
				<div class="modal-header">
					<button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
						<i class="font-icon-close-2"></i>
					</button>
					<h4 class="modal-title" id="myModalLabel">Add New Short Code</h4>
				</div>
				<div class="modal-body">
                    <!-- CSRF Token -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-group row">
						<label class="col-sm-3 offset-sm-1 form-control-label">Company:</label>
						<div class="col-sm-7">
							<p class="form-control-static"> 
							<select id="company" name="company" required class="form-control">
								<option value="" selected>Select Company</option>
								
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 offset-sm-1 form-control-label">Short Code Type:</label>
						<div class="col-sm-7">
							<p class="form-control-static"> 
							<select id="type" name="type" required class="form-control">
								<option value="" selected>Select Short Code Type</option>
								
							</select>
						</div>
					</div>
                    <div class="form-group row">
						<label class="col-sm-3 offset-sm-1 form-control-label">Short Code:</label>
						<div class="col-sm-7">
							<p class="form-control-static"> <input type="text" name="short_code" class="form-control" placeholder="Short Code" required value="">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 offset-sm-1 form-control-label">Sender ID:</label>
						<div class="col-sm-7">
							<p class="form-control-static"> <input type="text" name="sender_id" class="form-control" placeholder="Sender ID" value="">
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" id="createSC" class="btn btn-success">Create</button>
				</div>
            </form>
		</div>
	</div>
</div><!--.modal-->
