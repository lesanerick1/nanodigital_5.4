@extends('layouts.master')

@section('header_scripts')
<link rel="stylesheet" href="{{ asset('assets/css/lib/bootstrap-table/bootstrap-table.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/lib/datatables-net/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/datatables-net.min.css') }}">
@stop

@section('content')

<div class="page-content">
	
    <div class="container-fluid">
    	<section class="tabs-section">
			<div class="tabs-section-nav tabs-section-nav-inline pull-right">
				<ul class="nav pull-right" role="tablist">
					<li class="nav-item">
						<a class="nav-link" href="#overview" role="tab" data-toggle="tab">
							Overview
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link active" href="#influencers" role="tab" data-toggle="tab">
							Influencers
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#sentiment" role="tab" data-toggle="tab">
							Sentiment Analysis
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#trends" role="tab" data-toggle="tab">
							Trends
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#mapping" role="tab" data-toggle="tab">
							Mapping
						</a>
					</li>
					<!--<li class="nav-item">
						<a class="nav-link" href="#tabs-4-tab-4" role="tab" data-toggle="tab">
							Phone Types
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#phone-types" role="tab" data-toggle="tab">
							Phone Types
						</a>
					</li>-->
				</ul>
			</div><!--.tabs-section-nav-->

			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade" id="overview">
					<p>&nbsp;</p>
				</div><!--.tab-pane-->
				<div role="tabpanel" class="tab-pane fade in active" id="customers">
					<p>&nbsp;</p>
				</div><!--.tab-pane-->
				<div role="tabpanel" class="tab-pane fade" id="customer-groups">
					<p>&nbsp;</p>
				</div><!--.tab-pane-->
				<!--<div role="tabpanel" class="tab-pane fade" id="phone-types">
				<p>&nbsp;</p>
				</div><!--.tab-pane-->
			</div><!--.tab-content-->
		</section><!--.tabs-section-->
	</div>
</div>
@stop

@section('footer_scripts')
<script src="{{ asset('assets/js/lib/bootstrap-table/bootstrap-table.js') }}"></script>
<script src="{{ asset('assets/js/lib/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
<script>
$(document).ready(function() {
    var $table = $('#table-campaigns');

    $table.bootstrapTable({
        iconsPrefix: 'font-icon',
        icons: {
            paginationSwitchDown:'font-icon-arrow-square-down',
            paginationSwitchUp: 'font-icon-arrow-square-down up',
            refresh: 'font-icon-refresh',
            toggle: 'font-icon-list-square',
            columns: 'font-icon-list-rotate',
            export: 'font-icon-download',
            detailOpen: 'font-icon-plus',
            detailClose: 'font-icon-minus-1'
        },
        paginationPreText: '<i class="font-icon font-icon-arrow-left"></i>',
        paginationNextText: '<i class="font-icon font-icon-arrow-right"></i>'
        //,
        //data: data
    });

    /*$('#table-campaigns select').selectpicker({
        style: '',
        width: '100%',
        size: 8
    });*/
});
</script>

@stop