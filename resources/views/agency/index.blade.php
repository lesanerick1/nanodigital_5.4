@extends('layouts.master')

@section('header_scripts')
<link rel="stylesheet" href="{{ asset('assets/css/lib/datatables-net/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/datatables-net.min.css') }}">

@stop

@section('content')
<div class="page-content">
	<div class="container-fluid">
    <div class="row">
    	<div class="col-md-12">
        <section class="card">
          <header class="card-header">
            Agencies
            <button type="button" class="modal-close">
              <a href="users/create" class="btn btn-inline btn-primary btn-sm ladda-button"><i class="font-icon-left font-icon-plus"></i>Add Agency</a>
            </button>
          </header>
          <div class="card-block">
            <table id="agencies" class="table table-bordered" >
          <!-- <table id="agencies"
            class="table table-striped"
             data-search="true"
             data-show-refresh="false"
             data-show-toggle="false"
             data-show-columns="false"
             data-show-export="true"
             data-detail-view="false"
             data-minimum-count-columns="2"
             data-show-pagination-switch="true"
             data-pagination="true"
             data-id-field="id"
             data-page-list="[10, 25, 50, 100]"
             data-show-footer="false"
             data-filter-control="true"
             data-response-handler="responseHandler"
             data-toolbar="#toolbar"> -->
          <thead>
            <tr>
              <th>#</th>
              <th>Logo</th>
              <th>Agency Name</th>
              <th>No Of Companies</th>
              <th data-filter-control="select" >Status</th>
              <th>Actions</th>
            </tr>
          </thead>
            
          <tfoot>
            <tr>
              <th>#</th>
              <th></th>
              <th>Agency Name</th>
              <th>No Of Companies</th>
              <th>Status</th>
              <th></th>
            </tr>
          </tfoot>
          
        </table>
        <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="create" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title" id="user_delete_confirm_title">Delete Campaign</h4>
                    </div>
                    <div class="modal-body">
                      Are you sure you want to Delete this Campaign??
                      <input id="ids" name="ids" type="text" />
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                      <a href="">Confirm</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>

@stop

@section('footer_scripts')


<!-- <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
<script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script>

<script>
	$(function() {
    

		 var table =$('#agencies').DataTable({
      dom: 'Bfrtipl',
              buttons: [
                  'copy', 'excel', 'pdf','print'
              ],
      processing: true,
      serverSide: false,
      ajax: '{!! route('agency.main') !!}',
      columns: [
          { data: 'id' },
          { data: 'logo' },
          { data: 'name' },
          { data: 'name' },
          // { data: 'status' },
          { 
              data: 'status',
              render: function ( data, type, row ) {
                if ( data == 'Inactive')
                  data = '<span class="label label-warning">Inactive</span>';
                else if ( data == 'Active')
                  data = '<span class="label label-success">Active</span>';
                else
                  data = " ";
                return data;
                }
              },                     
          { data: 'actions', name: 'actions', orderable: true, searchable: true }

      ],

      initComplete: function () {

            this.api().columns([2,3,4]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }

    });
    table.column(0).visible(false);
    // table.on( 'draw', function () {
    //         $('.livicon').each(function(){
    //             $(this).updateLivicon();
    //         });
    //     } );


	});


    // listening for show event of editAluno's modal
  $( '#delete_confirm' ).on( 'show.bs.modal', function (e) {
      var target = e.relatedTarget;
      // get values for particular rows
      var tr = $( target ).closest( 'tr' );
      var tds = tr.find( 'td' );

      // put values into editor's form elements
      // tds.eq(0).val() -- 1st column
      $( '#ids' ).val( tds.eq(4).val() );
      // alert( tds.eq(0).val());
      alert(JSON.stringify(tds, null, 4));
      // tds.eq(1).val() -- 2nd column and so on.
      // same goes to others element
  });
</script>
@stop