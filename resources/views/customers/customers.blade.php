<section class="box-typical">
	<header class="box-typical-header">
		<div class="tbl-row">
			<div class="tbl-cell tbl-cell-title">
				<h3>Customers</h3>
			</div>
			<div class="tbl-cell tbl-cell-actions">
				<a href="" data-target="#addCustomer" data-toggle="modal" class="btn btn-sm btn-success"><i class="font-icon-left font-icon-plus"></i>New Customer</a>&nbsp;&nbsp;
				<a href="" data-target="#importCustomers" data-toggle="modal" class="btn btn-sm btn-primary"><i class="font-icon-left font-icon-upload"></i>Import Customer</a>
			</div>
		</div>
	</header>
	<div class="table-responsive">
		<table id="table-campaigns"
			   data-filter-control="true"
			   data-toolbar="#toolbar">
			<thead>
				<tr>
					<th>#</th>
					<th>Phone Number</th>
					<th>Name</th>
					<th>Country</th>
					<th>Carrier</th>
					<th>Company</th>
					<th>Status</th>
					<!-- <th>Actions</th> -->
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>#</th>
					<th>Phone Number</th>
					<th>Name</th>
					<th>Country</th>
					<th>Carrier</th>
					<th>Company</th>
					<th>Status</th>
					<!-- <th>Actions</th> -->
				</tr>
			</tfoot>
			<tbody>
				
			</tbody>
		</table>
	</div>
</section><!--.box-typical-->

<div class="modal fade" id="importCustomers" tabindex="-1" role="dialog" aria-labelledby="create" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-dialog">
             <div class="modal-content">
             <form action="{{ route('importCustomers') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="user_delete_confirm_title">Import Customers</h4>
                    </div>
                    <div class="modal-body">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <input type="file" name="customer" />
                        <div>
							<br />
							Use this <a href="{{ asset('uploads/file/customers.csv')}}" download>template excel file</a> to import your customers easily.
						</div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                         <button type="submit" class="btn btn-primary">Import File</button>
                    </div>
                </form>
             </div>
        </div>
    </div>
</div>
	
