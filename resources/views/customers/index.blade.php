@extends('layouts.master')

@section('header_scripts')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@stop

@section('content')

<div class="page-content">
	
    <div class="container-fluid">
    	<section class="tabs-section">
			<div class="tabs-section-nav tabs-section-nav-inline pull-right">
				<ul class="nav pull-right" role="tablist">
					<li class="nav-item">
						<a class="nav-link" href="#overview" role="tab" data-toggle="tab">
							Overview
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link active" href="#customers" role="tab" data-toggle="tab">
							Customers
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#customer-groups" role="tab" data-toggle="tab">
							Customer Groups
						</a>
					</li>
					<!--<li class="nav-item">
						<a class="nav-link" href="#tabs-4-tab-4" role="tab" data-toggle="tab">
							Networks
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#tabs-4-tab-4" role="tab" data-toggle="tab">
							Phone Types
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#phone-types" role="tab" data-toggle="tab">
							Phone Types
						</a>
					</li>-->
				</ul>
			</div><!--.tabs-section-nav-->

			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade" id="overview">
					<p>&nbsp;</p>
					@include('customers.overview')
				</div><!--.tab-pane-->
				<div role="tabpanel" class="tab-pane fade in active" id="customers">
					<p>&nbsp;</p>
					@include('customers.customers')
				</div><!--.tab-pane-->
				<div role="tabpanel" class="tab-pane fade" id="customer-groups">
					<p>&nbsp;</p>
					@include('customers.groups')
				</div><!--.tab-pane-->
				<!--<div role="tabpanel" class="tab-pane fade" id="phone-types">
				<p>&nbsp;</p>
				</div><!--.tab-pane-->
			</div><!--.tab-content-->
		</section><!--.tabs-section-->
	</div>
</div>
@stop

@section('footer_scripts')


<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script>
	$(function() {
		 $('#table-campaigns').DataTable({
      // dom: 'Bfrtip',
      //       buttons: [
      //           'copy', 'excel', 'pdf','print'
      //       ],
      processing: true,
      serverSide: true,
      ajax: '{!! route('customer.main') !!}',
      columns: [

          { data: 'id' },
	          { data: 'phone_number' },
	          { data: 'name' },
	          { data: 'country' },
	          { data: 'carrier' },
	          { data: 'company' },
	          { data: 'status' }

	          //{ data: 'actions', name: 'actions', orderable: true, searchable: true }
      ],
      initComplete: function () {
            this.api().columns([2,3,4,5,6]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }

    });
    // table.on( 'draw', function () {
    //         $('.livicon').each(function(){
    //             $(this).updateLivicon();
    //         });
    //     } );


	});
</script>

@stop