@extends('layouts.master')

@section('header_scripts')
<link rel="stylesheet" href="{{ asset('assets/css/lib/datatables-net/datatables.min.css') }}">
<!-- <link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/datatables-net.min.css') }}"> -->
<link rel="stylesheet" href="{{ asset('assets/css/separate/pages/gallery.min.css') }}">
<link type="text/css" href="{{ asset('assets/css/zGallery.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('assets/css/separate/pages/widgets.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/bootstrap-datetimepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/lib/clockpicker/bootstrap-clockpicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/lib/fullcalendar/fullcalendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/pages/calendar.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/dropzone.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/bootstrap-select/bootstrap-select.min.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/lib/daterangepicker/daterangepicker.css') }}" />
<!-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" /> -->
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/select2.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/separate/vendor/jquery-steps.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/image-preview.css')  }}">

{!! Charts::assets() !!}
@stop

@section('content')

<div class="page-content">
	
    <div class="container-fluid">
    	<section class="tabs-section">
			<div class="tabs-section-nav tabs-section-nav-inline pull-right">
				<ul class="nav pull-right" role="tablist">
					<li class="nav-item">
						<a class="nav-link " href="#overview" role="tab" data-toggle="tab">
							Overview
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link " href="#composer" role="tab" data-toggle="tab">
							Composer
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link active" href="#campaigns" role="tab" data-toggle="tab">
							Campaigns
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#scheduler" role="tab" data-toggle="tab">
							Scheduler
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link " href="#analysis" role="tab" data-toggle="tab">
							Analysis
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#inventory" role="tab" data-toggle="tab">
							Inventory
						</a>
					</li>
				</ul>
			</div><!--.tabs-section-nav-->

			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade" id="overview">
					<p>&nbsp;</p>
					@include('picture-ads.overview')
				</div><!--.tab-pane-->
				<div role="tabpanel" class="tab-pane fade " id="composer">
					<p>&nbsp;</p>
					@include('picture-ads.composer')
				</div><!--.tab-pane-->
				<div role="tabpanel" class="tab-pane fade in active" id="campaigns">
					<p>&nbsp;</p>
					@include('picture-ads.campaigns')
				</div><!--.tab-pane-->
				<div role="tabpanel" class="tab-pane fade" id="scheduler">
					<p>&nbsp;</p>
					@include('picture-ads.scheduler')
				</div><!--.tab-pane-->
				<div role="tabpanel" class="tab-pane fade " id="analysis">
					<p>&nbsp;</p>
                    @include('picture-ads.analysis')
				</div><!--.tab-pane-->
				<div role="tabpanel" class="tab-pane fade" id="inventory">
					<p>&nbsp;</p>
				</div><!--.tab-pane-->
			</div><!--.tab-content-->
		</section><!--.tabs-section-->
	</div>
</div>
@stop

@section('footer_scripts')
<script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script>
<script>

    // Function for converting the Broadcast time to 12 hr clock system
    function tConv24(time24) {
      var ts = time24;
      var H = +ts.substr(0, 2);
      var h = (H % 12) || 12;
      h = (h < 10)?("0"+h):h;  // leading 0 at the left for 1 digit hours
      var ampm = H < 12 ? " AM" : " PM";
      ts = h + ts.substr(2, 3) + ampm;
      return ts;
    };

    $(function() {
         $('#table-campaigns').DataTable({
          processing: true,
          serverSide: true,
          ajax: '{!! route('picture-ads.campaigns') !!}',
          columns: [

              { data: 'index' },
              { data: 'name', name: 'campaign.name' },
              { data: 'rc_company.name', name:'company.name' },
              { data: 'subject', name: 'campaign.subject' },
               { data: 'info.broadcast_time',
                    render: function(data, type, row){
                       fdata = tConv24(data);
                       return fdata;
                    }
                 }, 
              { data: 'info.intervals', name: 'campaignInfo.interval' }, 
              
              
              { 
              data: 'status',
              render: function ( data, type, row ) {
                if ( data == 'Inactive')
                  data = '<span class="label label-warning">Inactive</span>';
                else if ( data == 'Active')
                  data = '<span class="label label-success">Active</span>';
                else
                  data = " ";
                return data;
                }
              },
              // {data: 'status'},
              { data: 'run' },
              { data: 'analysis' },

              { data: 'actions', name: 'actions', orderable: true, searchable: true }
          ],

          initComplete: function () {

            this.api().columns([2,3,4]).every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }

        });
        // table.on( 'draw', function () {
        //         $('.livicon').each(function(){
        //             $(this).updateLivicon();
        //         });
        //     } );


        });


</script>
<script>
    $(function() {
    

         var table =$('#campaign_analysis').DataTable({
          dom: 'Bfrtipl',
                buttons: [
                    'copy', 'excel', 'pdf','print'
                ],
          processing: true,
          serverSide: false,
          ajax: '',
          columns: [
              { data: 'id' },
              { data: 'receiver' },
              { data: 'subject' },
              { data: 'content' },
              // { data: 'status' },
              { 
                  data: 'delivery_status',
                  render: function ( data, type, row ) {
                    if ( data == 'Delivered')
                      data = '<span class="label label-success">Delivered</span>';
                    else if ( data == 'Pending')
                      data = '<span class="label label-warning">Pending</span>';
                    else
                      data = " ";
                    return data;
                    }
                  },                     
              

          ],

          initComplete: function () {

                this.api().columns([4]).every( function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );
     
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }

        });
        
        // table.on( 'draw', function () {
        //         $('.livicon').each(function(){
        //             $(this).updateLivicon();
        //         });
        //     } );


    });


  
</script>
<script src="{{ asset('assets/js/lib/select2/select2.full.min.js') }}"></script>
<script type="text/javascript">
    $(".sample-multiple-select").select2();
    $("#select-carrier").select2();
    $("#select-customer-group").select2();
</script>
<script src="{{ asset('assets/js/lib/moment/moment.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/js/lib/daterangepicker/daterangepicker.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/js/lib/moment/moment-with-locales.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lib/eonasdan-bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lib/clockpicker/bootstrap-clockpicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lib/clockpicker/bootstrap-clockpicker-init.js') }}"></script>

<script type="text/javascript">
    $(function () {
                $('#broadcast_date').datetimepicker({
                    widgetPositioning: {
                        horizontal: 'right',
                        vertical: 'bottom'
                    },
                    format: 'DD-MM-YYYY'
                });
                $('#broadcast_time').datetimepicker({
                    // datepicker:false,
                    widgetPositioning: {
                        vertical: 'bottom'
                    },
                    
                    format: 'hh:mm '      
                });
            });
    
</script>


<script type="text/javascript">
$(function() {

    $('#analysis_daterange').daterangepicker({
        ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Last 6 Months': [moment().subtract(6, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
        locale: {
            format: 'YYYY-MM-DD'
        },
                "linkedCalendars": false,
                "autoUpdateInput": true,
                "alwaysShowCalendars": true,
                "showWeekNumbers": true,
                "showDropdowns": true,
                "opens": 'left',
                "showISOWeekNumbers": true
    });
});
</script>
<script type="text/javascript">
    $('#analysis').submit({
        function(){

        }
    }) 
</script>
<!-- <script type="text/javascript">
    /* attach a submit handler to the form */
    $("#analysis").submit(function(event) {

      /* stop form from submitting normally */
      event.preventDefault();

      var formData = {
            'campaign'              : $('input[name=campaign]').val(),
            'daterange'             : $('input[name=daterange]').val()
            
        };

        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : '{{ route('picture-ads.analysis') }}', // the url where we want to POST
            data        : formData, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
                        encode          : true
        })

      /* Alerts the results */
      posting.done(function( data ) {
        alert('success');
      });
    });
</script> -->
<script type="text/javascript" src="{{ asset('assets/js/lib/match-height/jquery.matchHeight.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lib/moment/moment-with-locales.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/fullcalendar/fullcalendar.min.js') }}"></script>
<script>
	$(document).ready(function(){

/* ==========================================================================
    Fullcalendar
    ========================================================================== */

    $('#calendar-overview').fullCalendar({
        header: {
            left: '',
            center: 'prev, title, next',
            right: 'today agendaDay,agendaTwoDay,agendaWeek,month'
        },
        buttonIcons: {
            prev: 'font-icon font-icon-arrow-left',
            next: 'font-icon font-icon-arrow-right',
            prevYear: 'font-icon font-icon-arrow-left',
            nextYear: 'font-icon font-icon-arrow-right'
        },
        defaultDate: '2016-01-12',
        editable: true,
        selectable: true,
        eventLimit: true, // allow "more" link when too many events
        events: [
            {
                title: 'All Day Event',
                start: '2016-01-01'
            },
            {
                title: 'Long Event',
                start: '2016-01-07',
                end: '2016-01-10',
                className: 'event-green'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: '2016-01-09T16:00:00',
                className: 'event-red'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: '2016-01-16T16:00:00',
                className: 'event-orange'
            },
            {
                title: 'Conference',
                start: '2016-01-11',
                end: '2016-01-13',
                className: 'event-coral'
            },
            {
                title: 'Meeting',
                start: '2016-01-12T10:30:00',
                end: '2016-01-12T12:30:00',
                className: 'event-green'
            },
            {
                title: 'Lunch',
                start: '2016-01-12T12:00:00'
            },
            {
                title: 'Meeting',
                start: '2016-01-12T14:30:00',
                className: 'event-red'
            },
            {
                title: 'Happy Hour',
                start: '2016-01-12T17:30:00'
            },
            {
                title: 'Dinner',
                start: '2016-01-12T20:00:00',
                className: 'event-orange'
            },
            {
                title: 'Birthday Party',
                start: '2016-01-13T07:00:00'
            },
            {
                title: 'Click for Google',
                url: 'http://google.com/',
                start: '2016-01-28',
                className: 'event-coral'
            }
        ],
        viewRender: function(view, element) {
            // При переключении вида инициализируем нестандартный скролл
            if (!("ontouchstart" in document.documentElement)) {
                $('.fc-scroller').jScrollPane({
                    autoReinitialise: true,
                    autoReinitialiseDelay: 100
                });
            }

            $('.fc-popover.click').remove();
        },
        eventClick: function(calEvent, jsEvent, view) {

            var eventEl = $(this);

            // Add and remove event border class
            if (!$(this).hasClass('event-clicked')) {
                $('.fc-event').removeClass('event-clicked');

                $(this).addClass('event-clicked');
            }

            // Add popover
            $('body').append(
                '<div class="fc-popover click">' +
                    '<div class="fc-header">' +
                        moment(calEvent.start).format('dddd • D') +
                        '<button type="button" class="cl"><i class="font-icon-close-2"></i></button>' +
                    '</div>' +

                    '<div class="fc-body main-screen">' +
                        '<p>' +
                            moment(calEvent.start).format('dddd, D YYYY, hh:mma') +
                        '</p>' +
                        '<p class="color-blue-grey">Name Surname Patient<br/>Surgey ACL left knee</p>' +
                        '<ul class="actions">' +
                            '<li><a href="#">More details</a></li>' +
                            '<li><a href="#" class="fc-event-action-edit">Edit event</a></li>' +
                            '<li><a href="#" class="fc-event-action-remove">Remove</a></li>' +
                        '</ul>' +
                    '</div>' +

                    '<div class="fc-body remove-confirm">' +
                        '<p>Are you sure to remove event?</p>' +
                        '<div class="text-center">' +
                            '<button type="button" class="btn btn-rounded btn-sm">Yes</button>' +
                            '<button type="button" class="btn btn-rounded btn-sm btn-default remove-popover">No</button>' +
                        '</div>' +
                    '</div>' +

                    '<div class="fc-body edit-event">' +
                        '<p>Edit event</p>' +
                        '<div class="form-group">' +
                            '<div class="input-group date datetimepicker">' +
                                '<input type="text" class="form-control" />' +
                                '<span class="input-group-addon"><i class="font-icon font-icon-calend"></i></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<div class="input-group date datetimepicker-2">' +
                                '<input type="text" class="form-control" />' +
                                '<span class="input-group-addon"><i class="font-icon font-icon-clock"></i></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<textarea class="form-control" rows="2">Name Surname Patient Surgey ACL left knee</textarea>' +
                        '</div>' +
                        '<div class="text-center">' +
                            '<button type="button" class="btn btn-rounded btn-sm">Save</button>' +
                            '<button type="button" class="btn btn-rounded btn-sm btn-default remove-popover">Cancel</button>' +
                        '</div>' +
                    '</div>' +
                '</div>'
            );

            // Datepicker init
            // $('.fc-popover.click .datetimepicker').datetimepicker({
            //     widgetPositioning: {
            //         horizontal: 'right'
            //     }
            // });

            // $('.fc-popover.click .datetimepicker-2').datetimepicker({
            //     widgetPositioning: {
            //         horizontal: 'right'
            //     },
            //     format: 'LT',
            //     debug: true
            // });


            // Position popover
            function posPopover(){
                $('.fc-popover.click').css({
                    left: eventEl.offset().left + eventEl.outerWidth()/2,
                    top: eventEl.offset().top + eventEl.outerHeight()
                });
            }

            posPopover();

            $('.fc-scroller, .calendar-page-content, body').scroll(function(){
                posPopover();
            });

            $(window).resize(function(){
               posPopover();
            });


            // Remove old popover
            if ($('.fc-popover.click').length > 1) {
                for (var i = 0; i < ($('.fc-popover.click').length - 1); i++) {
                    $('.fc-popover.click').eq(i).remove();
                }
            }

            // Close buttons
            $('.fc-popover.click .cl, .fc-popover.click .remove-popover').click(function(){
                $('.fc-popover.click').remove();
                $('.fc-event').removeClass('event-clicked');
            });

            // Actions link
            $('.fc-event-action-edit').click(function(e){
                e.preventDefault();

                $('.fc-popover.click .main-screen').hide();
                $('.fc-popover.click .edit-event').show();
            });

            $('.fc-event-action-remove').click(function(e){
                e.preventDefault();

                $('.fc-popover.click .main-screen').hide();
                $('.fc-popover.click .remove-confirm').show();
            });
        }
    });

	$('#calendar-scheduler').fullCalendar({
        header: {
            left: '',
            center: 'prev, title, next',
            right: 'today agendaDay,agendaTwoDay,agendaWeek,month'
        },
        buttonIcons: {
            prev: 'font-icon font-icon-arrow-left',
            next: 'font-icon font-icon-arrow-right',
            prevYear: 'font-icon font-icon-arrow-left',
            nextYear: 'font-icon font-icon-arrow-right'
        },
        defaultDate: '2016-01-12',
        editable: true,
        selectable: true,
        eventLimit: true, // allow "more" link when too many events
        events: [
            {
                title: 'Long Event',
                start: '2016-01-07',
                end: '2016-01-10',
                className: 'event-green'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: '2016-01-09T16:00:00',
                className: 'event-red'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: '2016-01-16T16:00:00',
                className: 'event-orange'
            },
            {
                title: 'Conference',
                start: '2016-01-11',
                end: '2016-01-13',
                className: 'event-coral'
            },
            {
                title: 'Meeting',
                start: '2016-01-12T10:30:00',
                end: '2016-01-12T12:30:00',
                className: 'event-green'
            },
            {
                title: 'Lunch',
                start: '2016-01-12T12:00:00'
            },
            {
                title: 'Meeting',
                start: '2016-01-12T14:30:00',
                className: 'event-red'
            },
            {
                title: 'Happy Hour',
                start: '2016-01-12T17:30:00'
            },
            {
                title: 'Dinner',
                start: '2016-01-12T20:00:00',
                className: 'event-orange'
            },
            {
                title: 'Birthday Party',
                start: '2016-01-13T07:00:00'
            },
            {
                title: 'Click for Google',
                url: 'http://google.com/',
                start: '2016-01-28',
                className: 'event-coral'
            }
        ],
        viewRender: function(view, element) {
            // При переключении вида инициализируем нестандартный скролл
            if (!("ontouchstart" in document.documentElement)) {
                $('.fc-scroller').jScrollPane({
                    autoReinitialise: true,
                    autoReinitialiseDelay: 100
                });
            }

            $('.fc-popover.click').remove();
        },
        eventClick: function(calEvent, jsEvent, view) {

            var eventEl = $(this);

            // Add and remove event border class
            if (!$(this).hasClass('event-clicked')) {
                $('.fc-event').removeClass('event-clicked');

                $(this).addClass('event-clicked');
            }

            // Add popover
            $('body').append(
                '<div class="fc-popover click">' +
                    '<div class="fc-header">' +
                        moment(calEvent.start).format('dddd • D') +
                        '<button type="button" class="cl"><i class="font-icon-close-2"></i></button>' +
                    '</div>' +

                    '<div class="fc-body main-screen">' +
                        '<p>' +
                            moment(calEvent.start).format('dddd, D YYYY, hh:mma') +
                        '</p>' +
                        '<p class="color-blue-grey">Name Surname Patient<br/>Surgey ACL left knee</p>' +
                        '<ul class="actions">' +
                            '<li><a href="#">More details</a></li>' +
                            '<li><a href="#" class="fc-event-action-edit">Edit event</a></li>' +
                            '<li><a href="#" class="fc-event-action-remove">Remove</a></li>' +
                        '</ul>' +
                    '</div>' +

                    '<div class="fc-body remove-confirm">' +
                        '<p>Are you sure to remove event?</p>' +
                        '<div class="text-center">' +
                            '<button type="button" class="btn btn-rounded btn-sm">Yes</button>' +
                            '<button type="button" class="btn btn-rounded btn-sm btn-default remove-popover">No</button>' +
                        '</div>' +
                    '</div>' +

                    '<div class="fc-body edit-event">' +
                        '<p>Edit event</p>' +
                        '<div class="form-group">' +
                            '<div class="input-group date datetimepicker">' +
                                '<input type="text" class="form-control" />' +
                                '<span class="input-group-addon"><i class="font-icon font-icon-calend"></i></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<div class="input-group date datetimepicker-2">' +
                                '<input type="text" class="form-control" />' +
                                '<span class="input-group-addon"><i class="font-icon font-icon-clock"></i></span>' +
                            '</div>' +
                        '</div>' +
                        '<div class="form-group">' +
                            '<textarea class="form-control" rows="2">Name Surname Patient Surgey ACL left knee</textarea>' +
                        '</div>' +
                        '<div class="text-center">' +
                            '<button type="button" class="btn btn-rounded btn-sm">Save</button>' +
                            '<button type="button" class="btn btn-rounded btn-sm btn-default remove-popover">Cancel</button>' +
                        '</div>' +
                    '</div>' +
                '</div>'
            );

            // Datepicker init
            // $('.fc-popover.click .datetimepicker').datetimepicker({
            //     widgetPositioning: {
            //         horizontal: 'right'
            //     }
            // });

            // $('.fc-popover.click .datetimepicker-2').datetimepicker({
            //     widgetPositioning: {
            //         horizontal: 'right'
            //     },
            //     format: 'LT',
            //     debug: true
            // });


            // Position popover
            function posPopover(){
                $('.fc-popover.click').css({
                    left: eventEl.offset().left + eventEl.outerWidth()/2,
                    top: eventEl.offset().top + eventEl.outerHeight()
                });
            }

            posPopover();

            $('.fc-scroller, .calendar-page-content, body').scroll(function(){
                posPopover();
            });

            $(window).resize(function(){
               posPopover();
            });


            // Remove old popover
            if ($('.fc-popover.click').length > 1) {
                for (var i = 0; i < ($('.fc-popover.click').length - 1); i++) {
                    $('.fc-popover.click').eq(i).remove();
                }
            }

            // Close buttons
            $('.fc-popover.click .cl, .fc-popover.click .remove-popover').click(function(){
                $('.fc-popover.click').remove();
                $('.fc-event').removeClass('event-clicked');
            });

            // Actions link
            $('.fc-event-action-edit').click(function(e){
                e.preventDefault();

                $('.fc-popover.click .main-screen').hide();
                $('.fc-popover.click .edit-event').show();
            });

            $('.fc-event-action-remove').click(function(e){
                e.preventDefault();

                $('.fc-popover.click .main-screen').hide();
                $('.fc-popover.click .remove-confirm').show();
            });
        }
    });



/* ==========================================================================
    Side datepicker
    ========================================================================== */

    // $('#side-datetimepicker').datetimepicker({
    //     inline: true,
    //     format: 'DD/MM/YYYY'
    // });

/* ========================================================================== */

});


/* ==========================================================================
    Calendar page grid
    ========================================================================== */

(function($, viewport){
    $(document).ready(function() {

        if(viewport.is('>=lg')) {
            $('.calendar-page-content, .calendar-page-side').matchHeight();
        }

        // Execute code each time window size changes
        $(window).resize(
            viewport.changed(function() {
                if(viewport.is('<lg')) {
                    $('.calendar-page-content, .calendar-page-side').matchHeight({ remove: true });
                }
            })
        );
    });
})(jQuery, ResponsiveBootstrapToolkit);
</script>
<script>
var Interval = 6000;
var totalThumbsFirstList = 12;
var totalThumbsSecondList = 6;
</script>
<script src="{{ asset('assets/js/zGallery.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/jquery.livequery.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/dropzone.js') }}"></script>
<script src="{{ asset('assets/js/lib/bootstrap-select/bootstrap-select.min.js') }}"></script>

<script src="{{ asset('assets/js/lib/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
<script>
	$(function() {
		$('.gallery-item').matchHeight({
			target: $('.gallery-item .gallery-picture')
		});
	});
</script>
<script>
	$(function() {
		$('#test-results').DataTable();
	});
    $(function (){

            Dropzone.options.myAwesomeDropzone = {
                paramName: "files",
                uploadMultiple:true,
                maxFilesize:1,
                autoProcessQueue: true,
                uploadMultiple: true,
                addRemoveLinks: true,
                acceptedFiles: ".png, .jpg, .jpeg",
                dictFallbackMessage:"Your browser does not support drag'n'drop file uploads.",
                dictRemoveFile: "Remove",
                dictFileTooBig:"Image is bigger than 10MB",

                accept: function(file, done) {
                    console.log("Uploaded");
                    done();
                },

                init:function() {
                  var submitButton = document.querySelector('#submit-all')
                        myAwesomeDropzone = this;

                    // submitButton.addEventListener("click", function(
                    //     myAwesomeDropzone.processQueue();
                    // ));

                    this.on("addedfile", function(){
                        $('#submit-all').show();
                    });

                },  
                success: function(file,done){
                    console.log("All files done!");
                }
        }
        });
</script>
<script>
$(document).ready(function(){   
    $(".deleteImg").click(function(){
        var id = $(this).data("id");
        $.ajax({
            type: "POST",
            url: "{!! route('picture-ads.delete') !!}",
            data: $('#picture-ad-delete' +id).serialize(),
            success: function(message){ 
                $("#deleteImage" +id).modal('hide');
                $( "#composer" ).load(window.location.href + " #composer" );
                $.notify({
                    icon: 'font-icon font-icon-check-circle',
                    message: 'Image Deleted Successfully'
                },{
                    type: 'success'
                });
            },
            error: function(){
                $("#addShortCode").modal('hide');
                $.notify({
                    icon: 'font-icon font-icon-warning',
                    message: 'Something went wrong. Try again.'
                },{
                    type: 'danger'
                });
            }
        });
    });
    
});
</script>

<script>
$(document).ready(function(){   
    $(".runReport").click(function(){
        //var id = $(this).data("id");
        //alert($('#form-run-report').serialize());
        $.ajax({
            type: "POST",
            url: "{!! route('picture-ads.analysis') !!}",
            data: $('#form-run-report').serialize(),
            success: function(response){ 
                //console.log(message);

              alert(JSON.stringify(response.chart));
              var data1 = response.datatables.original.data;
              var table_analysis = $('#table_analysis').DataTable({
                processing: true,
                destroy: true,
                serverSide: false,
                data: data1,
                columns: [

                    { data: 'id' },
                    { data: 'receiver' },
                    { data: 'subject' },
                    { data: 'content' },
                    // { data: 'status' },
                    { 
                        data: 'delivery_status',
                        render: function ( data, type, row ) {
                          if ( data == 'Delivered')
                            data = '<span class="label label-success">Delivered</span>';
                          else if ( data == 'Pending')
                            data = '<span class="label label-warning">Pending</span>';
                          else
                            data = " ";
                          return data;
                          }
                        },
                ],

                initComplete: function () {

                  this.api().columns([2,3,4]).every( function () {
                      var column = this;
                      var select = $('<select><option value=""></option></select>')
                          .appendTo( $(column.footer()).empty() )
                          .on( 'change', function () {
                              var val = $.fn.dataTable.util.escapeRegex(
                                  $(this).val()
                              );
       
                              column
                                  .search( val ? '^'+val+'$' : '', true, false )
                                  .draw();
                          } );
       
                      column.data().unique().sort().each( function ( d, j ) {
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                      } );
                  } );
              }

              });



                // alert(JSON.stringify(message.chart));
                // $('.chart_view').removeClass('hidden');
                $('#txtHint').append(response.chart);
                
                document.getElementById("txtHint").innerHTML = response.chart;
                //$("#deleteImage" +id).modal('hide');
                //$( "#composer" ).load(window.location.href + " #composer" );
                // $('#'+uid).html('<img src="dislove.png" style=" border: none;">').addClass('unlike').removeClass('like');
                //  $('#you'+uid).text('');
                 $('body').append('<div class="row"><div class="col-md-12" style="text-align: center; "><section class="card"></section></div></div>')
                $.notify({
                    icon: 'font-icon font-icon-check-circle',
                    message: 'Report Generated Successfully'
                },{
                    type: 'success'
                });
            },
            error: function(){
                $("#addShortCode").modal('hide');
                $.notify({
                    icon: 'font-icon font-icon-warning',
                    message: 'Something went wrong. Try again.'
                },{
                    type: 'danger'
                });
            }
        });
    });
    
});
</script>
<script src="{{ asset('assets/js/lib/jquery-steps/jquery.steps.js') }}" type="text/javascript"></script>
<!-- <script type="text/javascript">
    $('#create-campaign').steps();
</script> -->
<script>

$("#createCampaign").steps({
    headerTag: "h1",
    bodyTag: "div",
    transitionEffect: "slideLeft",


        // onFinishing: function (event, currentIndex)
        // {
        //     var form = $("#create-campaign");

        //     // alert('Nothing');
        //     // Disable validation on fields that are disabled.
        //     // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
        //     form.validate().settings.ignore = ":disabled";

        //     // Start validation; Prevent form submission if false
        //     return form.valid();
        //     alert('Here');
        // },
        onFinished: function (event, currentIndex)
        {
            var form = $("#create-campaign");

            // Submit form input

            form.submit();
        }
        });

</script>
<script type="text/javascript" src="{{ asset('assets/js/image-preview.js') }}"></script>
@stop