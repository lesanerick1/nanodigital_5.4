<section class="box-typical">
	<header class="box-typical-header">
		<div class="tbl-row">
			<div class="tbl-cell tbl-cell-title">
				<h2>Picture Ads Analysis</h2>
			</div>
		</div>
	</header>
    <div class="row" style="padding-left:30px">
    	<form role="form" method="post" class="form-horizontal" id="form-run-report">
    	{{ csrf_field() }}
			<div class="col-md-4  form-group">
				<label for="campaign" control-label"><b>Select campaign/s:</b></label>
				<select class="select2 sample-multiple-select" name="campaign[]" id="campaign" multiple="multiple" >
					<option value="*" selected="">All Campaigns</option>
					@foreach($campaigns as $campaign)
						<option value="{{ $campaign->id }}">{{ $campaign->name }}</option>
					@endforeach
				</select>
			</div>
			<div class="col-md-5  form-group">
				<b>Select Analysis Period:</b>
				<input type="text" name="daterange" id="analysis_daterange" class="form-control" value="" />
			</div>
			<div class="col-sm-2 form-group">
				<br />
				<button type="button" id="runReport" class="btn  btn-primary btn-md btn-success runReport"><i class=" font-icon font-icon-search  "></i> &nbsp;Run Report</button>
			</div>
    	</form>
    </div>
</section>

<section class="box-typical">
	<header class="box-typical-header">
		<div class="tbl-row">
			<div class="tbl-cell tbl-cell-title">
				<h2></h2>
			</div>
		</div>
	</header>
    <div class="row" style="padding-left:30px">
    	<div class="col-md-8" >
    		<!-- <table id="table_analysis" class="display" width="100%"></table> -->
    		<table id="table_analysis" class="table table-bordered" >
	          
		          <thead>
		            <tr>
		              <th>#</th>
		              <th>Phone Number</th>  
		              <th>Subject</th>
		              <th>Content</th>
		              <th>Delivery Status</th>
		              
		            </tr>
		          </thead>
		            
		          <tfoot>
		            <tr>
		              <th>#</th>
		              <th>Phone Number</th>
		              <th>Subject</th>
		              <th>Content</th>
		              <th>Delivery Status</th>
		            </tr>
		          </tfoot>
	          
	        </table>
    	</div>
    	<div class="col-md-4" style="text-align: center; ">
	        <section class="card">
	          
	        </section>
	    </div>
</section>

		<div class="row">
	    	<div class="col-md-12" style="text-align: center; ">
		        <section class="card">
		          <span id="txtHint">
		          	
		          </span>
		        </section>
		    </div>
	    </div>
