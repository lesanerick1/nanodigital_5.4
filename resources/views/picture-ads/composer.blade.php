<div class="row">
	<div class="col-xxl-9 col-lg-12 col-xl-8 col-md-8">
		<section class="box-typical box-typical-full-height-with-header">
			<header class="box-typical-header box-typical-header-bordered">
				<div class="tbl-row">
					<div class="tbl-cell tbl-cell-title">
						<h3>Ad Composer</h3>
					</div>						
					<div class="tbl-cell tbl-cell-actions">
						<a href="#" data-toggle="modal" data-target="#uploadModal" class="btn btn-primary"><i class="font-icon-left font-icon-upload-2"></i>Upload</a>
						<!--<button type="button" class="action-btn view active">
							<i class="font-icon font-icon-view-grid"></i>
						</button>
						<button type="button" class="action-btn view">
							<i class="font-icon font-icon-view-rows"></i>
						</button>
						<button type="button" class="action-btn view">
							<i class="font-icon font-icon-view-cascade"></i>
						</button>-->
					</div>
				</div>
			</header>
			<div class="box-typical-body">
				<div class="gallery-grid">
					

					@foreach($uploaded_images as $image)
						 <div class="gallery-col" id="zThumbs">
							<article class="gallery-item" id="zLi-1">
								<img class="gallery-picture zImages" src="{{ '/uploads/' .$image->file_name }}" alt="" height="158">
								<div class="gallery-hover-layout">
									<div class="gallery-hover-layout-in">
										<p class="gallery-item-title">{{ $image->original_name }}</p>
										<p>by {{ $user->first_name ." " .$user->last_name}}</p>
										<div class="btn-group">
											<button id="edit" aria-controls="{{ '/uploads/' .$image->file_name }}" name="" type="button" class="btn">
												<i class="font-icon glyphicon glyphicon-send"></i>
											</button>
												
											<a href="#" data-toggle="modal" data-target="#deleteImage{{ $image->id }}">
												<button type="button" class="btn">
													<i class="font-icon font-icon-trash"></i>
												</button>
												
											</a>
											
											 
											
										</div>
										<p>{{ $image->created_at->diffForHumans()}}</p>
									</div>
								</div>
							</article>
						</div> 

						<div class="modal fade" id="deleteImage{{ $image->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
						        <form  role="form" class="form-horizontal" method="post"  id="picture-ad-delete{{$image->id}}">
									<div class="modal-header">
										<button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
											<i class="font-icon-close-2"></i>
										</button>
										<h4 class="modal-title" id="myModalLabel">Delete Image: {{$image->original_name}}</h4>
									</div>
									<div class="modal-body"> 
										<!— CSRF Token —>
										<input type="hidden" name="_token" value="{{ csrf_token() }}" />
										<input type="hidden" name="id" value="{{ $image->id }}" />
										<div class="form-group row">
										
											<div class="col-sm-8">
												<p> Are your sure you want to delete this image? </p>
											</div>
										</div>
										
										
										
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Cancel</button>
										<button type="button" class="btn btn-success btn-lg deleteImg" data-id="{{$image->id}}">&nbsp;&nbsp; Yes &nbsp;&nbsp;</button>
									</div>
								</form>
							</div>
						</div>
					</div><!--.modal-->


					@endforeach

					
			</div><!--.box-typical-body-->
			<footer class="box-typical-header box-typical-header-bordered">
				<div class="tbl-row">
					<div class="tbl-cell tbl-cell-title">
						<h3></h3>
					</div>						
					<div class="tbl-cell tbl-cell-actions">
						
						<button type="button" class="action-btn view ">
							<i class="font-icon font-icon-arrow-left"></i>
						</button>
						<button type="button" class="action-btn view active">
							<i class="font-icon font-icon-view-rows"></i>
						</button>
						<button type="button" class="action-btn view">
							<i class="font-icon font-icon-arrow-right"></i>
						</button>
					</div>
				</div>
			</footer>
		</section><!--.box-typical-->
	</div>
	<div class="col-xxl-3 col-lg-12 col-xl-4 col-md-4">
		<section class="card">
			<header class="card-header">
				Preview
				<span class="pull-right"><a href="" data-target="#send" data-toggle="modal" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-send"></span> Send</a>
				
			</header>
			<div class="card-block">
				<div id="iphone">
                  <div id="screen">
                    <div class="bs-example">
                      <div id="zMainImage" class="zMainImage" style="width:100% !important;height:80% !important; align:center !important; ">
                                    <div class="zSpace" style="margin-top:65px !important">
                                       <img src="images/1.jpg"  alt="" />
                                     </div>
                       </div>
                    </div>
                  </div>
              	<div class="home-button"></div>
            	</div>
			</div>
		</section>
	</div>
</div>
<section class="card">
	<header class="card-header">
		Test Results
	</header>
	<div class="card-block">
		<table id="test-results" class="display table table-striped table-bordered" cellspacing="0" width="100%">
			<thead>
			<tr>
				<th>#</th>
				<th>Ad Content</th>
				<th>Subject</th>
				<th>Sent To</th>
				<th>Sent On</th>
				<th>Sent By</th>
				<th>Delivery Status</th>
			</tr>
			</thead>
			<tbody>
			@foreach($tests as $index => $test)
			<tr>
				<td>{{ $index+1 }}</td>
				<td>{{ $test->content }}</td>
				<td>{{ $test->subject }}</td>
				<td>{{ $test->receiver }}</td>
				<td>{{ $test->broadcast_time }}</td>
				<td>{{ $test->sender }}</td>
				<td>{{ $test->delivery_status}}</td>
			</tr>
			@endforeach
			</tbody>
		</table>
	</div>
</section>

<div class="modal fade" id="send" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
	        <form  role="form" class="form-horizontal" method="post" action="{{route('send_mms')}}">
				<div class="modal-header">
					<button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
						<i class="font-icon-close-2"></i>
					</button>
					<h4 class="modal-title" id="myModalLabel">Send Test Picture/Video Ad</h4>
				</div>
				<div class="modal-body"> 
					<!— CSRF Token —>
					<input type="hidden" name="_token" value="{{ csrf_token() }}" />
					<div class="form-group row">
						<label class="col-sm-2 offset-sm-1 form-control-label">Send To:</label>
						<div class="col-sm-8">
							<p class="form-control-static"><input type="text" required class="form-control" id="phone" placeholder="Enter Phone Number(s)"></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 offset-sm-1 form-control-label">Subject:</label>
						<div class="col-sm-8">
							<p class="form-control-static"><textarea class="form-control" id="subject" name="subject" rows="2" placeholder="Subject"></textarea></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 offset-sm-1 form-control-label">Advert:</label>
						<div class="col-sm-8">
					       <div id="zMainImage" class="zMainImage">
					             <div class="zSpace">					              
					              </div>
					       </div>
					  </div>
					</div>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success">Send</button>
				</div>
			</form>
		</div>
	</div>
</div><!--.modal-->

<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
					 aria-hidden="true">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                    <i class="font-icon-close-2"></i>
                </button>
                <h4 class="modal-title" id="myModalLabel">Upload Files</h4>
            </div>
            <div class="modal-body">

            	<div class="row">
            	<div class="col-sm-12">
            	<form  action="{{ route('upload-files') }}" method="post" class="dropzone" id="my-awesome-dropzone">
            	{{ csrf_field() }}
            	</form>

            	</div>
            	</div>
            </div>
            <div class="modal-footer">
            	<button type="button" class="btn btn-success" data-dismiss="modal">Done</button>
            </div>
	    </div>
    </div>
</div><!--.modal-->


