<div class="modal fade" id="deleteImage{{ $image->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
	        <form  role="form" class="form-horizontal" method="post" action="{{route('send_mms')}}">
				<div class="modal-header">
					<button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
						<i class="font-icon-close-2"></i>
					</button>
					<h4 class="modal-title" id="myModalLabel">Send Test Picture/Video Ad</h4>
				</div>
				<div class="modal-body"> 
					<!— CSRF Token —>
					<input type="hidden" name="_token" value="{{ csrf_token() }}" />
					<div class="form-group row">
						<label class="col-sm-2 offset-sm-1 form-control-label">Send To:</label>
						<div class="col-sm-8">
							<p class="form-control-static"><input type="text" required class="form-control" id="phone" placeholder="Enter Phone Number(s)"></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 offset-sm-1 form-control-label">Subject:</label>
						<div class="col-sm-8">
							<p class="form-control-static"><textarea class="form-control" id="subject" name="subject" rows="2" placeholder="Subject"></textarea></p>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-2 offset-sm-1 form-control-label">Advert:</label>
						<div class="col-sm-8">
					       <div id="zMainImage" class="zMainImage">
					             <div class="zSpace">					              
					              </div>
					       </div>
					  </div>
					</div>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success">Send</button>
				</div>
			</form>
		</div>
	</div>
</div><!--.modal-->


