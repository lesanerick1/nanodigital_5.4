<section class="box-typical">
	<header class="box-typical-header">
		<div class="tbl-row">
			<div class="tbl-cell tbl-cell-title">
				<h3>Campaigns</h3>
			</div>
			<div class="tbl-cell tbl-cell-actions">
				<a href="#" data-toggle="modal" data-target="#newCampaign" class="btn btn-primary"><i class="font-icon-left font-icon-plus"></i>New Campaign</a>
			</div>
		</div>
	</header>
	<div class="table-responsive">
		<table id="table-campaigns"
			   data-filter-control="true"
			   data-toolbar="#toolbar"
			   class="table table-bordered">
			<thead>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Company</th>
				<th>Advert</th>
				<th>Broadcast Time</th>
				<th>Interval</th>
				<th>Status</th>
				<th>Run</th>
				<th>Analysis</th>
				<th>Actions</th>
			</tr>
			</thead>
			
			<tfoot>
			<tr>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
			</tr>
			</tfoot>
		</table>
	</div>
</section><!--.box-typical-->

<div class="modal fade " id="newCampaign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog  modal-lg">
		
		<div class="modal-content">
			<form name="create-campaign" id="create-campaign" class="form-horizontal form-wizard" method="post" action="{{ route('picture-ads.create') }}">
				<div class="modal-header">
					<button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
						<i class="font-icon-close-2"></i>
					</button>
					<h4 class="modal-title" id="myModalLabel">Create New Campaign</h4>
				</div>
				<div class="modal-body" id="createCampaign" style="max-height: 800px; z-index:0;">



                    <!-- CSRF Token -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    
					<h1 >Campaign Details</h1>
					<div >
						 <div class="form-group row">
							<label class="col-md-3  form-control-label">Campaign Name:</label>
							<div class="col-md-9">
								<p class="form-control-static"> 
								<input type="text" name="campaign_name" id="campaign_name" class="form-control" placeholder="Campaign Name"  value="">
							</div>
						</div>

						<div class="form-group row">
							<label class="col-md-3  form-control-label">Company Name:</label>
							<div class="col-md-9">
								<p class="form-control-static"> 
								<select id="company_name" name="company_name"  class="form-control">
									<option value="" selected>Select Company</option>
									@foreach($agencies as $agency)
	                                    <option  value="{{ $agency->id }}">{!!$agency->name !!}</option>
	                                @endforeach 
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3  form-control-label">Broadcast Time:</label>
							<div class="col-md-9 " id="broadcasttime">
								<p class="form-control-static"> 
								<input type="text" class="form-control " placeholder="Click Here for Clock" name="broadcast_time" id="broadcast_time"/>
                                    <!-- <span class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </span> -->
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3  form-control-label">Broadcast Date:</label>
							<div class="col-md-9   date " id="broadcastdate">
								<p class="form-control-static"> 
								<input type="text" class="form-control" placeholder="Click Here for Calendar" name="broadcast_date"  id="broadcast_date"/>
                                    <!-- <span class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </span> -->
							</div>
						</div>
                    </div>
				    <h1>Ad Content</h1>
				    <div>
				    	
				    	<div class="form-group row">
							<label class="col-md-3  form-control-label">Subject:</label>
							<div class="col-md-9">
								<p class="form-control-static"> 
								<textarea rows="3" cols="100" name="subject" id="subject" class="form-control" placeholder="Message Subject" value="">
									
								</textarea> 
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3  form-control-label">Attach Picture/Video:</label>
							<div class="col-md-9  image-preview">
								<span>
									<!-- image-preview-clear button -->
                                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                        <span class="glyphicon glyphicon-remove"></span> Clear
                                    </button>
                                    <div class="btn btn-default image-preview-input">
                                    	<p class="form-control-static"> 
										<span class="glyphicon glyphicon-folder-open"></span>
		                                <span class="image-preview-input-title">Browse</span>
										<input type="file" accept="image/png, image/jpeg, image/gif" name="file" /> <!-- rename it -->
                                    </div>
									
								</span>
								<input type="text" class="form-control image-preview-filename" disabled="disabled" /> <!-- don't give a name === doesn't send on POST/GET -->
							</div>
						</div>
						
                        <div class="form-group row">
							<label for="carrier" class="col-md-3  form-control-label">Carriers:</label>
							<div class="col-md-9">
								<select class="select2 carriers-multiple-select" name="carrier[]" id="select-carrier" multiple="multiple" >
									<option value="*" selected="selected">All Campaigns</option>
									@foreach($carriers as $carrier)
										<option value="{{$carrier->id}}" selected="">{{ $carrier->name }}</option>
									@endforeach
									
								</select>
							</div>
						</div>
						<div class="form-group row">
                          <label class="col-md-3  form-control-label" for="company">Customer Groups:</label>
                            <div class="col-md-9">
								<select class="select2 customer-multiple-select" name="group[]" id="select-customer-group" multiple="multiple" >
									<option value="*" selected="">All Campaigns</option>
									@foreach($customer_groups as $customer_group)
										<option value="{{$customer_group->id}}" selected="">{{ $customer_group->name }}</option>
									@endforeach
									
								</select>
							</div>
                        </div>
				    </div>
				</div>
				 <!-- <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" id="createSC" class="btn btn-success">Create</button>
				</div> -->
            </form>
		</div>
	</div>
</div><!--.modal 


	
