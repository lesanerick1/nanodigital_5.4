@extends('layouts.master')

@section('header_scripts')
<link rel="stylesheet" href="{{ asset('assets/css/lib/datatables-net/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/separate/vendor/datatables-net.min.css') }}">

<link rel="stylesheet" href="{{ asset('assets/css/separate/pages/widgets.min.css') }}">
{!! Charts::assets() !!}

@stop

@section('content')

	<div class="page-content">
		<div class="container-fluid">
	    <div class="row">
	    	<div class="col-md-12" >
	        <section class="card">
	          <header class="card-header" style="text-align: center;" >

	          	
              	<a href="{{route('picture-ads')}}" style="float:left;" class="btn  btn-primary btn-sm btn-success"><i class=" font-icon font-icon-arrow-left  "></i> &nbsp;Back to All Campaigns</a>
	            
              	<span style="float:center; vertical-align: bottom;">
              		Campaign Analysis: {{ $campaign[0]->name }}
              	</span>
	            
	            
	              <a href="users/create" style="float: right" class="btn btn-inline btn-primary btn-sm ladda-button"><i class="font-icon-left font-icon-plus"></i>Create New Campaign</a>
	            
	          </header>
	          <div class="card-block">
	            <table id="deliveries" class="table table-bordered" >
	          
		          <thead>
		            <tr>
		              <th>#</th>
		              <th>Phone Number</th>  
		              <th>Subject</th>
		              <th>Content</th>
		              <th>Delivery Status</th>
		              
		            </tr>
		          </thead>
		            
		          <tfoot>
		            <tr>
		              <th>#</th>
		              <th>Phone Number</th>
		              <th>Subject</th>
		              <th>Content</th>
		              <th>Delivery Status</th>
		            </tr>
		          </tfoot>
	          
	        	</table>
	        
	          </div>
	        </section>
	      </div>
	    </div>
	    <!-- Div for Campaign Delivery Charts -->
	    <div class="row">
	    	<div class="col-md-12" style="text-align: center; ">
		        <section class="card">
		          {!! $chart->render() !!}
		        </section>
		    </div>
	    </div>
	  </div>
	</div>

@stop

@section('footer_scripts')
<!-- <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->
<script src="{{ asset('assets/js/lib/datatables-net/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/bootstrap-notify/bootstrap-notify.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/js/lib/match-height/jquery.matchHeight.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lib/moment/moment-with-locales.min.js') }}"></script>

<script>
	$(function() {
    

		 var table =$('#deliveries').DataTable({
	      dom: 'Bfrtipl',
	            buttons: [
	                'copy', 'excel', 'pdf','print'
	            ],
	      processing: true,
	      serverSide: false,
	      ajax: '{!! route('picture-ads.deliveryData', $encodedId) !!}',
	      columns: [
	          { data: 'id' },
	          { data: 'receiver' },
	          { data: 'subject' },
	          { data: 'content' },
	          // { data: 'status' },
	          { 
	              data: 'delivery_status',
	              render: function ( data, type, row ) {
	                if ( data == 'Delivered')
	                  data = '<span class="label label-success">Delivered</span>';
	                else if ( data == 'Pending')
	                  data = '<span class="label label-warning">Pending</span>';
	                else
	                  data = " ";
	                return data;
	                }
	              },                     
	          

	      ],

	      initComplete: function () {

	            this.api().columns([4]).every( function () {
	                var column = this;
	                var select = $('<select><option value=""></option></select>')
	                    .appendTo( $(column.footer()).empty() )
	                    .on( 'change', function () {
	                        var val = $.fn.dataTable.util.escapeRegex(
	                            $(this).val()
	                        );
	 
	                        column
	                            .search( val ? '^'+val+'$' : '', true, false )
	                            .draw();
	                    } );
	 
	                column.data().unique().sort().each( function ( d, j ) {
	                    select.append( '<option value="'+d+'">'+d+'</option>' )
	                } );
	            } );
	        }

	    });
	    
	    // table.on( 'draw', function () {
	    //         $('.livicon').each(function(){
	    //             $(this).updateLivicon();
	    //         });
	    //     } );


	});


  
</script>


@stop