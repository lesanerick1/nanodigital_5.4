<?php

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Nano Digital Installer',
    'next' => 'Next Step',
    'back' => 'Back',
    'finish' => 'Finish',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title'   => 'Welcome To The Nano Digital Installer',
        'message' => 'Welcome to the Nano Digital setup wizard.',
        'start'   => 'You have made an excellent choice in selecting Nano Digital as your solution provider. We promise to have you up and running in under 2 minutes. Click Next whenever you are ready.',
    ],

    /*Pre-installation checks */
    'pre-installation' => [
        'title' => 'Pre-installation checks',
        'message' => 'Pre-installation checks for Nano Digital 1.0.5',
        'check'   => 'Congratulations, you aced the tests! On to the next step.',
        'fail'   => 'Something is not right, please fix it so we can continue installation.',
    ],

    /*Database Configuration */
    'db-configuration' => [
        'title' => 'Database Configuration',
        'message' => 'Database Configuration',
        'dba'   => 'You might need to call your Database Administrator for this one. Teamwork right?'
    ],


    /*Database Configuration */
    'administrator' => [
        'title' => 'System Administrator',
        'message' => 'System Administrator Configuration'
    ],

    /**
    *
    * Completed page translations.
    *
    */
    'completed' => [
        'title' => 'Completed',
        'message' => 'Application has been successfully installed.',
        'text' => 'Congratulations, you have completed the setup. Click on the Login button below to login to the system now.',
        'login' => 'Login',
    ],

    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Requirements',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'Permissions',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'Environment Settings',
        'save' => 'Save .env',
        'success' => 'Your .env file settings have been saved.',
        'errors' => 'Unable to save the .env file, Please create it manually.',
    ],

    'install' => 'Install',


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'Finished',
        'finished' => 'Application has been successfully installed.',
        'exit' => 'Click here to exit',
    ],

    /**
     *
     * Update specific translations
     *
     */
    'updater' => [
        /**
         *
         * Shared translations.
         *
         */
        'title' => 'Laravel Updater',

        /**
         *
         * Welcome page translations for update feature.
         *
         */
        'welcome' => [
            'title'   => 'Welcome To The Updater',
            'message' => 'Welcome to the update wizard.',
        ],

        /**
         *
         * Welcome page translations for update feature.
         *
         */
        'overview' => [
            'title'   => 'Overview',
            'message' => 'There is 1 update.|There are :number updates.',
        ],

        /**
         *
         * Final page translations.
         *
         */
        'final' => [
            'title' => 'Finished',
            'finished' => 'Application\'s database has been successfully updated.',
            'exit' => 'Click here to exit',
        ],
    ],

];
