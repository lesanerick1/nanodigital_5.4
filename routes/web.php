<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', ['middleware' => 'api.auth'], function ($api) {
    $api->get('authenticated_user', 'App\Http\Controllers\Api\AuthenticateController@authenticatedUser');
    
    $api->get('customers', 'App\Http\Controllers\Api\CustomerController@getCustomers');
    $api->get('customer/{phone_number}', 'App\Http\Controllers\Api\CustomerController@show');
    $api->get('customer-group/{group}', 'App\Http\Controllers\Api\CustomerController@customerByGroup');
    $api->get('customer-network/{carrier}', 'App\Http\Controllers\Api\CustomerController@customerByNetwork');
    $api->get('customers-subscribed/', 'App\Http\Controllers\Api\CustomerController@customerSubscribed');
    $api->get('customers-unsubscribed/', 'App\Http\Controllers\Api\CustomerController@customerUnsubscribed');

    $api->post('sendSms/{phone_number}/{message}', 'App\Http\Controllers\Api\BulkSmsController@sendSms');
    $api->post('sendSmsByGroup/{group}/{message}', 'App\Http\Controllers\Api\BulkSmsController@sendToGroup');
    $api->post('sendSmsByNetwork/{carrier}/{message}', 'App\Http\Controllers\Api\BulkSmsController@sendToNetwork');

    $api->post('sendAd/{phone_number}/{subject}/{image}', 'App\Http\Controllers\Api\PictureAdsController@sendAd');
    $api->post('sendAdByGroup/{group}/{subject}/{image}', 'App\Http\Controllers\Api\PictureAdsController@sendToGroup');
    $api->post('sendAdByNetwork/{carrier}/{subject}/{image}', 'App\Http\Controllers\Api\PictureAdsController@sendToNetwork');
});
$api->version('v1', function ($api) {

    $api->get('sendSms/{phone_number}/{message}', 'App\Http\Controllers\Api\BulkSmsController@sendSms');
    $api->post('authenticate', 'App\Http\Controllers\Api\AuthenticateController@authenticate');
    $api->post('logout', 'App\Http\Controllers\Api\AuthenticateController@logout');
    $api->get('token', 'App\Http\Controllers\Api\AuthenticateController@getToken');
});

# Error pages should be shown without requiring login
Route::get('404', function () {
    return View('404');
});
Route::get('500', function () {
    return View::make('500');
});

Route::get('signin', array('as' => 'signin', 'uses' => 'AuthController@getSignin'));
Route::post('signin', 'AuthController@postSignin');
Route::post('forgot-password', array('as' => 'forgot-password', 'uses' => 'AuthController@postForgotPassword'));
Route::get('/', array('as' => 'dashboard','uses' => 'MainController@showHome'));
Route::get('logout', array('as' => 'logout', 'uses' => 'AuthController@getLogout'));
/** end user login, registration **/

# Users Manager
Route::group(array('prefix' => 'users', 'middleware' => 'SentinelAdmin'), function () {
    Route::get('/', array('as' => 'users', 'uses' => 'UsersController@index'));
    Route::get('groups',['as' => 'groups.edit', 'uses' =>'UsersController@edit']);
    Route::get('create', 'UsersController@create');

    /*Route::get('data',['as' => 'users.data', 'uses' =>'UsersController@data']);
    Route::get('create', 'UsersController@create');
    Route::post('create', 'UsersController@store');
    Route::get('{userId}/show', array('as' => 'admin.users.show', 'uses' => 'UsersController@show'));
    Route::get('{userId}/delete', array('as' => 'delete/user', 'uses' => 'UsersController@destroy'));
    Route::get('{userId}/confirm-delete', array('as' => 'confirm-delete/user', 'uses' => 'UsersController@getModalDelete'));
    Route::get('{userId}/confirm-edit', array('as' => 'confirm-edit/user', 'uses' => 'UsersController@getModaledit'));
    Route::get('{userId}/restore', array('as' => 'restore/user', 'uses' => 'UsersController@getRestore'));
    Route::get('{userId}', array('as' => 'users.show', 'uses' => 'UsersController@show'));
    Route::post('{userId}/passwordreset', array('as' => 'passwordreset', 'uses' => 'UsersController@passwordreset'));*/
});

Route::post('upload','PictureAdsController@uploadFiles')->name('upload-files');
Route::post('send_mms','ComposerController@sendMms')->name('send_mms');
Route::post('sendSms','BulkSmsController@sendSms')->name('sendSms');
Route::post('importCustomers', 'CustomersController@importCustomers')->name('importCustomers');
Route::post('createShortCode', 'SettingsController@createShortCode')->name('createShortCode');
Route::group(array('prefix' => 'logs'), function () {
    Route::get('/show','\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    Route::get('/view/{file_name}', 'LogController@preview')->name('log.preview');
    Route::get('/download/{file_name}', 'LogController@download')->name('log.download');
    Route::get('log/delete/{file_name}', 'LogController@delete')->name('log.delete');
});

# Agency Manager
Route::group(array('prefix' => 'agencies', 'middleware' => 'SentinelAdmin'), function () {
    Route::get('/', array('as' => 'agencies', 'uses' => 'AgencyController@index'));
    Route::get('main', array('as' => 'agency.main', 'uses' => 'AgencyController@agencyData'));
    Route::get('{agency}/update', array('as' => 'agency/update', 'uses' => 'AgencyController@agencyData'));

    /*Route::get('data', array('as' => 'agency.data', 'uses' => 'AgencyController@data'));
    Route::get('{agency}/confirm-delete', array('as' => 'confirm-delete/agency', 'uses' => 'AgencyController@getModalDelete'));
    Route::get('{agency}/confirm-opt', array('as' => 'confirm-opt/agency', 'uses' => 'AgencyController@getModalOpt'));
    Route::get('{agency}/confirm-edit', array('as' => 'confirm-edit/agency', 'uses' => 'AgencyController@getModalEdit'));
    Route::get('{Agency1d}/restore', array('as' => 'restore/agency', 'uses' => 'AgencyController@getRestore'));
     Route::get('{agency}/delete', array('as' => 'delete/agency', 'uses' => 'AgencyController@destroy')); 
     Route::get('{agency}/opt', array('as' => 'opt/agency', 'uses' => 'AgencyController@optOut')); 
     Route::get('/filter','AgencyController@filterAgency')->name('agency.filter');*/    
});

# Company Manager
Route::group(array('prefix' => 'companies', 'middleware' => 'SentinelAdmin'), function () {
    Route::get('/', array('as' => 'companies', 'uses' => 'CompanyController@index'));
    /*Route::get('data', array('as' => 'agency.data', 'uses' => 'AgencyController@data'));
    Route::get('{agency}/confirm-delete', array('as' => 'confirm-delete/agency', 'uses' => 'AgencyController@getModalDelete'));
    Route::get('{agency}/confirm-opt', array('as' => 'confirm-opt/agency', 'uses' => 'AgencyController@getModalOpt'));
    Route::get('{agency}/confirm-edit', array('as' => 'confirm-edit/agency', 'uses' => 'AgencyController@getModalEdit'));
    Route::get('{Agency1d}/restore', array('as' => 'restore/agency', 'uses' => 'AgencyController@getRestore'));
     Route::get('{agency}/delete', array('as' => 'delete/agency', 'uses' => 'AgencyController@destroy')); 
     Route::get('{agency}/opt', array('as' => 'opt/agency', 'uses' => 'AgencyController@optOut')); 
     Route::get('/filter','AgencyController@filterAgency')->name('agency.filter');*/    
});
Route::get('/messages', function () {
    return view('messaging/index');
});
Route::get('/composer', function () {
    return view('composer/index');
});
Route::get('/scheduler', function () {
    return view('scheduler/index');
});
Route::get('/invoice', function () {
    return view('billing/invoice');
});
Route::get('/support', function () {
    return view('documentation/index');
});

# Picture Ads Manager
Route::group(array('prefix' => 'picture-ads', 'middleware' => 'SentinelAdmin'), function () {
    Route::get('/', array('as' => 'picture-ads', 'uses' => 'PictureAdsController@index'));
    Route::get('campaigns', array('as' => 'picture-ads.campaigns', 'uses' => 'PictureAdsController@campaignData'));
    Route::get('{id}/delivery', array('as' => 'picture-ads.delivery', 'uses' => 'PictureAdsController@deliveryAnalysis'));
    Route::get('{id}/deliveryData', array('as' => 'picture-ads.deliveryData', 'uses' => 'PictureAdsController@deliveryData'));
    // Route::post('delete/{id}', array('as' => 'picture-ads.delete', 'uses' => 'PictureAdsController@deleteImage'));
    Route::post('update/{id}', array('as' => 'picture-ads.update', 'uses' => 'PictureAdsController@updateCampaign'));
    Route::post('delete/', array('as' => 'picture-ads.delete', 'uses' => 'PictureAdsController@deleteImage'));
    Route::get('analysis/', array('as' => 'picture-ads.analysis', 'uses' => 'PictureAdsController@campaignAnalysis'));
    Route::post('analysis', array('as' => 'picture-ads.analysis', 'uses' => 'PictureAdsController@campaignAnalysis'));
    Route::post('create', array('as' => 'picture-ads.create', 'uses' => 'PictureAdsController@create'));

});

# Bulk SMS Manager
Route::group(array('prefix' => 'bulk-sms', 'middleware' => 'SentinelAdmin'), function () {
    Route::get('/', array('as' => 'bulk-sms', 'uses' => 'BulkSmsController@index'));
    Route::get('campaigns', array('as' => 'bulk-sms.campaigns', 'uses' => 'BulkSmsController@campaignData'));
    Route::get('{id}/delivery', array('as' => 'bulk-sms.delivery', 'uses' => 'BulkSmsController@deliveryAnalysis'));
    Route::get('{id}/deliveryData', array('as' => 'bulk-sms.deliveryData', 'uses' => 'BulkSmsController@deliveryData'));
Route::post('createCampaign','BulkSmsController@createCampaign')->name('createCampaign');

});

# Customers Manager
Route::group(array('prefix' => 'customers', 'middleware' => 'SentinelAdmin'), function () {
    Route::get('/', array('as' => 'customers', 'uses' => 'CustomersController@index'));
    Route::get('main', array('as' => 'customer.main', 'uses' => 'CustomersController@customerData'));


});

# Settings Manager
Route::group(array('prefix' => 'settings', 'middleware' => 'SentinelAdmin'), function () {
    Route::get('/', array('as' => 'settings', 'uses' => 'SettingsController@index'));

});

# Social Media Manager
Route::group(array('prefix' => 'social-media', 'middleware' => 'SentinelAdmin'), function () {
    Route::get('/', array('as' => 'social-media', 'uses' => 'SocialMediaController@index'));

});